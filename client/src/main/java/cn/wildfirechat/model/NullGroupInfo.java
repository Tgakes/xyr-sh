/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.model;

import cn.wildfirechat.client.R;
import cn.wildfirechat.remote.ChatManager;

public class NullGroupInfo extends GroupInfo {
    public NullGroupInfo(String groupId) {
        this.target = groupId;
        //this.name = "<" + groupId + ">";
        this.name = ChatManager.Instance().getApplicationContext().getString(R.string.group_chat);
    }
}

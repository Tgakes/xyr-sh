/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.model;

import cn.wildfirechat.client.R;
import cn.wildfirechat.remote.ChatManager;

public class NullChatRoomInfo extends ChatRoomInfo {
    public NullChatRoomInfo(String chatRoomId) {
        this.chatRoomId = chatRoomId;
        //this.title = "<" + chatRoomId + ">";
        this.title = ChatManager.Instance().getApplicationContext().getString(R.string.chat_room);
    }
}
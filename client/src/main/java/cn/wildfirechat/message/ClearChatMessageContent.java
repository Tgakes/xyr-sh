/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.message;

import android.os.Parcel;

import cn.wildfirechat.message.core.ContentTag;
import cn.wildfirechat.message.core.MessagePayload;
import cn.wildfirechat.message.core.PersistFlag;

import static cn.wildfirechat.message.core.MessageContentType.ContentType_Clear_Chat;

/**
 * Created by gakes on 2020/10/11.
 */

@ContentTag(type = ContentType_Clear_Chat, flag = PersistFlag.Persist_And_Count)
public class ClearChatMessageContent extends MessageContent {
    private String content;

    public ClearChatMessageContent() {
    }

    public ClearChatMessageContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public MessagePayload encode() {
        MessagePayload payload = new MessagePayload();
        payload.searchableContent = content;
        payload.mentionedType = mentionedType;
        payload.mentionedTargets = mentionedTargets;
        return payload;
    }


    @Override
    public void decode(MessagePayload payload) {
        content = payload.searchableContent;
        mentionedType = payload.mentionedType;
        mentionedTargets = payload.mentionedTargets;
    }

    @Override
    public String digest(Message message) {
        return content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.content);
    }

    protected ClearChatMessageContent(Parcel in) {
        super(in);
        this.content = in.readString();
    }

    public static final Creator<ClearChatMessageContent> CREATOR = new Creator<ClearChatMessageContent>() {
        @Override
        public ClearChatMessageContent createFromParcel(Parcel source) {
            return new ClearChatMessageContent(source);
        }

        @Override
        public ClearChatMessageContent[] newArray(int size) {
            return new ClearChatMessageContent[size];
        }
    };
}

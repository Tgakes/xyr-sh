/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.message;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import cn.wildfirechat.message.core.ContentTag;
import cn.wildfirechat.message.core.MessagePayload;
import cn.wildfirechat.message.core.PersistFlag;

import static cn.wildfirechat.message.core.MessageContentType.ContentType_Assistant_Pay;

/**
 * 支付助手
 * Created by gakes lee on 2020/12/01.
 */
@ContentTag(type = ContentType_Assistant_Pay, flag = PersistFlag.Persist_And_Count)
public class PayAssistantMessageContent extends MessageContent {


    private static final String PAY_PREFIX = "[山海支付通知]";

    private static final String RECEIPT_MONEY_PREFIX = "[收款到账通知]";


//    private String target;

    String amount;

    String orderId;

    /**
     * 币种
     */
    int currency;

    /**
     * 1：扫一扫 2：转账
     */
    int way;
    /**
     * 1，支付；2，收款
     */
    private int type;

    public String getAmount() {
        return amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public int getCurrency() {
        return currency;
    }

    public int getWay() {
        return way;
    }

    public int getType() {
        return type;
    }

    public PayAssistantMessageContent(String amount, String orderId, int currency, int way, int type) {
        this.amount = amount;
        this.orderId = orderId;
        this.currency = currency;
        this.way = way;
        this.type = type;
    }


    public PayAssistantMessageContent() {
//        this.mediaType = MessageContentMediaType.TRANSFER;
    }

   /* public TransferMessageContent(String filePath) {
        this.mediaType = MessageContentMediaType.TRANSFER;
    }*/

    @Override
    public MessagePayload encode() {
        MessagePayload payload = new MessagePayload();
        payload.content = TypingMessageContent.TYPING_PAY + "";
        return payload;
    }

    @Override
    public void decode(MessagePayload payload) {

        try {
//            Log.i("logs","payload.content==="+payload.content);
            JSONObject data = new JSONObject(payload.content);
            orderId = data.has("orderid") ? data.getString("orderid") : "";
            amount = data.has("amount") ? data.getString("amount") : "";
            currency = data.has("currency") ? data.getInt("currency") : -1;
            way = data.has("way") ? data.getInt("way") : -1;
            type = data.has("type") ? data.getInt("type") : -1;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public String digest(Message message) {
        return type == 2 ? RECEIPT_MONEY_PREFIX : PAY_PREFIX;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.amount);
        dest.writeString(this.orderId);
        dest.writeInt(this.currency);
        dest.writeInt(this.way);
        dest.writeInt(this.type);
    }

    protected PayAssistantMessageContent(Parcel in) {
        super(in);
        this.amount = in.readString();
        this.orderId = in.readString();
        this.currency = in.readInt();
        this.way = in.readInt();
        this.type = in.readInt();
    }

    public static final Creator<PayAssistantMessageContent> CREATOR = new Creator<PayAssistantMessageContent>() {
        @Override
        public PayAssistantMessageContent createFromParcel(Parcel source) {
            return new PayAssistantMessageContent(source);
        }

        @Override
        public PayAssistantMessageContent[] newArray(int size) {
            return new PayAssistantMessageContent[size];
        }
    };
}

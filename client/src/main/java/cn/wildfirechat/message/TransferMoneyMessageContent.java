/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.message;

import android.os.Parcel;
import android.util.Log;

import com.easy.utils.EmptyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import cn.wildfirechat.message.core.ContentTag;
import cn.wildfirechat.message.core.MessagePayload;
import cn.wildfirechat.message.core.PersistFlag;

import static cn.wildfirechat.message.core.MessageContentType.Transfer_Account_Chat;

/**
 * 转账
 * Created by gakes on 2020/12/08.
 */
@ContentTag(type = Transfer_Account_Chat, flag = PersistFlag.Persist_And_Count)
public class TransferMoneyMessageContent extends MessageContent {


    private static final String TRANSFER_PREFIX = "[转账]";

    String amount;

    String orderId;

    String des;

    /**
     * 币种
     */
    int currency;



    public String getAmount() {
        return amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public int getCurrency() {
        return currency;
    }

    public String getDes() {
        return des;
    }



    public TransferMoneyMessageContent(String amount, String orderId, String des, int currency) {
        this.amount = amount;
        this.orderId = orderId;
        this.des = des;
        this.currency = currency;

    }


    public TransferMoneyMessageContent() {
//        this.mediaType = MessageContentMediaType.TRANSFER;
    }

   /* public TransferMessageContent(String filePath) {
        this.mediaType = MessageContentMediaType.TRANSFER;
    }*/

    @Override
    public MessagePayload encode() {
        MessagePayload payload = new MessagePayload();
        payload.searchableContent = TRANSFER_PREFIX;
        try {
            JSONObject objWrite = new JSONObject();
            objWrite.put("amount", amount);
            objWrite.put("orderId", orderId);
            objWrite.put("des", des);
            objWrite.put("currency", currency);
            payload.content = objWrite.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    @Override
    public void decode(MessagePayload payload) {

        try {
           String jsonData = payload.content;
            Log.i("logs", "TransferAccountMessageContent decode jsonData===" + jsonData);
            Log.i("logs", "TransferAccountMessageContent decode payload===" + payload.toString());
            if (EmptyUtils.isNotEmpty(jsonData)) {

                JSONObject data = new JSONObject(jsonData);
                orderId = data.has("orderId") ? data.getString("orderId") : "";
                amount = data.has("amount") ? data.getString("amount") : "";
                des = data.has("des") ? data.getString("des") : "";
                currency = data.has("currency") ? data.getInt("currency") : -1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public String digest(Message message) {
        return TRANSFER_PREFIX;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.amount);
        dest.writeString(this.orderId);
        dest.writeString(this.des);
        dest.writeInt(this.currency);

    }

    protected TransferMoneyMessageContent(Parcel in) {
        super(in);
        this.amount = in.readString();
        this.orderId = in.readString();
        this.des = in.readString();
        this.currency = in.readInt();
    }

    public static final Creator<TransferMoneyMessageContent> CREATOR = new Creator<TransferMoneyMessageContent>() {
        @Override
        public TransferMoneyMessageContent createFromParcel(Parcel source) {
            return new TransferMoneyMessageContent(source);
        }

        @Override
        public TransferMoneyMessageContent[] newArray(int size) {
            return new TransferMoneyMessageContent[size];
        }
    };
}

package com.easy.qrcode.ui.qr_code;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Vibrator;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.qrcode.R;
import com.easy.qrcode.databinding.QrScanBinding;
import com.easy.utils.IntentUtils;
import com.easy.utils.ToastUtils;
import com.lqr.imagepicker.ImagePicker;
import com.lqr.imagepicker.bean.ImageItem;

import java.util.ArrayList;

@ActivityInject
@Route(path = QrScanActivity.QR_SCAN_ACTIVITY, name = "二维码扫码")
public class QrScanActivity extends BaseActivity<QrScanPresenter, QrScanBinding> implements QrScanView, QRCodeReaderView.OnQRCodeReadListener, ViewTreeObserver.OnGlobalLayoutListener  {


    public static final String QR_SCAN_ACTIVITY = "/qrCode/QrScanActivity";


    private boolean hasPermission;
    Vibrator vibrator;
    long[] pattern;
    public final int CODE_SELECT_IMAGE = 2;
    QRCodeReaderView qrCodeReaderView;

    @Override
    public void initStateBar() {
        StatusBarUtil.setTranslucentStatus(this);
        StatusBarUtil.setStatusBarDarkTheme(this, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.qr_scan;
    }

    @Override
    public void initView() {


        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewBind.rlScanTop.getLayoutParams();
        params.topMargin = StatusBarUtil.getStatusBarHeight(this);
        viewBind.rlScanTop.setLayoutParams(params);

        viewBind.rlRoot.getViewTreeObserver().addOnGlobalLayoutListener(this);


    }

    /**
     * 震动
     */
    private void initVibrator() {
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        pattern = new long[]{100, 400, 100, 400};   // 停止 开启 停止 开启
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (hasPermission && qrCodeReaderView != null) {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (hasPermission && qrCodeReaderView != null) {
            qrCodeReaderView.stopCamera();
        }
    }

    @Override
    public void permissionCallback(Boolean granted, int type, Throwable e) {
        if (e != null) {
            ToastUtils.showShort("error：" + e.getMessage());
        } else {
            if (granted) {
                hasPermission = true;
                if (type == 1) {
                    initQRCodeReaderView();
                } else if (type == 2) {
                    presenter.selectAlbum(this, CODE_SELECT_IMAGE);
                }
            } else {
                ToastUtils.showShort(getString(R.string.permission_not_allow));
            }
        }
    }

    public static final String SCAN_RESULT = "SCAN_RESULT";

    @Override
    public void scanAlbumCallBack(String result, int code) {
        hideLoading();
        if (qrCodeReaderView != null) {
            qrCodeReaderView.setQRDecodingEnabled(true);
        }
        if (code == 2) { //扫码异常
            ToastUtils.showShort(result);
        } else {
            Intent intent = new Intent();
            intent.putExtra(SCAN_RESULT, result);
            setResult(Activity.RESULT_OK, intent);
            finish();

//           ToastUtils.showShort(result);
          /*  ARouter.getInstance().build("/app/SearchActivity").withString("keySearch", result).withTransition(0, 0).navigation(QrScanActivity.this,  new NavCallback() {

                @Override
                public void onArrival(Postcard postcard) {
                    QrScanActivity.this.finish();
                }
            });*/
        }
    }

    private void initQRCodeReaderView() {
        qrCodeReaderView = new QRCodeReaderView(this);
        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setQRDecodingEnabled(true);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewBind.flContain.addView(qrCodeReaderView,0, layoutParams);
//       qrCodeReaderView.setTorchEnabled(true);
        qrCodeReaderView.startCamera();
    }

    @Override
    public void onQRCodeRead(String result, PointF[] points) {
        vibrator.vibrate(pattern, -1);
        //todo 扫码结果
//        ToastUtils.showShort(text);
        if (qrCodeReaderView != null) {
            qrCodeReaderView.setQRDecodingEnabled(false);
        }

        Intent intent = new Intent();
        intent.putExtra(SCAN_RESULT, result);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CODE_SELECT_IMAGE:
                if (resultCode == RESULT_OK) {
                    if (qrCodeReaderView != null) {
                        qrCodeReaderView.setQRDecodingEnabled(false);
                    }
                    ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    if (images != null && images.size() > 0) {
                        showLoading();
                        presenter.scanAlbum(images.get(0).path);
                    }
//                    showLoading();
//                    String picturePath = IntentUtils.selectPic(this, data);
//                    presenter.scanAlbum(picturePath);
                }
                break;
        }
    }

    @Override
    public void onGlobalLayout() {

        viewBind.ivBack.setOnClickListener(view -> finish());
        viewBind.ivMore.setOnClickListener(view ->
                presenter.requestPermission(getRxPermissions(), 2, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE));
        initVibrator();
        presenter.requestPermission(getRxPermissions(), 1, Manifest.permission.CAMERA, Manifest.permission.VIBRATE);


        viewBind.rlRoot.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}

package com.easy.framework.base.common;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.easy.framework.R;
import com.easy.framework.base.BaseApplication;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.utils.KeyBoardUtils;
import com.easy.utils.StreamUtils;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;
import com.easy.widget.swipeback.SwipeBackActivityHelper;
import com.easy.widget.swipeback.SwipeBackLayout;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;

/**
 * 添加通用的Ui
 */
public abstract class CommonActivity extends AppCompatActivity {
    public Context context;
    //右滑退出
    public SwipeBackActivityHelper mHelper;
    public SwipeBackLayout mSwipeBackLayout;
    BasePopupView loadingDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
//        setBackground(R.color.color_ffffff);
        initSwipeBackLayout();
    }

    @Override
    public void onDestroy() {
        hideLoading();
        super.onDestroy();
    }

    public SkinTitleView addTitleView() {
        ViewGroup rootView = findViewById(android.R.id.content);
        if (rootView != null && rootView.getChildCount() > 0) {
            View view = rootView.getChildAt(0);
            if (view instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) view;
                SkinTitleView skinTitleView = new SkinTitleView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.addView(skinTitleView, 0, layoutParams);
                skinTitleView.setLeftClickListener(v -> finish());
                return skinTitleView;
            } else {
                ConstraintLayout constraintLayout = view.findViewById(R.id.cl_title_bar);
                if (constraintLayout != null) {
                    SkinTitleView skinTitleView = new SkinTitleView(this);
                    ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    constraintLayout.addView(skinTitleView, 0, layoutParams);
                    skinTitleView.setLeftClickListener(v -> finish());
                    return skinTitleView;
                }

            }
        }
        return null;
    }

    public void setBackground(int id) {
        View rootView = findViewById(android.R.id.content);
        if (rootView != null) {
            rootView.setBackgroundResource(id);
        }
    }

    /**
     * 初始化左滑关闭
     */
    public void initSwipeBackLayout() {
        mHelper = new SwipeBackActivityHelper(this);
        mHelper.onActivityCreate();
        mSwipeBackLayout = mHelper.getSwipeBackLayout();
        mSwipeBackLayout.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT);
        mSwipeBackLayout.setEnableGesture(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mHelper != null) {
            mHelper.onPostCreate();
        }
    }

    public void closeSwipeBackLayout() {
        if (mSwipeBackLayout != null) {
            mSwipeBackLayout.setEnableGesture(false);
        }
    }

    public void openSwipeBackLayout() {
        if (mSwipeBackLayout != null) {
            mSwipeBackLayout.setEnableGesture(true);
        }
    }

    public void hideLoading() {
        if (loadingDialog != null && loadingDialog.isShow()) {
            loadingDialog.dismiss();
        }
    }

    public void showLoading(String... content) {
        if (loadingDialog == null) {
            loadingDialog = new XPopup.Builder(this).autoDismiss(false)
                    .asLoading(content == null || content.length == 0 ? "正在加载中" : content[0])
                    .show();
        } else if (!loadingDialog.isShow()) {
            loadingDialog.show();
        }
    }




    /**
     * 设置自定义状态颜色
     */
    public void initStateBar() {

      /*  SkinUtils.Skin skin = SkinUtils.getSkin(this);
        boolean light = skin.isLight();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // >=5.0 背景为全透明
            *//* >=5.0，this method(getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS));
            in some phone is half-transparent like vivo、nexus6p..
            in some phone is full-transparent
            so ...*//*
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (light) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                }
                window.setStatusBarColor(Color.TRANSPARENT);
            } else {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                if (light) {
                    window.setStatusBarColor(Color.BLACK);
                } else {
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
            }
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 4.4背景为渐变半透明
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }*/
        StatusBarUtil.setRootViewFitsSystemWindows(this, true);
        StatusBarUtil.setTranslucentStatus(this);
        if (!StatusBarUtil.setStatusBarDarkTheme(this, true)) {
            //如果不支持设置深色风格 为了兼容总不能让状态栏白白的看不清, 于是设置一个状态栏颜色为半透明,
            //这样半透明+白=灰, 状态栏的文字能看得清
            StatusBarUtil.setStatusBarColor(this, 0x55000000);
        } else {
            StatusBarUtil.setStatusBarColor(this, StreamUtils.getInstance().resourceToColor(PreferenceUtils.getBoolean(this, PreferenceUtils.DARK_THEME) ? R.color.status_color_ : R.color.status_color, BaseApplication.getInst()));
        }
    }

    /**
     * 点击空白地方隐藏键盘
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (MotionEvent.ACTION_DOWN == ev.getAction()) {
            View view = getCurrentFocus();
            if (view != null) {
                KeyBoardUtils.hideKeyboard(ev, view, getApplicationContext());//调用方法判断是否需要隐藏键盘
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}

package com.easy.framework.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Lifecycle;

import com.easy.apt.lib.InjectActivity;
import com.easy.framework.R;
import com.easy.framework.base.common.CommonActivity;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.store.bean.Accounts;
import com.easy.utils.LocaleHelper;
import com.easy.utils.RootUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.AutoDisposeConverter;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 添加通用的功能
 *
 * @param <P>
 * @param <V>
 */
public abstract class BaseActivity<P extends BasePresenter, V extends ViewDataBinding> extends CommonActivity implements BaseView{

    public V viewBind;

    @Inject
    public P presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LocaleHelper.setLocale(this, LocaleHelper.getLanguage(this));
        super.onCreate(savedInstanceState);


        int layoutId = getLayoutId();
        if (layoutId != 0) {
            viewBind = DataBindingUtil.setContentView(this, layoutId);
        }
        initStateBar();
        StatusBarUtil.setNavigationBarColor(this);
        InjectActivity.inject(this);
        if (presenter != null) {
            presenter.attachView(context, this, this);
        }
        if (RootUtils.isDeviceRooted()) {//设备已root
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(getString(R.string.app_not_support_root))
                    .setPositiveButton(getString(R.string.sure), (d, w) -> ActivityManager.getInstance().exitApp())
                    .show();
        }else {
            initView();
        }

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
    }

    public abstract int getLayoutId();

    public abstract void initView();

    public RxPermissions getRxPermissions() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.setLogging(true);
        return rxPermissions;
    }

    public <T> AutoDisposeConverter<T> getAutoDispose() {
        return AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this));
    }

    public <T> AutoDisposeConverter<T> getAutoDispose(Lifecycle.Event untilEvent) {
        return AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this, untilEvent));
    }




}

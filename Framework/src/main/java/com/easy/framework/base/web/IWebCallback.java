package com.easy.framework.base.web;

import android.webkit.WebView;

public interface IWebCallback {

    void loadWebFinish(WebView view, String url);

    void receivedWebTitle(String title);

    void loadProgress(int progress);

    void finish();

    void showCloseBtn(boolean show);

    JsToAndroid getJsToAndroid();
}

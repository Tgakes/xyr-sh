package com.easy.framework.base;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;

import com.easy.apt.lib.InjectFragment;
import com.easy.framework.R;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

public abstract class BaseDialogFragment<P extends BasePresenter, V extends ViewDataBinding> extends AppCompatDialogFragment implements BaseView {
    public V viewBind;
    public Context context;
    View rootView;
    @Inject
    public P presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.BottomDialog);
        context = getContext();
        InjectFragment.inject(this);
        if (presenter != null)
            presenter.attachView(context, this,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(getCancelOutside());
        viewBind = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        rootView = viewBind.getRoot();
        initView(rootView);
        return rootView;
    }

    public boolean getCancelOutside() {
        return true;
    }

    public abstract int getLayoutId();

    public abstract void initView(View view);

    public RxPermissions getRxPermissions() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.setLogging(true);
        return rxPermissions;
    }

    @Override
    public void onDetach() {
        if (presenter != null) {
            presenter.detachView();
        }
        super.onDetach();
    }
}

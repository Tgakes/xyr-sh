package com.easy.framework.dialog;

import android.Manifest;

import androidx.lifecycle.Lifecycle;

import com.easy.framework.base.BasePresenter;
import com.easy.net.RxHttp;
import com.easy.net.beans.Response;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WebDownloadDialogPresenter extends BasePresenter<WebDownloadDialogView> {
    @Inject
    public WebDownloadDialogPresenter() {

    }

    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }

    public void requestFileSize(String url) {
        RxHttp.get(url)
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.requestFileSize(result),
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.requestFileSize(response);
                        });
    }
}
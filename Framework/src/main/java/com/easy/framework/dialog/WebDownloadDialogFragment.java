package com.easy.framework.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.R;
import com.easy.framework.base.BottomDialogFragment;
import com.easy.framework.databinding.WebDownloadDialogBinding;
import com.easy.net.beans.Response;
import com.easy.store.bean.DownloadDo;
import com.easy.store.dao.DownloadDao;
import com.easy.utils.FileUtils;
import com.easy.utils.StringUtils;
import com.easy.utils.SystemUtils;
import com.easy.utils.ToastUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.inject.Inject;

@FragmentInject
public class WebDownloadDialogFragment extends BottomDialogFragment<WebDownloadDialogPresenter, WebDownloadDialogBinding> implements WebDownloadDialogView {
    String url;
    long contentLength;
    String size;
    Context context;
    String fileName;
    String mimeType;

    @Inject
    DownloadDao downloadDao;

    @Override
    public int getLayoutId() {
        return R.layout.web_download_dialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        parse(getArguments());
    }

    @Override
    public void initView(View view) {
        viewBind.tvFileName.setText(fileName);
        viewBind.tvSize.setText(size);
        viewBind.btnDownload.setOnClickListener(v -> {
            presenter.requestPermission(getRxPermissions());
        });
    }

    private void startDownload() {
        long currentTimeMillis = System.currentTimeMillis();

        int index = fileName.lastIndexOf(".");
        String apk = null;
        if (index != -1) {
            String fileTitle = fileName.substring(0, index);
            apk = fileName.substring(index + 1);
            fileName = StringUtils.buildString(fileTitle, "_", currentTimeMillis, ".", apk);
        } else {
            fileName = StringUtils.buildString(fileName, "_", currentTimeMillis);
        }
        String directory = FileUtils.getFilePath(FileUtils.getFileType(apk, mimeType), getContext());
        String savePath = directory + fileName;
        Log.d("startDownload", "savePath:" + savePath);

        DownloadDo downloadDo = new DownloadDo();
        downloadDo.setFileName(fileName);
        downloadDo.setServerUrl(url);
        downloadDo.setLocalUrl(savePath);
        downloadDo.setTotalSize(contentLength == 0 ? 1 : contentLength);
        downloadDo.setTag("tag_" + currentTimeMillis);
        downloadDao.insertOrUpdate(downloadDo);
        ARouter.getInstance().build("/app/DownloadActivity").navigation();
        dismiss();
    }

    private void parse(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        //方式1
        //url = https://static.ethte.com/client/release/Android/MEET.ONE_3.5.2.apk
        // contentDisposition = attachment; filename="MEET.ONE_3.5.2.apk"; filename*=utf-8''MEET.ONE_3.5.2.apk
        // mimeType = application/vnd.android.package-archive contentLength = 45364110

        //方式2
        // url = https://dldir1.qq.com/invc/tt/QB/Public/repack/qqbrowser_10.6.1.4036_7630_GA_10001.apk?pkg_name=com.tencent.mtt&app=qqbrowser
        // contentDisposition =
        // mimeType = application/octet-stream contentLength = 38944380

        //方式3
        //url = http://down.soxs.cc/all/28431/199214/%E6%B1%89%E4%B9%A1%E5%85%A8%E6%96%87@m.soxs.cc.txt
        // contentDisposition =
        // mimeType = application/octet-stream contentLength = 383204

        //方式4
        //url = https://m3.8js.net:99/20200306/57_yelangdiscorap.mp3
        // contentDisposition =
        // mimeType = audio/mpeg contentLength = 765461

        //方式5
        // url = http://ggzy.dg.gov.cn/ggzy/Common/Attachment/download?id=8a1280cb73dcfa6c0173dd03569b035f
        // contentDisposition = attachment;
        // filename=%E4%B8%9C%E8%8E%9E%E5%B8%82%E5%BB%BA%E8%AE%BE%E5%B7%A5%E7%A8%8B%E6%8A%95%E6%A0%87%E6%96%87%E4%BB%B6%E7%AE%A1%E7%90%86%E8%BD%AF%E4%BB%B6V8.1%EF%BC%88%E6%AD%A3%E5%BC%8F%E7%89%88%EF%BC%89.zip
        // mimeType = application/zip
        // contentLength = 0

        url = bundle.getString("url");
        contentLength = bundle.getLong("contentLength");
        if (contentLength <= 0) {
            SystemUtils.goSystemBrowser(getContext(), url);
            dismiss();
            return;
        }
        mimeType = bundle.getString("mimeType");
        String[] formatSize = FileUtils.getFormatSize(contentLength);
        size = formatSize[0] + formatSize[1];
        String contentDisposition = bundle.getString("contentDisposition");
        if (contentDisposition != null && contentDisposition.contains("; ")) {//方式1
            String[] splits = contentDisposition.split("; ");
            if (splits.length > 0) {
                for (String split : splits) {
                    if (split.contains("=")) {
                        String[] keyValue = split.split("=");
                        if ("filename".equals(keyValue[0])) {
                            fileName = keyValue[1].replace("\"", "");
                            try {
                                fileName = URLDecoder.decode(fileName, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } else {//方式2
            String tmpUrl = url;
            if (url.contains("?")) {
                tmpUrl = url.substring(0, url.indexOf("?"));
            }
            fileName = tmpUrl.substring(tmpUrl.lastIndexOf("/") + 1);
            if (!TextUtils.isEmpty(fileName)) {
                try {
                    fileName = URLDecoder.decode(fileName, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return;
            }
            SystemUtils.goSystemBrowser(getContext(), url);
            dismiss();
        }
    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (e != null) {
            ToastUtils.showShort("error：" + e.getMessage());
        } else {
            if (granted) {
                startDownload();
            } else {
                ToastUtils.showShort(getString(R.string.download_need_premission));
            }
        }
    }

    @Override
    public void requestFileSize(Response<String> response) {
        if (response.isSuccess()) {
            Log.d("requestFileSize", "size : " + response.getOriginalData());
        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }
}

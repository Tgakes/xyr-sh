package com.easy.framework.bean;

public class AppVersion {
    private String version;//版本名称
    private String code;//版本号
    private int type;//0:不强制 1:强制
    private String maintxt;//弹窗提示语
    private String addtime;//新版本上传时间
    private String url;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMaintxt() {
        return maintxt;
    }

    public void setMaintxt(String maintxt) {
        this.maintxt = maintxt;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

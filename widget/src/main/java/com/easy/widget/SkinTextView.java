package com.easy.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;


/**
 * 抽象出标题栏上的控件以实现根据皮肤切换深色浅色，
 */
public class SkinTextView extends AppCompatTextView {
    public SkinTextView(Context context) {
        super(context);
        init();
    }

    public SkinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SkinTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
//        SkinUtils.Skin skin = SkinUtils.getSkin(getContext());
//        setTextColor(ContextCompat.getColorStateList(getContext(), skin.isLight() ? R.color.color_333333 : R.color.color_ffffff));
    }
}

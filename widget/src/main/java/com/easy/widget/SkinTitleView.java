package com.easy.widget;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.easy.utils.StreamUtils;


public class SkinTitleView extends RelativeLayout {

    private SkinImageView ivBack;
    private SkinTextView tvTitle;
    private SkinTextView tvRight;

    public SkinTitleView(Context context) {
        super(context);
        init();
    }

    public SkinTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SkinTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.title_bar, this);
        ivBack = findViewById(R.id.ivBack);
        tvTitle = findViewById(R.id.tvTitle);
        tvRight = findViewById(R.id.tvRight);
//        SkinUtils.Skin skin = SkinUtils.getSkin(getContext());
        setBackground(R.color.title_color);
    }

    public void hideBackBtn() {
        ivBack.setVisibility(GONE);
    }

    public void setBackIcon(int res) {
        ivBack.setImageResource(res);
        if (!PreferenceUtils.getBoolean(getContext(),PreferenceUtils.DARK_THEME, true)) {
            ivBack.getDrawable().setColorFilter(StreamUtils.getInstance().resourceToColor(R.color.color_ffffff, getContext()), PorterDuff.Mode.SRC_IN);
        }
    }

    public void setLeftClickListener(OnClickListener clickListener) {
        if (clickListener != null) {
            ivBack.setOnClickListener(clickListener);
        }
    }

    public SkinTextView setTitleText(String titleText) {
        if (!TextUtils.isEmpty(titleText)) {
            if (tvTitle != null) {
                tvTitle.setText(titleText);
                tvTitle.setVisibility(VISIBLE);
            }
        }
        return tvTitle;

    }

    public void setRightImage(Drawable drawable) {
        if (tvRight != null) {
            tvRight.setVisibility(VISIBLE);
            tvRight.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    public SkinTextView setRightText(String rightText) {
        if (tvRight != null) {
            tvRight.setVisibility(VISIBLE);
            tvRight.setText(rightText);
            return tvRight;
        }
        return null;
    }

    public void setRightClickListener(OnClickListener clickListener) {
        if (clickListener != null) {
            tvRight.setOnClickListener(clickListener);
        }
    }

    public void setBackground(int resId) {
        this.setBackgroundResource(resId);
    }
}

package com.easy.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.method.ScrollingMovementMethod;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialog;

import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;


public class AppUpdateDialog extends AppCompatDialog {
    View.OnClickListener listener;
    TextView tvContent, tvUpdate, tvUpdateTitle, tvProgress,tvForceUpdate;
    ProgressBar progressBar;
    boolean isDownloaded;
    View vLine;
    LinearLayout llProgress, rlNoForce, llForce;


    public AppUpdateDialog(@NonNull Activity activity) {
        super(activity);
        initDialog(activity);
    }

    private void initDialog(Activity activity) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.app_update_dialog, null);
        setContentView(view);
        initView(view, activity);
    }

    private void initView(View view, Activity activity) {
        tvContent = view.findViewById(R.id.tvContent);
        tvUpdate = view.findViewById(R.id.tvUpdate);
        vLine = view.findViewById(R.id.vLine);
        tvUpdateTitle = view.findViewById(R.id.tvUpdateTitle);
        llProgress = view.findViewById(R.id.llProgress);
        progressBar = view.findViewById(R.id.progressBar);
        tvProgress = view.findViewById(R.id.tvProgress);
        llForce = view.findViewById(R.id.llForce);
        rlNoForce = view.findViewById(R.id.rlNoForce);
        tvForceUpdate= view.findViewById(R.id.tvForceUpdate);
        View imageClose = view.findViewById(R.id.tvNoUpdate);
        imageClose.setOnClickListener(v -> {
            if (progressBar.getProgress() == 0) {
                dismiss();
            } else {
                ToastUtils.showShort(activity.getString(R.string.downloading));
            }
        });
        tvContent.setMovementMethod(new ScrollingMovementMethod());
    }

    public void setData(boolean downloaded, String title, String content, int type,View.OnClickListener onClickListener) {
        listener = onClickListener;
        isDownloaded = downloaded;
        if (EmptyUtils.isNotEmpty(title)) {
            tvUpdateTitle.setText(title);
        }
        if (EmptyUtils.isNotEmpty(content)) {
            content = content.replace("\\n", "\r\n");
        }
        tvContent.setText(content);
        if (isDownloaded) {
            tvUpdate.setText(getContext().getResources().getString(R.string.install_app));
        }
        if (type == 1) {
            llForce.setVisibility(View.VISIBLE);
            rlNoForce.setVisibility(View.GONE);
            tvForceUpdate.setOnClickListener(v -> {
                if (!isDownloaded) {
                    vLine.setVisibility(View.GONE);
                    llForce.setVisibility(View.GONE);
                    llProgress.setVisibility(View.VISIBLE);
                    progressBar.setProgress(0);
                    tvProgress.setText("0%");
                }
                if (listener != null) {
                    listener.onClick(v);
                }
            });
        } else {
            llForce.setVisibility(View.GONE);
            rlNoForce.setVisibility(View.VISIBLE);
            tvUpdate.setOnClickListener(v -> {
                if (!isDownloaded) {
                    vLine.setVisibility(View.GONE);
                    rlNoForce.setVisibility(View.GONE);
                    llProgress.setVisibility(View.VISIBLE);
                    progressBar.setProgress(0);
                    tvProgress.setText("0%");
                }
                if (listener != null) {
                    listener.onClick(v);
                }
            });
        }


    }

    @Override
    public void show() {
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        super.show();
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (display.getWidth()); //设置宽度
        getWindow().setAttributes(lp);

        Window dialogWindow = getWindow();
        ColorDrawable dw = new ColorDrawable(0x0000ff00);
        dialogWindow.setBackgroundDrawable(dw);
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setWindowAnimations(R.style.dialogBottomAnimation);
    }

    public void setProgress(int aLong) {
        progressBar.setProgress(aLong);
        tvProgress.setText(aLong + "%");
    }
}

package com.easy.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;


import java.util.ArrayList;

public class WalletView {


    public static final String SH_NAME = "SH";

    public static final String USDT_NAME = "USDT";

    public static void showWalletView(OnWalletSelected onWalletSelected, Context context) {
        ArrayList<String> optionsItems = new ArrayList<>();
        optionsItems.add(SH_NAME);
        optionsItems.add(USDT_NAME);
        OptionsPickerBuilder builder = new OptionsPickerBuilder(context, (options1, options2, options3, v) -> {
            onWalletSelected.onWalletSelected(optionsItems.get(options1));
        });
        OptionsPickerView<String> sexView = builder.setLineSpacingMultiplier(2f)
                .setTitleBgColor(context.getResources().getColor(R.color.color_ffffff))
                .setSubmitColor(context.getResources().getColor(R.color.colorPrimary))
                .setCancelColor(context.getResources().getColor(R.color.color_9B9B9B))
                .build();
        sexView.setPicker(optionsItems);
        sexView.show();
    }

    public interface OnWalletSelected {
        void onWalletSelected(String accountType);
    }
}

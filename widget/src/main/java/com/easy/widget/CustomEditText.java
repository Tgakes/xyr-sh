package com.easy.widget;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatEditText;


/**
 * @author gakes
 * 2014-07-14
 */
public class CustomEditText extends AppCompatEditText {

    private Drawable imgDelete, imageSearch;
    boolean isShowDelete;
    boolean isShowSearch;
    boolean isGreyDeleteIcon;


    public CustomEditText(Context context) {
        super(context);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomAttributes(context, attrs);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomAttributes(context, attrs);
        init();
    }

    private void setCustomAttributes(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.EditAttrs);
        isShowDelete = array.getBoolean(R.styleable.EditAttrs_showDelete, false);
        isShowSearch = array.getBoolean(R.styleable.EditAttrs_showSearch, false);
        isGreyDeleteIcon = array.getBoolean(R.styleable.EditAttrs_greyDeleteIcon, true);
        if (isShowSearch) {
            imageSearch = array.getDrawable(R.styleable.EditAttrs_searchRes);
        }
        if (isShowDelete) {
            imgDelete = array.getDrawable(R.styleable.EditAttrs_searchDel);
        }
        array.recycle();
    }

    boolean hasFocus;

    private void init() {
        if (isShowSearch && imageSearch == null)
            imageSearch = resourceToDrawable(R.drawable.search_grey_icon);
        if (isShowDelete) {
            if (imgDelete == null) {
                imgDelete = resourceToDrawable(isGreyDeleteIcon ? R.drawable.clear_up : R.drawable.clear_up);
            }
            imgDelete.setBounds(0, 0, imgDelete.getIntrinsicWidth(), imgDelete.getIntrinsicHeight());
            setOnFocusChangeListener((view, focus) -> {
                hasFocus = focus;
                judgeShowDrawable();
            });
        }
        setListener();
        judgeShowDrawable();

    }

    public Drawable resourceToDrawable(int resource) {

        return getResources().getDrawable(resource);
    }

    TextWatcher listener;

    public void onChangedListener(TextWatcher listener) {
        this.listener = listener;
    }

    SelectionChangedListener selectionChangedListener;

    public void setSelectionChangedListener(SelectionChangedListener selectionChangedListener) {
        this.selectionChangedListener = selectionChangedListener;
    }


    public void setListener() {

        addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (listener != null) {
                    listener.onTextChanged(s, start, before, count);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                if (listener != null) {
                    listener.beforeTextChanged(s, start, count, after);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                judgeShowDrawable();
                if (listener != null) {
                    listener.afterTextChanged(s);
                }
            }
        });
    }


    //设置删除图片
    public void judgeShowDrawable() {
        if (isEnabled()) {
            Drawable[] drawables = getCompoundDrawables();
            if (getText().length() < 1) {
                if (isShowSearch) {
                    setCompoundDrawablesWithIntrinsicBounds(imageSearch, null, null, null);
                } else {
                    setCompoundDrawablesWithIntrinsicBounds(drawables != null ? drawables[0] : null, null, null, null);
                }
            } else {
                if (isShowDelete && isShowSearch) {
                    setCompoundDrawablesWithIntrinsicBounds(imageSearch, null, hasFocus ? imgDelete : null, null);
                } else if (isShowDelete) {
                    setCompoundDrawablesWithIntrinsicBounds(drawables != null ? drawables[0] : null, null, hasFocus ? imgDelete : null, null);
                } else if (isShowSearch) {
                    setCompoundDrawablesWithIntrinsicBounds(imageSearch, null, null, null);
                } else {
                    setCompoundDrawablesWithIntrinsicBounds(drawables != null ? drawables[0] : null, null, null, null);
                }
            }
        } else {
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }


    /**
     * @return null
     * @Description 是否显示删除键
     * @author chengaobin
     */
    public void setShowDelDrawable() {


        if (isShowDelete && getText().length() > 0) {

            setCompoundDrawablesWithIntrinsicBounds(null, null, imgDelete, null);
        }

        if (isShowSearch && getText().length() < 1) {

            setCompoundDrawablesWithIntrinsicBounds(imageSearch, null, null, null);
        }


    }

    // 处理删除事件
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (imgDelete != null && event.getAction() == MotionEvent.ACTION_UP) {


            boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight())
                    && (event.getX() < ((getWidth() - getPaddingRight())));

            if (touchable) {
                this.setText("");
            }

        }
        return super.onTouchEvent(event);
    }


    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (selectionChangedListener != null) {
            selectionChangedListener.onSelectionChanged(selStart, selEnd);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public interface SelectionChangedListener {

        void onSelectionChanged(int selStart, int selEnd);
    }

}

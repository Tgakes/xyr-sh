package com.easy.net.function;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

public class DataSwitchFunction<T> implements Function<ResponseBody, Response<T>> {

    Class<T> tClass;

    public DataSwitchFunction(Class<T> t) {
        this.tClass = t;
    }

    public final static String TOKEN_ERROR = "token";

    @Override
    public Response<T> apply(ResponseBody responseBody) {
        Response response;
        String result = null;
        try {
            result = responseBody.string();
            Log.e("logs", "result====" + result);
            response = JSON.parseObject(result, Response.class);
            response.setOriginalData(result);
            if (tClass != List.class) {
                response.setResultObj(onConvert(response));
            }
            if (Response.ERROR_STATUS.equals(response.getStatus()) && TOKEN_ERROR.equals(response.getMsg())) {
                LiveEventBus.get("logout", String.class).post(TOKEN_ERROR);
                response.setMsg("");
            }
        } catch (Exception e) {
            Log.d("gakes","请求数据异常e===="+e.getMessage());
            response = new Response<>();
            response.setOriginalData(result);
            response.setCode(Response.ERROR_CODE);
            response.setMsg("请求数据响应异常.");
        }
        return response;
    }

    public T onConvert(Response response) {
        if (tClass == String.class) {
            return (T) response.getResult();
        } else
            return JSON.parseObject(EmptyUtils.isNotEmpty(response.getResult()) ? response.getResult() : response.getData(), (Type) tClass);
    }
}


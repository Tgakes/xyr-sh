package com.easy.utils.bean;

import com.easy.utils.base.FileConstant;

import java.util.HashMap;
import java.util.Map;

public enum FileType {
    txt("txt", "application/octet-stream", FileConstant.TYPE_TEXT),
    bin("bin", "application/octet-stream", FileConstant.TYPE_TEXT),
    Aclass("doc", "application/octet-stream", FileConstant.TYPE_TEXT),
    doc("doc", "application/msword", FileConstant.TYPE_TEXT),
    htm("htm", "text/html", FileConstant.TYPE_TEXT),
    html("html", "text/html", FileConstant.TYPE_TEXT),
    js("js", "application/x-javascript", FileConstant.TYPE_TEXT),
    pps("pdf", "application/vnd.ms-powerpoint", FileConstant.TYPE_TEXT),
    ppt("pdf", "application/vnd.ms-powerpoint", FileConstant.TYPE_TEXT),
    pdf("pdf", "application/pdf", FileConstant.TYPE_TEXT),
    conf("conf", "text/plain", FileConstant.TYPE_TEXT),
    cpp("cpp", "text/plain", FileConstant.TYPE_TEXT),
    h("h", "text/plain", FileConstant.TYPE_TEXT),
    log("log", "text/plain", FileConstant.TYPE_TEXT),
    c("c", "text/plain", FileConstant.TYPE_TEXT),
    java("java", "text/plain", FileConstant.TYPE_TEXT),
    prop("prop", "text/plain", FileConstant.TYPE_TEXT),
    RC("rc", "text/plain", FileConstant.TYPE_TEXT),
    XML("xml", "text/plain", FileConstant.TYPE_TEXT),
    RTF("rtf", "application/rtf", FileConstant.TYPE_TEXT),
    SH("sh", "text/plain", FileConstant.TYPE_TEXT),
    TXT("txt", "text/plain", FileConstant.TYPE_TEXT),
    WPS("wps", "application/vnd.ms-works", FileConstant.TYPE_TEXT),

    APK("apk", "application/vnd.android.package-archive", FileConstant.TYPE_APP),
    exe("exe", "application/octet-stream", FileConstant.TYPE_APP),

    mpe("mpe",    "video/mpeg", FileConstant.TYPE_VIDEO),
    mpeg("mpeg",    "video/mpeg", FileConstant.TYPE_VIDEO),
    mpg("mpg",    "video/mpeg", FileConstant.TYPE_VIDEO),
    m4u("m4u",    "video/vnd.mpegurl", FileConstant.TYPE_VIDEO),
    mov("mov", "video/quicktime", FileConstant.TYPE_VIDEO),
    m4v("m4v", "video/x-m4v", FileConstant.TYPE_VIDEO),
    AVI("avi", "video/x-msvideo", FileConstant.TYPE_VIDEO),
    ASF("asf", "video/x-ms-asf", FileConstant.TYPE_VIDEO),
    ThreeGP("3gp", "video/3gpp", FileConstant.TYPE_VIDEO),
    mp4("mp4", "video/mp4", FileConstant.TYPE_VIDEO),
    mpg4("mpg4", "video/mp4", FileConstant.TYPE_VIDEO),

    mpga("mpga",    "audio/mpeg", FileConstant.TYPE_AUDIO),
    m3u("m3u",    "audio/x-mpegurl", FileConstant.TYPE_AUDIO),
    m4a("m4a",    "audio/mp4a-latm", FileConstant.TYPE_AUDIO),
    m4b("m4b",    "audio/mp4a-latm", FileConstant.TYPE_AUDIO),
    m4p("m4p",    "audio/mp4a-latm", FileConstant.TYPE_AUDIO),
    mp2("mp2", "audio/x-mpeg", FileConstant.TYPE_AUDIO),
    mp3("mp3", "audio/x-mpeg", FileConstant.TYPE_AUDIO),
    RMVB("rmvb", "audio/x-pn-realaudio", FileConstant.TYPE_AUDIO),
    WMV("wmv", "audio/x-ms-wmv", FileConstant.TYPE_AUDIO),
    WAV("wav", "audio/x-wav", FileConstant.TYPE_AUDIO),
    WMA("wma", "audio/x-ms-wma", FileConstant.TYPE_AUDIO),

    GIF("gif", "image/gif", FileConstant.TYPE_PHOTO),
    jpeg("jpeg", "image/jpeg", FileConstant.TYPE_PHOTO),
    jpg("jpg", "image/jpeg", FileConstant.TYPE_PHOTO),
    gif("gif", "image/gif", FileConstant.TYPE_PHOTO),
    bmp("bmp", "image/bmp", FileConstant.TYPE_PHOTO),
    png("png", "image/png", FileConstant.TYPE_PHOTO),


    ogg("msg", "application/vnd.ms-outlook", FileConstant.TYPE_OTHER),
    mpc("mpc", "application/vnd.mpohun.certificate", FileConstant.TYPE_OTHER),
    gtar("gtar", "application/x-gtar", FileConstant.TYPE_OTHER),
    gz("gz", "application/x-gzip", FileConstant.TYPE_OTHER),
    jar("jar", "application/java-archive", FileConstant.TYPE_OTHER),
    TGZ("tgz", "application/x-compressed", FileConstant.TYPE_OTHER),
    RAR("rar", "application/x-rar-compressed", FileConstant.TYPE_OTHER),
    TAR("tar", "application/x-tar", FileConstant.TYPE_OTHER),
    ZIP("zip", "application/zip", FileConstant.TYPE_OTHER),
    Z("z", "application/x-compress", FileConstant.TYPE_OTHER),
    OTHER("", "*/*", FileConstant.TYPE_OTHER),
    ;

    FileType(String extension, String mimeType, int type) {
        this.type = type;
        this.extension = extension;
        this.mimeType = mimeType;
    }

    private int type;
    private String extension;//扩展名
    private String mimeType;//扩展名

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * 获取所有类型
     *
     * @return
     */
    public static Map<String, FileType> getFileTypes() {
        Map<String, FileType> map = new HashMap<>();
        FileType[] fileTypes = FileType.values();
        for (FileType fileType : fileTypes) {
            map.put(fileType.getExtension(), fileType);
        }
        return map;
    }
}

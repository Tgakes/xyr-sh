package com.easy.utils;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViewUtils {

    public static void updateNum(TextView numTv, int unReadNum) {
        if (numTv == null) {
            return;
        }
        if (unReadNum < 1) {
            numTv.setText("");
            numTv.setVisibility(View.INVISIBLE);
        }
        // 不限制未读消息显示数量
        else if (unReadNum > 99) {
            numTv.setText("99+");
            numTv.setVisibility(View.VISIBLE);
        } else {
            numTv.setText(String.valueOf(unReadNum));
            numTv.setVisibility(View.VISIBLE);
        }
    }

    //只能输入一位小数点
    public static boolean inputSinglePoint(EditText editText, CharSequence s) {
        if (!EmptyUtils.isEmpty(s.toString())) {
            if (s.toString().startsWith("0") && s.toString().trim().length() > 1) {
                if (!s.toString().substring(1, 2).equals(".")) {
                    editText.setText(s.subSequence(0, 1));
                    editText.setSelection(1);
                    return true;
                }
            }
            if (isPositiveInteger(s.toString()) || isPositiveDecimal(s.toString())) {
                return false;
            } else {

                editText.setText(s.toString().substring(0, s.length() - 1));
                editText.setSelection(editText.getText().length());
                return true;
            }
        } else {
            return false;
        }
    }

    private static boolean isMatch(String regex, String orginal) {
        if (orginal == null || orginal.trim().equals("")) {
            return false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher isNum = pattern.matcher(orginal);
        return isNum.matches();
    }

    public static boolean isPositiveInteger(String orginal) {
        return isMatch("^\\+{0,1}[1-9]\\d*", orginal);
    }

    public static boolean isPositiveDecimal(String orginal) {
        return isMatch(
                "^\\+{0,1}[0]\\.[1-9]*|\\+{0,1}[1-9]\\d*\\.\\d*|^[0-9]+([.]{1}[0-9]+){0,1}$",
                orginal);
    }

    private static long sLastClickTime;

    public static boolean canClick() {
        long curTime = System.currentTimeMillis();
        if (curTime - sLastClickTime < 500) {
            return false;
        }
        sLastClickTime = curTime;
        return true;
    }

    public static boolean doubleClick() {
        long curTime = System.currentTimeMillis();
        if (curTime - sLastClickTime < 500) {
            return true;
        }
        sLastClickTime = curTime;
        return false;
    }
}

package com.easy.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.easy.utils.base.FileConstant;
import com.easy.utils.bean.FileType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {



    /**
     * 判断文件大小处于限制内
     *
     * @param fileLen  文件长度
     * @param fileSize 限制大小
     * @param fileUnit 限制的单位（B,K,M,G）
     * @return
     */
    public static boolean checkFileSizeIsLimit(Long fileLen, int fileSize, String fileUnit) {
//        long len = file.length();
        double fileSizeCom = 0;
        if ("B".equals(fileUnit.toUpperCase())) {
            fileSizeCom = (double) fileLen;
        } else if ("K".equals(fileUnit.toUpperCase())) {
            fileSizeCom = (double) fileLen / 1024;
        } else if ("M".equals(fileUnit.toUpperCase())) {
            fileSizeCom = (double) fileLen / (1024 * 1024);
        } else if ("G".equals(fileUnit.toUpperCase())) {
            fileSizeCom = (double) fileLen / (1024 * 1024 * 1024);
        }
        if (fileSizeCom > fileSize) {
            return false;
        }
        return true;

    }

    /**
     * 按分类自定义文件路径
     *
     * @param @param  type 图片为 pic 应用为 app
     * @param @return 设定文件
     * @return String    返回类型
     * @throws
     * @Title: getSaveFilePath
     * @Description:
     */
    public static String getFilePath(int saveType, Context context) {
        String filePath;
        switch (saveType) {
            case FileConstant.TYPE_TEXT:
                filePath = getRootFilePath(context) + FileConstant.SAVE_TEXT_PATH;
                break;
            case FileConstant.TYPE_PHOTO:
                filePath = getRootFilePath(context) + FileConstant.SAVE_PHOTO_PATH;
                break;
            case FileConstant.TYPE_AUDIO:
                filePath = getRootFilePath(context) + FileConstant.SAVE_AUDIO_PATH;
                break;
            case FileConstant.TYPE_VIDEO:
                filePath = getRootFilePath(context) + FileConstant.SAVE_VIDEO_PATH;
                break;
            case FileConstant.TYPE_APP:
                filePath = getRootFilePath(context) + FileConstant.SAVE_APP_PATH;
                break;
            default:
                filePath = getRootFilePath(context) + FileConstant.SAVE_OTHER_PATH;
                break;
        }
        createDirectory(filePath);
        return filePath;
    }


    public static void saveImageToGallery2(Context context, Bitmap bitmap,String msg) {
        if (bitmap == null) {
           return;
        }

        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(getFilePath(FileConstant.TYPE_PHOTO,context), fileName);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2.把文件插入到系统图库
       /* try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),file.getAbsolutePath(), fileName, null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
        // 3.通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getAbsolutePath())));

        ToastUtils.showShort(msg);
    }

    /**
     * 获取对应类型
     *
     * @param extensionName 文件扩展名
     * @param mineType      对应的mini类型
     * @return
     */
    public static int getFileType(String extensionName, String mineType) {
        Map<String, FileType> fileTypeMap = FileType.getFileTypes();
        FileType fileType = fileTypeMap.get(extensionName.toLowerCase());
        if (fileType == null) {
            fileType = FileType.OTHER;
        }
        return fileType.getType();
    }

    /**
     * 获取系统自带文件路径
     *
     * @return
     */
    public static String getSystemFilePath(Context context, String type) {
        //Environment.DIRECTORY_PICTURES : /mnt/sdcard/pictures
        File file = context.getExternalFilesDir(type);
        return file.getAbsolutePath();
    }

    /**
     * 从URL中获取扩张名
     *
     * @param url
     * @return
     */
    public static String getFileExtension(String url, String defaultStr) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (EmptyUtils.isEmpty(extension)) {
            extension = defaultStr;
        }
        return extension;
    }

    /**
     * 创建文件目录
     *
     * @param filePath
     * @return
     */
    public static boolean createDirectory(String filePath) {
        if (fileIsExist(filePath)) {
            return true;
        } else {
            File file = new File(filePath);
            return file.mkdirs();
        }
    }

    /**
     * 判断文件是否存在---不存在就创建
     *
     * @param filePath
     * @return
     */
    public static boolean createOrExistsFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) return file.isFile();
        if (!createOrExistsDir(file.getParentFile())) return false;
        try {
            return file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 判断是否是文件目录--不存在就创建
     *
     * @param file
     * @return
     */
    public static boolean createOrExistsDir(final File file) {
        return file != null && (file.exists() ? file.isDirectory() : file.mkdirs());
    }

    /**
     * 文件是否存在
     *
     * @param filePath
     * @return
     */
    public static boolean fileIsExist(String filePath) {
        if (EmptyUtils.isEmpty(filePath)) {
            return false;
        }
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    /**
     * 获取文件保存根目录
     *
     * @return 如果有sdcard 则返回sdcard根路径，
     * 如果没有则返回应用包下file目录/data/data/包名/file/
     */
    public static String getRootFilePath(Context context) {
        String strPathHead;
        if (isCanUseSD()) {
            strPathHead = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
        } else {
            strPathHead = context.getFilesDir().getPath() + File.separator;
        }
        return strPathHead;
    }

    public static boolean isCanUseSD() {
        try {
            return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            Log.e("isCanUseSD", "sdcard不可用");
        }
        return false;
    }

    // 获取文件
    //Context.getExternalFilesDir() --> SDCard/Android/data/你的应用的包名/files/ 目录，一般放一些长时间保存的数据
    //Context.getExternalCacheDir() --> SDCard/Android/data/你的应用包名/cache/目录，一般存放临时缓存数据
    public static long getFolderSize(File file) {
        long size = 0;
        if (file == null) {
            return size;
        }
        try {
            File[] fileList = file.listFiles();
            if (fileList != null && fileList.length > 0) {
                for (int i = 0; i < fileList.length; i++) {
                    // 如果下面还有文件
                    if (fileList[i].isDirectory()) {
                        size = size + getFolderSize(fileList[i]);
                    } else {
                        long fileSize = fileList[i].length();
                        Log.d("FileSize", "size: " + fileSize + " path: " + fileList[i].getAbsolutePath());
                        size = size + fileSize;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }

    /**
     * 删除文件夹/文件
     *
     * @param dir
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }

    @SuppressLint("SimpleDateFormat")
    public static String getFileLastModifiedTime(File file) {
        Calendar cal = Calendar.getInstance();
        long time = file.lastModified();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        cal.setTimeInMillis(time);
        return formatter.format(cal.getTime());
    }

    public static String formatFileSize(File file) {
        String fileSize = "0B";
        if (file == null) {
            fileSize = "文件不存在";
            return fileSize;
        }
        long fileLength = file.length();
        DecimalFormat df = new DecimalFormat("#.00");
        if (fileLength < 1024) {
            fileSize = df.format((double) fileLength) + "B";
        } else if (fileLength < 1048576) {
            fileSize = df.format((double) fileLength / 1024) + "KB";
        } else if (fileLength < 1073741824) {
            fileSize = df.format((double) fileLength / 1048576) + "MB";
        } else {
            fileSize = df.format((double) fileLength / 1073741824) + "GB";
        }
        return fileSize;
    }
    /**
     * 格式化单位
     *
     * @param size
     */
    /**
     * 格式化单位
     *
     * @param size
     * @return
     */
    public static String[] getFormatSize(double size) {
        String[] sizeStr = new String[]{"0", "K"};
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return sizeStr;
        }

        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            sizeStr[0] = result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            sizeStr[1] = "KB";
            return sizeStr;
        }

        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            sizeStr[0] = result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            sizeStr[1] = "MB";
            return sizeStr;
        }

        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            sizeStr[0] = result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            sizeStr[1] = "GB";
            return sizeStr;
        }
        BigDecimal result4 = new BigDecimal(teraBytes);
        sizeStr[0] = result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
        sizeStr[1] = "TB";
        return sizeStr;
    }

    public static String getFromRaw(Context context, int id) {
        try {
            //获取文件中的内容
            InputStream inputStream = context.getResources().openRawResource(id);
            //将文件中的字节转换为字符
            InputStreamReader isReader = new InputStreamReader(inputStream, "UTF-8");
            //使用bufferReader去读取字符
            BufferedReader reader = new BufferedReader(isReader);
            StringBuilder builder = new StringBuilder();
            try {
                String out;
                while ((out = reader.readLine()) != null) {
                    builder.append(out);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 写文件
     *
     * @param saveFile
     * @param content
     * @return
     */
    public static boolean writeStringToFile(String saveFile, String content) {
        try {
            boolean createFile = createOrExistsFile(saveFile);
            if (createFile) {
                File file = new File(saveFile);
                FileOutputStream output = new FileOutputStream(file);
                output.write(content.getBytes());
                output.flush();
                output.close();
            } else {
                return false;
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void writeFile(InputStream in, FileOutputStream outs) throws Exception {
        byte[] buffer = new byte[1024];
        int length;
        while ((length = in.read(buffer)) > 0) {
            outs.write(buffer, 0, length);
        }
        outs.flush();
        outs.close();
        in.close();
    }

    public static ZipInputStream getFileFromZip(InputStream zipFileStream) throws Exception {
        ZipInputStream zis = new ZipInputStream(zipFileStream);
        ZipEntry ze;
        while ((ze = zis.getNextEntry()) != null) {
            Log.w("AssetUtil", "extracting file: '" + ze.getName() + "'...");
            return zis;
        }
        return null;
    }

    /**
     * 根据路径打开文件
     *
     * @param context 上下文
     * @param path    文件路径
     */
    public static void openFile(Context context, String path) {
        if (context == null || path == null)
            return;
        Intent intent = new Intent();
        //设置intent的Action属性
        intent.setAction(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_DEFAULT);

        //文件的类型
        String type = "";
        Map<String, FileType> fileTypeMap = FileType.getFileTypes();
        String extension = FileUtils.getFileExtension(path, "");
        FileType fileType = fileTypeMap.get(extension.toLowerCase());
        if (fileType == null) {
            fileType = FileType.OTHER;
        }
        type = fileType.getMimeType();
        try {
            File out = new File(path);
            Uri fileURI;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fileURI = FileProvider.getUriForFile(context, SystemUtils.getAuthority(context), out);
            } else {
                fileURI = Uri.fromFile(out);
            }
            //设置intent的data和Type属性
            intent.setDataAndType(fileURI, type);
            //跳转
            if (context.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "没有找到对应的程序", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) { //当系统没有携带文件打开软件，提示
            Toast.makeText(context, "无法打开该格式文件", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}

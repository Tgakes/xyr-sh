package com.easy.store.bean;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Accounts implements Serializable {
    @Id
    private long sqlId;//数据库ID


    private String uid;//imID 用户识别IM账号
    private String apptoken;//Token 成功登录才有
    private String imtoken;//应用token
    private String display;//用户名
    private String images;
    private int rank;//认证类型
    private int info;//0普通 1 承兑商
    private int paywd;//0未设置 1设置过

    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public int getPaywd() {
        return paywd;
    }

    public void setPaywd(int paywd) {
        this.paywd = paywd;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public long getSqlId() {
        return sqlId;
    }

    public void setSqlId(long sqlId) {
        this.sqlId = sqlId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getApptoken() {
        return apptoken;
    }

    public void setApptoken(String apptoken) {
        this.apptoken = apptoken;
    }

    public String getImtoken() {
        return imtoken;
    }

    public void setImtoken(String imtoken) {
        this.imtoken = imtoken;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}

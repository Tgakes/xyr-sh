package com.easy.store.dao;

import com.easy.store.bean.Accounts;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;

public class AccountsDao extends BaseDao {

    Box<Accounts> accountsBox;
    private ObjectBoxLiveData<Accounts> accountsLiveData;

    Accounts accounts;

    @Inject
    public AccountsDao() {
        if (boxStore != null) {
            accountsBox = boxStore.boxFor(Accounts.class);
        }
        accounts = getAccounts();
    }

    public ObjectBoxLiveData<Accounts> getAccountsLiveData() {
        if (accountsLiveData == null) {
            accountsLiveData = new ObjectBoxLiveData<>(accountsBox.query().build());
        }
        return accountsLiveData;
    }

    public void add(Accounts accounts) {
        accountsBox.removeAll();
        accountsBox.put(accounts);
    }

    public void delete() {
        accountsBox.removeAll();
    }

    public void update(Accounts accounts) {
        accountsBox.put(accounts);
    }

    public Accounts getAccounts() {

        return accounts == null ? queryAccounts() : accounts;
    }

    public Accounts queryAccounts() {

        return accountsBox.query().build().findFirst();
    }


    public String getAppToken() {


        return accounts != null ? accounts.getApptoken() : "";
    }

    public String getUserId() {


        return accounts != null ? accounts.getUid() : "";
    }

    public void setUpdatePayPwd() {
        if (!isSetPwd()) {
            accounts.setPaywd(1);
            update(accounts);
        }
    }


    public boolean isSetPwd() {


        return accounts != null ? accounts.getPaywd() == 1 : false;
    }

    public boolean isAcceptor() {


        return accounts != null ? accounts.getInfo() == 1 : false;
    }


    public String getAvatar() {

        Accounts accounts = getAccounts();

        return accounts != null ? accounts.getImages() : "";
    }


    public boolean isLogin() {
        return getAccounts() != null;
    }
}

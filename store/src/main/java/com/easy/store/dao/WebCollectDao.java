package com.easy.store.dao;

import android.text.TextUtils;

import com.easy.store.bean.WebCollect;
import com.easy.store.bean.WebCollect_;

import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;

public class WebCollectDao extends BaseDao {
    Box<WebCollect> collectBox;
    private ObjectBoxLiveData<WebCollect> collectLiveData;

    @Inject
    public WebCollectDao() {
        if (boxStore != null) {
            collectBox = boxStore.boxFor(WebCollect.class);
        }
    }

    public ObjectBoxLiveData<WebCollect> getCollectLiveData() {
        if (collectLiveData == null) {
            collectLiveData = new ObjectBoxLiveData<>(collectBox.query().build());
        }
        return collectLiveData;
    }

    /**
     * 添加收藏
     *
     * @param collect
     * @return true :添加成功 false:添加失败（已存在）
     */
    public boolean add(WebCollect collect) {
        if (isCollect(collect.getUrl())) {
            return false;
        } else {
            collectBox.put(collect);
            return false;
        }
    }

    public List<WebCollect> queryAll() {
        return collectBox.query().build().find();
    }

    public boolean isCollect(String url) {
        if (TextUtils.isEmpty(url)) {
            return true;
        }
        WebCollect webCollect = collectBox.query().equal(WebCollect_.url, url).build().findFirst();
        return webCollect != null;
    }

    public boolean deleteCollect(String url) {
        WebCollect webCollect = collectBox.query().equal(WebCollect_.url, url).build().findFirst();
        if (webCollect != null) {
            collectBox.remove(webCollect);
            return true;
        }
        return false;
    }

    public boolean deleteCollectAll() {
        collectBox.removeAll();
        return true;
    }
}

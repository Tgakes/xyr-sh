/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.push.huawei;

import android.util.Log;

import com.huawei.hms.push.HmsMessageService;
import com.huawei.hms.push.RemoteMessage;

import cn.wildfirechat.PushType;
import cn.wildfirechat.remote.ChatManager;

public class HwPushService extends HmsMessageService {
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("logs", "onNewToken: " + s);
        ChatManager.Instance().setDeviceToken(s, PushType.HMS);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null) {
            Log.e("logs", "接收到华为的透传消息为空");
            return;
        }

        Log.i("logs","remoteMessage data======"+remoteMessage.getData());
//      ChatManager.Instance()
        // do nothing
        // 山海IM采用的是透传，只需将主进程拉起即可，主进程会去拉取消息，并显示通知
        // 手机设置：
        // 1. 应用权限管理里面，需要允许自启动、运行后台活动
        // 2. 通知管理，允许通知，在状态栏显示，横幅，允许打扰
    }
}

/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfirechat.push;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cn.wildfirechat.remote.ChatManager;

class DefaultPushMessageHandler extends PushMessageHandler {
    @Override
    public void handleIMPushMessage(Context context, AndroidPushMessage pushMessage, PushService.PushServiceType pushServiceType) {
        // do nothing， 透传消息的receiver都在主进程执行，当有透传消息时，会启动主进程，然后主进程主动去拉取消息，
        // 并处理
//        Intent intent = new Intent();
//        intent.setAction(PushConstant.PUSH_MESSAGE_ACTION);
//        intent.putExtra(PushConstant.PUSH_MESSAGE, pushMessage);
//        context.sendBroadcast(intent);
//        ChatManager.Instance().forceConnect();
        Log.i("logs","pushMessage===="+pushMessage);

    }

    //Application push data
    @Override
    public void handlePushMessageData(Context context, String pushData) {
        Log.i("logs","pushData===="+pushData);
    }
}

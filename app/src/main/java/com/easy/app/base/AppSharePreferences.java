package com.easy.app.base;


import androidx.annotation.Keep;

import com.easy.apt.annotation.sp.Clear;
import com.easy.apt.annotation.sp.Default;
import com.easy.apt.annotation.sp.Expired;
import com.easy.apt.annotation.sp.Preferences;
import com.easy.apt.annotation.sp.Remove;

@Preferences(name = "defaultSp")
@Keep//keep 避免混淆
public interface AppSharePreferences {

    String getUserName();

    void setUserName(String userName);


    @Default("0")
    int getCoinType();

    void setCoinType(int coinType);


    /**
     * 自启动模式对话框引导 是否显示
     *
     * @return
     */
    @Default("false")
    boolean isShowSelfStart();

    /**
     *
     *
     * @param showSelfStart
     */
    void setShowSelfStart(boolean showSelfStart);



    @Default("true")
    @Expired(value = 5, unit = Expired.UNIT_SECONDS)
    boolean isGoGuide();

    void setGoGuide(boolean go);

    @Clear
    void clear();

    @Remove
    void removeUsername();

    /**
     * 设置搜索引擎
     *
     * @param engineName
     */
    void setSearchEngine(String engineName);

}

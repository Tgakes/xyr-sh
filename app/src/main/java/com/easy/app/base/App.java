package com.easy.app.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;


import com.easy.app.BuildConfig;
import com.easy.app.R;
import com.easy.framework.base.BaseApplication;
import com.easy.net.UrlConstant;
import com.easy.net.retrofit.RetrofitConfig;
import com.easy.store.dao.AccountsDao;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;

import java.util.HashMap;
import java.util.Map;

import cn.wildfire.chat.kit.ChatManagerHolder;
import cn.wildfire.chat.kit.WfcUIKit;
import cn.wildfire.chat.kit.conversation.message.viewholder.MessageViewHolderManager;
import cn.wildfire.chat.kit.conversation.message.viewholder.TransferMoneyMessageContentViewHolder;
import cn.wildfire.chat.kit.third.location.viewholder.LocationMessageContentViewHolder;
import cn.wildfirechat.message.TransferMoneyMessageContent;
import cn.wildfirechat.push.PushService;
import cn.wildfirechat.remote.ChatManager;

public class App extends BaseApplication {


    static {//static 代码段可以防止内存泄露
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator((context, layout) -> {
            layout.setPrimaryColorsId(R.color.transparent, R.color.color_333333);//全局设置主题颜色
            return new ClassicsHeader(context).setSpinnerStyle(SpinnerStyle.Translate);//指定为经典Header，默认是 贝塞尔雷达Header
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator((context, layout) -> {
            //指定为经典Footer，默认是 BallPulseFooter
            return new ClassicsFooter(context).setSpinnerStyle(SpinnerStyle.Translate);
        });
    }

    public static final String LOG_NAME = "logs";

    @Override
    protected void initBaseConfig(RetrofitConfig.Builder builder) {
        Map<String, String> hostMap = new HashMap<>();
        hostMap.put("meetone", "https://www.ethte.com");
        builder.baseUrl(UrlConstant.API_ADDRESS)
                .supportCookies(true)
                .supportHostGroup(hostMap)
                .forceCache(true, 600);
    }

    @Override
    public void initOnThread() {

    }

    @Override
    public void initOnMainThread() {

//        int launchCount = PreferenceUtils.getInt(this, Constants.APP_LAUNCH_COUNT, 0);// 记录app启动的次数
//        PreferenceUtils.putInt(this, Constants.APP_LAUNCH_COUNT, ++launchCount);

        SkinUtils.setDefaultSkins(new int[]{R.string.skin_qian_dou_green, R.string.skin_Qing_Shui_blue, R.string.skin_Shan_Hu_Hong,
                R.string.skin_Liu_Xia_Fen, R.string.skin_Dan_Gu_green, R.string.skin_pu_tao_zi, R.string.skin_Shang_Wu_lan,
                R.string.jing_dian_hong, R.string.jing_dian_lan});

        // 只在主进程初始化，否则会导致重复收到消息
        if (getCurProcessName(this).equals(BuildConfig.APPLICATION_ID)) {
            // 如果uikit是以aar的方式引入 ，那么需要在此对Config里面的属性进行配置，如：
            // Config.IM_SERVER_HOST = "im.example.com";
            WfcUIKit wfcUIKit = WfcUIKit.getWfcUIKit();
            wfcUIKit.init(this);
            wfcUIKit.setAppServiceProvider(AppService.Instance());
            PushService.init(this, BuildConfig.APPLICATION_ID);
            MessageViewHolderManager.getInstance().registerMessageViewHolder(LocationMessageContentViewHolder.class, R.layout.conversation_item_location_send, R.layout.conversation_item_location_send);
//            MessageViewHolderManager.getInstance().registerMessageViewHolder(TransferMoneyMessageContentViewHolder.class, cn.wildfire.chat.kit.R.layout.conversation_item_transfer_send, cn.wildfire.chat.kit.R.layout.conversation_item_transfer_receive);
            ChatManager.Instance().registerMessageContent(TransferMoneyMessageContent.class);
        }


    }



    public void logout(AccountsDao accountsDao,Activity activity,boolean go) {

        if (accountsDao != null) {
            //不要清除session，这样再次登录时能够保留历史记录。如果需要清除掉本地历史记录和服务器信息这里使用true
            ChatManagerHolder.gChatManager.disconnect(true, false);
            accountsDao.delete();
            PreferenceUtils.clearData(App.getInst());
            if (go) {
                RouterManager.goLoginActivity(activity);
            }

        }

    }


    public String getCurProcessName(Context context) {

        int pid = android.os.Process.myPid();

        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {

            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }


}

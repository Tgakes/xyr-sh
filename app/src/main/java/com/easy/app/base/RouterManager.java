package com.easy.app.base;

import android.app.Activity;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.qrcode.ui.qr_code.QrScanActivity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cn.wildfirechat.model.UserInfo;

public class RouterManager {
    public static final String ROUTER_HOME = "/app/";
    public static final String WEB_ACTIVITY = ROUTER_HOME + "WebActivity";
    public static final String HOME_ACTIVITY = ROUTER_HOME + "HomeActivity";
    public static final String LOCAL_FILE_ACTIVITY = ROUTER_HOME + "LocalFileActivity";
    public static final String EMPTY_ACTIVITY = ROUTER_HOME + "EmptyActivity";
    public static final String LOGIN_ACTIVITY = ROUTER_HOME + "LoginActivity";
    public static final String REGISTER_ACTIVITY = ROUTER_HOME + "RegisterActivity";
    public static final String FIND_PWD_ACTIVITY = ROUTER_HOME + "FindPwdActivity";
    public static final String SPLASH_ACTIVITY = ROUTER_HOME + "SplashActivity";
    public static final String SETTING_ACTIVITY = ROUTER_HOME + "SettingActivity";
    public static final String ABOUT_US_ACTIVITY = ROUTER_HOME + "AboutUsActivity";
    public static final String MY_WALLET_ACTIVITY = ROUTER_HOME + "MyWalletActivity";
    public static final String MY_WALLET_OLD_ACTIVITY = ROUTER_HOME + "MyWalletOldActivity";
    public static final String SET_PAY_PWD = ROUTER_HOME + "SetPayPwdActivity";
    public static final String TRANSFER_MONEY = ROUTER_HOME + "TransferMoneyActivity";
    public static final String PAY_CENTER = ROUTER_HOME + "PayCenterActivity";
    public static final String BILL = ROUTER_HOME + "BillActivity";
    public static final String RESET_PAY_PWD = ROUTER_HOME + "ResetPayPwdActivity";
    public static final String CHANGE_PWD = ROUTER_HOME + "ChangePwdActivity";
    public static final String PAYMENT_OR_RECEIPT = ROUTER_HOME + "PaymentOrReceiptActivity";
    public static final String RECEIPT_SET_MONEY = ROUTER_HOME + "ReceiptSetMoneyActivity";
    public static final String BANK_CARD_MANAGE = ROUTER_HOME + "BankCardManageActivity";
    public static final String ADD_BANK = ROUTER_HOME + "AddBankActivity";
    public static final String RECEIPT_PAY_MONEY = ROUTER_HOME + "ReceiptPayMoneyActivity";
    public static final String CHARGE_MONEY = ROUTER_HOME + "ChargeMoneyActivity";
    public static final String WITHDRAW_MONEY = ROUTER_HOME + "WithdrawMoneyActivity";
    public static final String EXCHANGE = ROUTER_HOME + "ExchangeActivity";
    public static final String SELL_COINS = ROUTER_HOME + "SellCoinsActivity";
    public static final String COUNTRY_CODE = ROUTER_HOME + "CountryCodeActivity";

    public static void goCountryCode() {
        ARouter.getInstance().build(COUNTRY_CODE).navigation();
    }


    public static void goQrScan() {
        ARouter.getInstance().build(QrScanActivity.QR_SCAN_ACTIVITY).navigation();
    }


    public static void goSellCoins(String market) {
        ARouter.getInstance().build(SELL_COINS).withString("market", market).navigation();
    }

    public static void goExchange() {
        ARouter.getInstance().build(EXCHANGE).navigation();
    }

    public static void goWithdrawMoney() {
        ARouter.getInstance().build(WITHDRAW_MONEY).navigation();
    }


    public static void goChargeMoney() {
        ARouter.getInstance().build(CHARGE_MONEY).navigation();
    }


    public static void goReceiptPayMoneyActivity(int type, String receiptPayMoney) {
        ARouter.getInstance().build(RECEIPT_PAY_MONEY)
                .withInt("type", type)
                .withString("receiptPayMoney", receiptPayMoney).navigation();
    }

    public static void goAddBankActivity() {
        ARouter.getInstance().build(ADD_BANK).navigation();
    }


    public static void goBankCardManageActivity(int skipPage) {

        ARouter.getInstance().build(BANK_CARD_MANAGE).withInt("skipPage", skipPage).navigation();
    }

    public static void goReceiptSetMoneyActivity(String userId) {
        ARouter.getInstance().build(RECEIPT_SET_MONEY).withString("userId", userId).navigation();
    }

    public static void goPaymentOrReceiptActivity() {
        ARouter.getInstance().build(PAYMENT_OR_RECEIPT).navigation();
    }

    public static void goChangePwdActivity() {
        ARouter.getInstance().build(CHANGE_PWD).navigation();
    }

    public static void goResetPayPwdActivity() {
        ARouter.getInstance().build(RESET_PAY_PWD).navigation();
    }


    public static void goBillActivity() {
        ARouter.getInstance().build(BILL).navigation();
    }


    public static void goPayCenterActivity() {
        ARouter.getInstance().build(PAY_CENTER).navigation();
    }

    public static void goTransferMoneyActivity(String transferInfo, String action, int type) {
        ARouter.getInstance().build(TRANSFER_MONEY)
                .withInt("type", type)
                .withString("action", action)
                .withString("transferInfo", transferInfo).navigation();
    }

    public static void goSetPayPwd(int pageFlag, String newPwd, String oldPwd) {
        ARouter.getInstance().build(SET_PAY_PWD)
                .withInt("pageFlag", pageFlag)
                .withString("newPwd", newPwd)
                .withString("oldPwd", oldPwd)
                .navigation();
    }

    public static void goMyWalletActivity(UserInfo userInfo) {
        ARouter.getInstance().build(MY_WALLET_ACTIVITY).withParcelable("userInfo", userInfo).navigation();
    }

    public static void goAboutUsActivity() {
        ARouter.getInstance().build(ABOUT_US_ACTIVITY).navigation();
    }

    public static void goSettingActivity() {
        ARouter.getInstance().build(SETTING_ACTIVITY).navigation();
    }

    public static void goSplashActivity(Activity activity) {
        ARouter.getInstance().build(SPLASH_ACTIVITY)
                .navigation(activity, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {
                        if (!activity.isFinishing()) {
                            activity.finish();
                        }
                    }
                });
    }

    public static void goHomeActivity(Activity activity) {
        ARouter.getInstance().build(HOME_ACTIVITY)
                .navigation(activity, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {
                        if (!activity.isFinishing()) {
                            activity.finish();
                        }
                    }
                });
    }

    public static void goFindPwdActivity() {
        ARouter.getInstance().build(FIND_PWD_ACTIVITY).navigation();
    }

    public static void goRegisterActivity() {
        ARouter.getInstance().build(REGISTER_ACTIVITY).navigation();
    }

    public static void goLoginActivity(Activity activity) {
        ARouter.getInstance().build(LOGIN_ACTIVITY)
                .navigation(activity, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {

                        if (!activity.isFinishing()) {
                            activity.finish();
                        }
                    }
                });
    }


    public static void goLocalFileActivity(int type) {
        ARouter.getInstance().build(LOCAL_FILE_ACTIVITY).withInt("type", type).navigation();
    }


    public static void goWebActivity(@Nonnull String url, @Nullable String title) {
        ARouter.getInstance().build(WEB_ACTIVITY)
                .withString("url", url)
                .withString("title", title).navigation();
    }


}

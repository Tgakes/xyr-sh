package com.easy.app.util;

import android.util.Log;


import com.orhanobut.logger.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

public class RsaUtils {

    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCIJ30BD0WszkExvrB088QzF0sQeZAPdUS2ySZOGh4qAzU9X1lNdhf/SSS7z6cKLsZHRNV3LgRPaJ9N/f6z3RUTrIJfsfL4+S7JEpJVVJBNErm5nS5nHXL1JVCYE1qtuPLd9jNaF8bPDtFU9wUorMTWDgqyYafS6URDBocYFVksgwIDAQAB";
    public static final String PRIVATE_KEY = "private_key";

    // 返回 RSA 加密的结果
    public static String encryptRSA(String text) {
        try {
            Cipher rsa = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            PublicKey pubkey = getPublicKeyFromX509(PUBLIC_KEY);
            rsa.init(Cipher.ENCRYPT_MODE, pubkey);
            byte[] originBytes = text.getBytes();
            //大于117时进行分段 加密
            int subLength = originBytes.length / 117 + (originBytes.length % 117 == 0 ? 0 : 1);
            byte[] finalByte = new byte[128 * subLength];
            for (int i = 0; i < subLength; i++) {
                //需要加密的字节长度
                int len = i == subLength - 1 ? (originBytes.length - i * 117) : 117;
                //加密完成的字节数组
                byte[] doFinal = rsa.doFinal(originBytes, i * 117, len);
                //复制这次加密的数组
                System.arraycopy(doFinal, 0, finalByte, i * 128, doFinal.length);
            }
            return Base64Utils.encode(finalByte);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 返回 RSA 解密的结果
     */
    public static String decryptRSA(Key privateKey, String content) {
        try {
            byte[] text = Base64Utils.decode(content);
            Cipher rsa = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            rsa.init(Cipher.DECRYPT_MODE, privateKey);
            //大于128时进行分段 解密
            int subLength = text.length / 128;
            StringBuilder finalString = new StringBuilder();
            for (int i = 0; i < subLength; i++) {
                byte[] doFinal = rsa.doFinal(text, i * 128, 128);
                finalString.append(new String(doFinal, Charset.forName("UTF-8")));
            }
            return finalString.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //根据文件路径返回公匙
    public static PublicKey readPublicKey(String filePath) throws IOException {
        FileInputStream fis = new FileInputStream(new File(filePath));
        return readPublicKey(fis);
    }

    // 根据输入流返回公匙
    public static PublicKey readPublicKey(InputStream input) throws IOException {
     /*   final ByteArrayOutputStream output = new ByteArrayOutputStream();
        int n = 0;
        final byte[] buffer = new byte[1024 * 4];
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        byte[] keyBytes = output.toByteArray();
        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(Base64Utils.encode(keyBytes));
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }*/
        return null;
    }

    // 根据文件路径返回私匙
    public static PrivateKey readPrivateKey(String filePath) throws IOException {
        FileInputStream fis = new FileInputStream(new File(filePath));
        return readPrivateKey(fis);
    }

    // 根据输入流返回私匙
    public static PrivateKey readPrivateKey(InputStream input) throws IOException {
      /*  final ByteArrayOutputStream output = new ByteArrayOutputStream();
        int n = 0;
        final byte[] buffer = new byte[1024 * 4];
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
        byte[] keyBytes = output.toByteArray();
        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(Base64Utlis.decode(keyBytes, Base64Utlis.NO_WRAP));
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }*/
        return null;
    }




    public static Map<String, Key> geration() {
        KeyPairGenerator keyPairGenerator;
        try {
            Map<String, Key> map = new HashMap<>();
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            SecureRandom secureRandom = new SecureRandom(new Date().toString().getBytes());
            keyPairGenerator.initialize(1024, secureRandom);
            KeyPair keyPair = keyPairGenerator.genKeyPair();

            byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
            X509EncodedKeySpec xks = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(xks);
            map.put(PUBLIC_KEY, publicKey);

            byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();
            PKCS8EncodedKeySpec pks = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey privateKey = kf.generatePrivate(pks);
            map.put(PRIVATE_KEY, privateKey);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decode(Key privKey, String content) {
        //开始解密
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] cipherText = cipher.doFinal(content.getBytes());
            byte[] plainText = cipher.doFinal(cipherText);
            return new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void test() {
        Map<String, Key> keyMap = geration();
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            Key pubKey = keyMap.get(PUBLIC_KEY);
            Key privKey = keyMap.get(PRIVATE_KEY);

            String input = "!!!hello world!!!";
            Log.d("cipher: ", "原始:" + input);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] cipherText = cipher.doFinal(input.getBytes());
            //加密后的东西
            Log.d("cipher: ", "加密后:" + new String(cipherText));
            //开始解密
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] plainText = cipher.doFinal(cipherText);
            Log.d("cipher: ", "解密后:" + new String(plainText));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }



















    /**
     *  使用私钥解密
     * @param content
     * @param private_key
     * @param input_charset
     * @return
     * @throws Exception
     */
    public static String decrypt(String content, String private_key, String input_charset) throws Exception {
        PrivateKey prikey = getPrivateKey(private_key);

        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, prikey);

        InputStream ins = new ByteArrayInputStream(Base64Utils.decode(content));
        ByteArrayOutputStream writer = new ByteArrayOutputStream();
        byte[] buf = new byte[128];
        int bufl;

        while ((bufl = ins.read(buf)) != -1) {
            byte[] block = null;

            if (buf.length == bufl) {
                block = buf;
            } else {
                block = new byte[bufl];
                for (int i = 0; i < bufl; i++) {
                    block[i] = buf[i];
                }
            }

            writer.write(cipher.doFinal(block));
        }

        return new String(writer.toByteArray(), input_charset);
    }

    /**
     * 获得私钥
     *
     * @param key
     *            私钥
     * @throws Exception
     */
    public static PrivateKey getPrivateKey(String key) throws Exception {

        byte[] keyBytes;

        keyBytes = Base64Utils.decode(key);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

        return privateKey;
    }

    /**
     * 得到公钥
     *
     * @param bysKey
     * @return
     */
    private static PublicKey getPublicKeyFromX509(String bysKey) throws Exception {
        byte[] decodedKey = Base64Utils.decode(bysKey);
        X509EncodedKeySpec x509 = new X509EncodedKeySpec(decodedKey);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(x509);
    }

    /**
     * 使用公钥加密
     *
     * @param content 密文
     * @param pub_key 公钥
     * @return
     */
    public static String encryptByPublic(String content, String pub_key) {
        try {
            PublicKey pubkey = getPublicKeyFromX509(pub_key);

            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubkey);

            byte plaintext[] = content.getBytes("UTF-8");
            byte[] output = cipher.doFinal(plaintext);


            return Base64Utils.encode(output);

        } catch (Exception e) {
            Logger.e("logs","加密失败e====="+e.getMessage());
            return null;
        }
    }





    /** *//**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;
    /**
     * RSA算法
     */
    public static final String RSA = "RSA";

    public static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";


    /**
     * 先base64解码，再RSA公钥解密
     * content 要解密的字符串
     * */
    public static String decryptByPublicKey(String data, String publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
            PublicKey pubKey = getPublicKeyFromX509(publicKey);
            cipher.init(Cipher.DECRYPT_MODE, pubKey);
            byte[] plaintext = cipher.doFinal(Base64Utils.decode(data));
            return new String(plaintext);
        } catch (Exception e) {
            return null;
        }
    }











}

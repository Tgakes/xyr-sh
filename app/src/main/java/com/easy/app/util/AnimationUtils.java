package com.easy.app.util;

import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

/**
 * 动画类工具
 * Created by Administrator on 2016/10/31.
 */
public class AnimationUtils {
    //旋转动画
    public static Animation getAnimation(){
        Animation rotateAnimation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        rotateAnimation.setFillAfter(true);
        rotateAnimation.setDuration(2000);
        rotateAnimation.setRepeatCount(1000);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        return rotateAnimation;
    }
}

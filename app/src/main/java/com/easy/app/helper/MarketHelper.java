package com.easy.app.helper;

import android.app.Activity;
import android.content.Intent;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.easy.app.R;
import com.easy.framework.dialog.GodDialog;
import com.easy.store.dao.AccountsDao;
import com.easy.utils.EmptyUtils;

import javax.inject.Inject;

import cn.wildfire.chat.kit.conversation.ConversationActivity;
import cn.wildfirechat.model.Conversation;
import dagger.Lazy;


public class MarketHelper {



    private MarketHelper() {
    }

    public static void goConversation(Activity activity,String userId) {

        if (new AccountsDao().getUserId().equals(userId)) {
            GodDialog otherDeviceLoginDialog = new GodDialog.Builder(activity)
                    .setMessage(activity.getString(R.string.not_support_send_msg))
                    .setOneButton(activity.getString(R.string.ok), (dialog, which) -> {
                        dialog.dismiss();
                    })
                    .build();
            otherDeviceLoginDialog.show();
            return;
        }

        Intent intent = new Intent(activity, ConversationActivity.class);
        Conversation conversation = new Conversation(Conversation.ConversationType.Single, userId, 0);
        intent.putExtra("conversation", conversation);
        activity.startActivity(intent);

    }
}

package com.easy.app.helper;

import java.util.Random;

public class RandomHelper {

    public static String getOrderId() {


        return getRandomString(4) + System.currentTimeMillis();
    }


    //length用户要求产生字符串的长度
    public static String getRandomString(int length) {
        String str = "0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(10);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}

package com.easy.app.helper;

import android.util.Log;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.utils.EmptyUtils;
import com.easy.utils.FileUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.base.FileConstant;

import java.io.File;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class UploadHelp {


    public static void uploadDeal(String localPath, PictureListener listener) {

        File file = new File(localPath);
        if (file.exists()) {
            if (!FileUtils.checkFileSizeIsLimit(file.length(), 300, "K")) {//限制300k
                final String cutPathNew = FileUtils.getFilePath(FileConstant.TYPE_PHOTO, App.getInst());
                Luban.with(App.getInst())
                        .load(file)
                        .ignoreBy(300)
                        .setTargetDir(cutPathNew)
                        .filter(path -> !(EmptyUtils.isEmpty(path) || path.toLowerCase().endsWith(".gif")))
                        .setCompressListener(new OnCompressListener() {
                            @Override
                            public void onStart() {
                                // TODO 压缩开始前调用，可以在方法内启动 loading UI
                                if (listener != null) {
                                    listener.uploadStatus(0, null);
                                }
                            }

                            @Override
                            public void onSuccess(File allFile) {
                                // TODO 压缩成功后调用，返回压缩后的图片文件
                                if (listener != null) {
                                    listener.uploadStatus(1, allFile);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                // TODO 当压缩过程出现问题时调用
                                Log.i("logs", "e====" + e.getMessage());
                                if (listener != null) {
                                    listener.uploadStatus(-1, null);
                                }
                                ToastUtils.showShort(App.getInst().getString(R.string.picture_compress_error));
                            }
                        }).launch();

            } else {
                if (listener != null) {
                    listener.uploadStatus(1, file);
                }
            }

        } else {
            if (listener != null) {
                listener.uploadStatus(-2, null);
            }
        }
    }


    public interface PictureListener {

        //-2 文件不存在 -1压缩出现异常 0开始压缩 1可进行请求上传
        void uploadStatus(int status, File file);

    }
}

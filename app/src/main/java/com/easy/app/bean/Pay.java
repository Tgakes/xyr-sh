package com.easy.app.bean;

import java.io.Serializable;

public class Pay implements Serializable {

    public static final int TRANSFER = 1;

    String action;

    //1:转账
    int payStyle;

    String data;

    public Pay(String action, String data, int payStyle) {
        this.action = action;
        this.payStyle = payStyle;
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public int getPayStyle() {
        return payStyle;
    }

    public String getData() {
        return data;
    }
}

package com.easy.app.bean;

public class MarketNotice {

    String title;

    public MarketNotice(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

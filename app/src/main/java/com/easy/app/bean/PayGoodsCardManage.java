package com.easy.app.bean;

public class PayGoodsCardManage {


    /**
     * bank : 兴业银行
     * bankform : 观音山支行
     * carname : 陈高宾
     * carnumber : 622908123067945448
     */
    private String id;
    private String bank;
    private String bankform;
    private String carname;
    private String carnumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankform() {
        return bankform;
    }

    public void setBankform(String bankform) {
        this.bankform = bankform;
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }
}

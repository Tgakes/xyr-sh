/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package com.easy.app.bean;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.wildfire.chat.kit.utils.PinyinUtils;
import cn.wildfirechat.remote.ChatManager;

public class UICountryCode {
    private String category = "";
    // 用来排序的字段
    private String sortName;
    private boolean showCategory;
    private CountryCode countryCode;


    public UICountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setShowCategory(boolean showCategory) {
        this.showCategory = showCategory;
    }

    public String getCategory() {
        return category;
    }

    public boolean isShowCategory() {
        return showCategory;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String displayNamePinyin) {
        this.sortName = displayNamePinyin;
    }


    public static UICountryCode fromCountryCode(CountryCode countryCode) {
        UICountryCode info = new UICountryCode(countryCode);
        String indexLetter;
        String displayName = countryCode.getCnKey();
        if (!TextUtils.isEmpty(displayName)) {
            String pinyin = PinyinUtils.getPinyin(displayName);
            char c = pinyin.toUpperCase().charAt(0);
            if (c >= 'A' && c <= 'Z') {
                indexLetter = c + "";
                info.setSortName(pinyin);
            } else {
                indexLetter = "#";
                // 为了让排序排到最后
                info.setSortName("{" + pinyin);
            }
            info.setCategory(indexLetter);
        } else {
            info.setSortName("");
        }
        return info;
    }



    public static List<UICountryCode> fromCountryCodes(List<CountryCode> countryCodes) {
        if (countryCodes != null && !countryCodes.isEmpty()) {
            List<UICountryCode> uiCountryCodes = new ArrayList<>(countryCodes.size());
            String indexLetter;
            for (CountryCode countryCode : countryCodes) {
                uiCountryCodes.add(fromCountryCode(countryCode));
            }
            Collections.sort(uiCountryCodes, (o1, o2) -> o1.getSortName().compareToIgnoreCase(o2.getSortName()));
            String preIndexLetter = null;
            for (UICountryCode info : uiCountryCodes) {
                indexLetter = info.getCategory();
                if (preIndexLetter == null || !preIndexLetter.equals(indexLetter)) {
                    info.setShowCategory(true);
                }
                preIndexLetter = indexLetter;
            }

            return uiCountryCodes;
        } else {
            return null;
        }
    }
}

package com.easy.app.bean;

public class RotationModule {

    int res;

    public RotationModule(int res) {
        this.res = res;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }
}

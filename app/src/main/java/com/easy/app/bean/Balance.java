package com.easy.app.bean;

public class Balance {

    String usdt;

    String sh;

    String rate;

    String utoh;//U兑换h手续费

    String htou;//h兑换u手续费


    public String getUsdt() {
        return usdt;
    }

    public void setUsdt(String usdt) {
        this.usdt = usdt;
    }

    public String getSh() {
        return sh;
    }

    public void setSh(String sh) {
        this.sh = sh;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


    public String getUtoh() {
        return utoh;
    }

    public void setUtoh(String utoh) {
        this.utoh = utoh;
    }

    public String getHtou() {
        return htou;
    }

    public void setHtou(String htou) {
        this.htou = htou;
    }
}

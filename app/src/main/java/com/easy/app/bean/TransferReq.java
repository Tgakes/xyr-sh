package com.easy.app.bean;

public class TransferReq {

    String money;

    String des;

    String receiptUid;

    int currency;

    int way;
    /**
     * 支付密码
     */
   String authentication;


   String orderId;
    /**
     * 验证码
     */
   String vrCode;

    public TransferReq() {
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setReceiptUid(String receiptUid) {
        this.receiptUid = receiptUid;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public void setWay(int way) {
        this.way = way;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getVrCode() {
        return vrCode;
    }

    public void setVrCode(String vrCode) {
        this.vrCode = vrCode;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMoney() {
        return money;
    }

    public String getDes() {
        return des;
    }

    public String getReceiptUid() {
        return receiptUid;
    }

    public int getCurrency() {
        return currency;
    }

    public int getWay() {
        return way;
    }

    public String getAuthentication() {
        return authentication;
    }


    public TransferReq(String money, String des, String receiptUid, int currency, int way) {
        this.money = money;
        this.des = des;
        this.receiptUid = receiptUid;
        this.currency = currency;
        this.way = way;
    }
}

package com.easy.app.bean;

public class CountryCode {

    String enKey;

    String cnKey;

    String shKey;

    Integer codeKey;

    public String getEnKey() {
        return enKey;
    }

    public void setEnKey(String enKey) {
        this.enKey = enKey;
    }

    public String getCnKey() {
        return cnKey;
    }

    public void setCnKey(String cnKey) {
        this.cnKey = cnKey;
    }

    public String getShKey() {
        return shKey;
    }

    public void setShKey(String shKey) {
        this.shKey = shKey;
    }

    public Integer getCodeKey() {
        return codeKey;
    }

    public void setCodeKey(Integer codeKey) {
        this.codeKey = codeKey;
    }
}

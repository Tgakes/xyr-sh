package com.easy.app.ui.home;

import android.Manifest;
import android.app.Activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Token;
import com.easy.app.helper.UploadHelp;
import com.easy.app.ui.home.fragment.discovery.DiscoveryFragment;
import com.easy.app.ui.home.fragment.friend.FriendFragment;
import com.easy.app.ui.home.fragment.market.MarketFragment;
import com.easy.app.ui.home.fragment.message.MessageFragment;
import com.easy.app.ui.home.fragment.self.SelfFragment;
import com.easy.app.ui.home.fragment.self.SelfOldFragment;
import com.easy.framework.bean.AppVersion;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.callback.UploadCallback;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.store.bean.Accounts;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import cn.wildfirechat.remote.ChatManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter extends AppPresenter<HomeView> {
    @Inject
    public HomePresenter() {
    }

    private int mLastFragmentId;// 当前界面

    private FriendFragment friendFragment;

    /**
     * 切换Fragment
     */
    public void changeFragment(int checkedId, FragmentActivity activity) {
        if (mLastFragmentId == checkedId) {
            return;
        }

        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(String.valueOf(checkedId));
        if (fragment == null) {
            switch (checkedId) {
                case R.id.rb_tab_1:
                    fragment = MessageFragment.newInstance();
                    break;
                case R.id.rb_tab_2:
//                    fragment = new FriendFragment();
                    friendFragment = FriendFragment.newInstance();
                    fragment = friendFragment;
                    break;
                case R.id.rb_html:
                    fragment = MarketFragment.newInstance();
                    break;
                case R.id.rb_tab_3:
                    fragment = DiscoveryFragment.newInstance();
//                    if (coreManager.getConfig().newUi) { // 切换新旧两种ui对应不同的发现页面，
//                        fragment = new SquareFragment();
//                    } else {
//                        //  fragment = new DiscoverFragment();
//                        fragment=new DiscoveryFragment();
//                    }
                    break;
                case R.id.rb_tab_4:
//                    fragment = new MeFragment();
                    fragment = SelfFragment.newInstance();
                    break;
            }
        }

        // fragment = null;
        assert fragment != null;

        if (!fragment.isAdded()) {// 未添加 add
            transaction.add(R.id.main_content, fragment, String.valueOf(checkedId));
        }

        Fragment lastFragment = activity.getSupportFragmentManager().findFragmentByTag(String.valueOf(mLastFragmentId));

        if (lastFragment != null) {
            transaction.hide(lastFragment);
        }
        // 以防万一出现last和current都是同一个fragment的情况，先hide再show,
        transaction.show(fragment);

        // transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);// 添加动画
        transaction.commitNowAllowingStateLoss();

        // getSupportFragmentManager().executePendingTransactions();
        if (friendFragment != null) {
            friendFragment.showQuickIndexBar(checkedId == R.id.rb_tab_2);
        }

        mLastFragmentId = checkedId;

//        if (checkedId == R.id.rb_tab_4) {
//            setStatusBarLight(false);
//        } else {
//            setStatusBarColor();
//        }
    }


    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestReadWritePermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionReadWriteCallback(granted, null),
                        throwable -> mvpView.permissionReadWriteCallback(null, throwable));
    }

    public void requestAppVersion() {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("business", "update");
        parameter.put("plat", 1);
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(AppVersion.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.appVersionCallback(result),
                        throwable -> {
                            Response<AppVersion> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.appVersionCallback(response);
                        });
    }


    /**
     *
     */
    public void requestGetToken() {
        Accounts accounts = accountsDao.get().getAccounts();
        if (accounts == null) {
            return;
        }
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("business", "ntoken");
        parameter.put("token", accounts.getApptoken());
        parameter.put("uid", accounts.getUid());
        parameter.put("getnew", 0);//0保持token 1更新token
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Token.class)
                /* .map(tokenResponse -> {
                     Token token;
                     if (tokenResponse.isSuccess() && (token = tokenResponse.getResultObj()) != null) {
                         accounts.setToken_2(token.getToken());
                         accountsDao.get().update(accounts);
                     }
                     return tokenResponse;
                 })*/
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.tokenCallback(result);
                        },
                        throwable -> {
                            Response<Token> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.tokenCallback(response);
                        });
    }

    public void cutImage(Activity activity, String cutPath) {


        UploadHelp.uploadDeal(cutPath, (status, file) -> {
            if (status == 1) {
                requestUploadFile(activity, file);
            }
        });
    }

    /**
     * 验证视频
     */
    public void requestUploadFile(Activity activity, File finalFile) {


        Map<String, File> fileMap = new HashMap<>();
        fileMap.put("imgfile", finalFile);
        RxHttp.post(UrlConstant.UPLOAD)
                .addHeader(getHeaderMap())
                .file(fileMap)
                .upload(new UploadCallback() {
                    @Override
                    public void handleSuccess(Response t) {
                        activity.runOnUiThread(() -> mvpView.uploadCallback(t));

                    }


                    @Override
                    public void handleError(ApiException exception) {
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });

                    }

                    @Override
                    public void onProgress(File file, long currentSize, long totalSize, float progress, int currentIndex, int totalFile) {
                        Logger.i("onProgress currentSize===" + currentSize + ",totalSize===" + totalSize + ",====progress=====" + progress);
                    }

                    @Override
                    public Response onConvert(String data) {
                        Logger.i("onConvert data===" + data);
                        return null;
                    }

                    @Override
                    public void onSuccess(Object value) {
                        Logger.i("onSuccess value===" + value);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setCode(Response.SUCCESS_CODE);
                            response.setStatus(Response.SUCCESS_STATUS);
                            response.setMsg((String) value);
                            response.setData((String) value);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onError(int code, String desc) {
                        Logger.i("onError code===" + code + ",desc===" + desc);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(desc);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onCancel() {
                        Logger.i("onCancel value===");
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });
                    }


                });
    }


    public void requestSetInfo(String avatar, String nickName) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "setself");
        parameter.put("img", avatar);
        parameter.put("nike", nickName);
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .map(response -> {

                    /*if (response.isSuccess()) {
                        List<Market> marketList = JSON.parseArray(response.getResult(), Market.class);
                        response.setResultObj(marketList);
                    }*/
                    return response;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            if (result.isSuccess()) {
                                ChatManager.Instance().updateUserInfo();
                                ToastUtils.showShort(App.getInst().getString(EmptyUtils.isNotEmpty(avatar) ? R.string.update_avatar : R.string.update_nick_name) + App.getInst().getString(R.string.success));

                            } else {
                                ToastUtils.showShort(App.getInst().getString(EmptyUtils.isNotEmpty(avatar) ? R.string.update_avatar : R.string.update_nick_name) + App.getInst().getString(R.string.fail));

                            }
                            if (EmptyUtils.isNotEmpty(nickName)) {
                                LiveEventBus.get("updateNickName").post(result.isSuccess() ? "success" : "fail");
                            }
                        },
                        throwable -> {
                            /*Response<List> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }*/
                            if (EmptyUtils.isNotEmpty(nickName)) {
                                LiveEventBus.get("updateNickName").post("fail");
                            }
                            ToastUtils.showShort(App.getInst().getString(EmptyUtils.isNotEmpty(avatar) ? R.string.update_avatar : R.string.update_nick_name) + App.getInst().getString(R.string.fail));
                        });
    }

}
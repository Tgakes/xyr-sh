package com.easy.app.ui.dialog.payPwd;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.TransferReq;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.utils.EmptyUtils;
import com.easy.utils.EncryptUtils;

import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PayPwdDialogPresenter extends AppPresenter<PayPwdDialogView> {


    @Inject
    public PayPwdDialogPresenter() {

    }

    public void requestTransfer(TransferReq transferReq) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "transfer");
        parameter.put("nid", transferReq.getReceiptUid());//收款的uid
        parameter.put("uid", accountsDao.get().getUserId());//收款的uid
        parameter.put("money", transferReq.getMoney());
        parameter.put("currency", transferReq.getCurrency());
        parameter.put("instructions", EmptyUtils.isEmpty(transferReq.getDes()) ? "" : transferReq.getDes());
        parameter.put("way", transferReq.getWay());
        parameter.put("authentication", transferReq.getAuthentication());
        parameter.put("orderid", transferReq.getOrderId());
        parameter.put("vrcode", transferReq.getVrCode());
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.transferCallback(result),
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.transferCallback(response);
                        });
    }




}

package com.easy.app.ui.myWallet;

import android.view.View;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Balance;
import com.easy.app.databinding.MyWalletBinding;
import com.easy.app.databinding.MyWalletOldBinding;
import com.easy.app.ui.dialog.exchange.ExchangeDialogFragment;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.net.beans.Response;
import com.easy.utils.Md5Utils;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.math.BigDecimal;

import cn.wildfirechat.model.UserInfo;
@Deprecated
@ActivityInject
@Route(path = RouterManager.MY_WALLET_OLD_ACTIVITY, name = "我的钱包")
public class MyWalletOldActivity extends BaseActivity<MyWalletPresenter, MyWalletOldBinding> implements MyWalletView, OnRefreshListener, View.OnClickListener {

    UserInfo userInfo;


    @Override
    public int getLayoutId() {
        return R.layout.my_wallet_old;
    }

    @Override
    public void initView() {
        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.my_purse));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.mipmap.navigation, App.getInst()));
            skinTitleView.setRightClickListener(v -> {
                RouterManager.goPayCenterActivity();
            });
        }
        userInfo = getIntent().getParcelableExtra("userInfo");
        if (userInfo != null) {
            viewBind.acTvNickName.setText(userInfo.displayName);
            viewBind.acTvWalletAddress.setText(Md5Utils.md5(userInfo.displayName).toUpperCase());
        } else {

        }
        viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, getString(R.string.default_amount)));
        viewBind.tvShExchange.setOnClickListener(this);
        viewBind.tvShSellMoney.setOnClickListener(this);
        viewBind.tvUsdtRecharge.setOnClickListener(this);
        viewBind.tvUsdtWithdraw.setOnClickListener(this);
        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(false);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
        showLoading();
        presenter.requestBalance();
    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.tvShExchange://兑换
//                    RouterManager.goExchange();
                    ExchangeDialogFragment.newInstance(getSupportFragmentManager());
                    break;
                case R.id.tvShSellMoney:
                    LiveEventBus.get("market", String.class).post("");
                    finish();
                    break;
                case R.id.tvUsdtRecharge://充币
                    RouterManager.goChargeMoney();
                    break;
                case R.id.tvUsdtWithdraw://提币
                    RouterManager.goWithdrawMoney();
                    break;
            }
        }
    }

    @Override
    public void balanceCallback(Response<Balance> response) {
        hideLoading();
        if (response.isSuccess()) {
            viewBind.swRefresh.finishRefresh(true);
            Balance balance = response.getResultObj();
            viewBind.tvShBalance.setText(Money.getMoneyFormat(balance.getSh()));
            viewBind.tvUsdtBalance.setText(Money.getMoneyFormat(balance.getUsdt()));
            viewBind.tvShBalanceAbout.setText("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getSh())).multiply(new BigDecimal(Double.parseDouble(balance.getHtou()))).toString())+" "+ WalletView.USDT_NAME);
            viewBind.tvUsdtBalanceAbout.setText("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getUsdt())).multiply(new BigDecimal(Double.parseDouble(balance.getUtoh()))).toString())+" "+ WalletView.SH_NAME);
            BigDecimal allAdd = new BigDecimal(Double.parseDouble(balance.getRate())).multiply(new BigDecimal(Double.parseDouble(balance.getUsdt()))).add(new BigDecimal(Double.parseDouble(balance.getSh())));
            viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, Money.getMoneyFormat(allAdd.toString())));
        } else {
            viewBind.swRefresh.finishRefresh();
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        presenter.requestBalance();
    }
}

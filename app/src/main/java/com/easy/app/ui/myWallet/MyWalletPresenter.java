package com.easy.app.ui.myWallet;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Balance;
import com.easy.app.bean.MyWallet;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.widget.WalletView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MyWalletPresenter extends AppPresenter<MyWalletView> {

    @Inject
    public MyWalletPresenter() {

    }


    public void requestBalance() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "balance");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Balance.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.balanceCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.balanceCallback(response);
                        });
    }

    public List<MyWallet> getWalletData() {

        List<MyWallet> myWalletList = new ArrayList<>();
        MyWallet myWallet = new MyWallet();
        myWallet.setAbout("≈0.00 " + WalletView.USDT_NAME);
        myWallet.setAmount("0.00");
        myWallet.setLogoRes(R.mipmap.ic_wallet_sh);
        myWallet.setCoinName(WalletView.SH_NAME);
        myWalletList.add(myWallet);

        myWallet = new MyWallet();
        myWallet.setAbout("≈0.00 " + WalletView.SH_NAME);
        myWallet.setAmount("0.00");
        myWallet.setLogoRes(R.mipmap.ic_wallet_usdt);
        myWallet.setCoinName(WalletView.USDT_NAME);
        myWalletList.add(myWallet);

        return myWalletList;
    }
}

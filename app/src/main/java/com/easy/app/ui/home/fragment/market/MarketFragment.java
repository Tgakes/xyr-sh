package com.easy.app.ui.home.fragment.market;


import android.view.View;

import androidx.fragment.app.Fragment;

import com.easy.app.R;
import com.easy.app.databinding.FmMarketBinding;
import com.easy.app.ui.home.fragment.market.hall.HallFragment;
import com.easy.app.ui.home.fragment.market.order.OrderFragment;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link MarketFragment} subclass.
 * Use the {@link MarketFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class MarketFragment extends BaseFragment<MarketPresenter, FmMarketBinding> implements MarketView {





    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarketFragment newInstance() {
        MarketFragment fragment = new MarketFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_market;
    }

    @Override
    public void initView(View view) {


        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        viewBind.clTopLayout.setPadding(0,statusHeight,0,0);
//        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewBind.clTopLayout.getLayoutParams();
//        params.topMargin = statusHeight;
//        viewBind.clTopLayout.setLayoutParams(params);




        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(HallFragment.newInstance());
        fragmentList.add(OrderFragment.newInstance());

        TitleViewPagerAdapter adapter = new TitleViewPagerAdapter(getChildFragmentManager(), fragmentList, new String[]{getString(R.string.market_tab_1), getString(R.string.market_tab_2)});
        viewBind.mViewPager.setAdapter(adapter);
        viewBind.tabLayout.setupWithViewPager(viewBind.mViewPager);

        int count = fragmentList.size();
        viewBind.mViewPager.setOffscreenPageLimit(count);

    }









}

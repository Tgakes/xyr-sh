package com.easy.app.ui.home.fragment.market;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.RotationModule;
import com.easy.loadimage.EasyLoadImage;
import com.jude.rollviewpager.adapter.DynamicPagerAdapter;

import java.util.List;


/**
 * Description:主页轮播图适配器
 * author:chengaobin
 * Created on 2018-10-18
 */
public class RotationAdapter extends DynamicPagerAdapter {

    List<RotationModule> squareCarouselList;
    Activity mActivity;


    public RotationAdapter(Activity mActivity) {
        this.mActivity = mActivity;
    }


    public void setData(List<RotationModule> list) {
        clearData();
        if (this.squareCarouselList != null) {
            this.squareCarouselList.addAll(list);
        } else {
            this.squareCarouselList = list;
        }
        notifyDataSetChanged();
    }


    public void clearData() {

        if (squareCarouselList != null) {
            squareCarouselList.clear();
        }

    }


    public RotationModule getItem(int position) {
        return squareCarouselList != null && position < squareCarouselList.size() ? squareCarouselList.get(position) : null;
    }

    @Override
    public View getView(ViewGroup container, int position) {
        ImageView view = new ImageView(mActivity);
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        RotationModule data = getItem(position);
        view.setImageResource(data.getRes());
        return view;
    }


    @Override
    public int getCount() {
        return squareCarouselList != null && !squareCarouselList.isEmpty() ? squareCarouselList.size() : 0;
    }
}

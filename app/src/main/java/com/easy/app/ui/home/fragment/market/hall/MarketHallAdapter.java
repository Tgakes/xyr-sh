package com.easy.app.ui.home.fragment.market.hall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.MarketHall;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.loadimage.EasyLoadImage;
import com.easy.utils.ViewUtils;

import java.util.List;

public class MarketHallAdapter extends BaseQuickAdapter<MarketHall, BaseViewHolder> {

    Fragment fragment;

    public MarketHallAdapter(Fragment fragment, int layoutResId, @Nullable List<MarketHall> data) {
        super(layoutResId, data);
        this.fragment = fragment;

    }


    @Override
    protected void convert(@NonNull BaseViewHolder helper, MarketHall item) {


        helper.getView(R.id.sell_coin_btn).setOnClickListener(v -> {
            if (ViewUtils.canClick()) {
//                RouterManager.goSellCoins(JSON.toJSONString(item));
                SellShDialogFragment.newInstance(JSON.toJSONString(item), fragment.getActivity().getSupportFragmentManager());
            }
        });
//        helper.getView(R.id.acIvBusiness).setOnClickListener(v -> {
//            if (ViewUtils.canClick() && EmptyUtils.isNotEmpty(item.getUsid())) {
//                MarketHelper.goConversation(fragment.getActivity(), item.getUsid());
//            }
//        });
        EasyLoadImage.loadImage2(App.getInst(), R.drawable.app_logo, item.getImage(), helper.getView(R.id.ivAvatar));
//        helper.setTextColor(R.id.acTvUserName, SkinUtils.getSkin(App.getInst()).getAccentColor());
        helper.setText(R.id.acTvUserName, item.getDisplay());
        helper.setText(R.id.acTvAcquisitionAmount, App.getInst().getString(R.string.all_amount) + item.getCash());
        helper.setText(R.id.acTvRate, item.getRate());
        helper.setText(R.id.acTvDealDone, App.getInst().getString(R.string.deal_done) + item.getClinch());
//        helper.setText(R.id.acTvAcquisitionAmountRate, new BigDecimal(Double.parseDouble(item.getClinch())).divide(new BigDecimal(Double.parseDouble(item.getCash())), 0, BigDecimal.ROUND_HALF_EVEN).toString() + "%");

//        helper.setImageResource(R.id.ivMineIcon, item.getIconRes());
////        helper.setText(R.id.tvDownloadSize, "21.82MB/63.69MB");
////        helper.setText(R.id.tvSpeed, "0.15MB/S");
////        helper.addOnClickListener(R.id.btnState);
    }


}

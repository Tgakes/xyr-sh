package com.easy.app.ui.home;

import com.easy.app.bean.Token;
import com.easy.framework.base.BaseView;
import com.easy.framework.bean.AppVersion;
import com.easy.net.beans.Response;

public interface HomeView extends BaseView {

    void permissionReadWriteCallback(Boolean granted, Throwable e);


    void appVersionCallback(Response<AppVersion> response);


    void tokenCallback(Response<Token> response);


    void uploadCallback(Response response);

}

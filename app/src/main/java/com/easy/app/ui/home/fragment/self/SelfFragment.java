package com.easy.app.ui.home.fragment.self;


import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.FmSelfBinding;
import com.easy.app.databinding.FmSelfOldBinding;
import com.easy.app.ui.home.HomeActivity;
import com.easy.app.ui.home.fragment.message.MessagePresenter;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.loadimage.EasyLoadImage;
import com.easy.qrcode.ui.qr_code.QrScanActivity;
import com.easy.store.bean.Accounts;
import com.easy.utils.DimensUtils;
import com.easy.utils.StreamUtils;
import com.easy.utils.StringUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

import java.util.List;

import cn.wildfire.chat.kit.WfcScheme;
import cn.wildfire.chat.kit.qrcode.QRCodeActivity;
import cn.wildfire.chat.kit.user.UserInfoActivity;
import cn.wildfire.chat.kit.user.UserViewModel;
import cn.wildfire.chat.moment.FeedListActivity;
import cn.wildfirechat.model.UserInfo;


/**
 * A simple {@link SelfFragment} subclass.
 * Use the {@link SelfFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class SelfFragment extends BaseFragment<SelfPresenter, FmSelfBinding> implements SelfView, View.OnClickListener {

//    @Inject
//    MessagePresenter presenter;


    public SelfFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelfFragment newInstance() {
        SelfFragment fragment = new SelfFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_self;
    }

    @Override
    public void initView(View view) {

        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewBind.acTvScan.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        viewBind.acTvScan.setLayoutParams(layoutParams);

        int width = DimensUtils.getScreenWidth(App.getInst()) / 2 - DimensUtils.dp2px(App.getInst(), 28);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewBind.clWalletLayout.getLayoutParams();
        params.width = width;
        params.height = width / 2;
        viewBind.clWalletLayout.setLayoutParams(params);

        params = (ConstraintLayout.LayoutParams) viewBind.clReceiptCode.getLayoutParams();
        params.width = width;
        params.height = width / 2;
        viewBind.clReceiptCode.setLayoutParams(params);

        initData();
        viewBind.acTvScan.setOnClickListener(this);
        viewBind.acTvSetting.setOnClickListener(this);
        viewBind.clUserInfo.setOnClickListener(this);
        viewBind.clWalletLayout.setOnClickListener(this);
        viewBind.clReceiptCode.setOnClickListener(this);
        viewBind.acTvMyMoments.setOnClickListener(this);
        viewBind.acTvMyCollection.setOnClickListener(this);
        viewBind.acTvBankCardManage.setOnClickListener(this);


    }

    private UserViewModel userViewModel;


    private Observer<List<UserInfo>> userInfoLiveDataObserver = new Observer<List<UserInfo>>() {
        @Override
        public void onChanged(@Nullable List<UserInfo> userInfos) {
            if (userInfos == null) {
                return;
            }
            for (UserInfo info : userInfos) {
                if (info.uid.equals(userViewModel.getUserId())) {

                    updateUserInfo(info);
                    break;
                }
            }
        }
    };

    private void updateUserInfo(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        viewBind.clWalletLayout.setTag(userInfo);
        EasyLoadImage.loadRoundCornerImage(App.getInst(), R.drawable.avatar_normal, userInfo.portrait, viewBind.avatarImg, DimensUtils.dp2px(App.getInst(), 5f), 0);
        viewBind.nickNameTv.setText(userInfo.displayName);
        Accounts accounts = presenter.accountsDao.get().getAccounts();
        if (accounts != null) {
//            viewBind.authStyleTv.setText(getString(R.string.auth_style, (accounts.getRank() == 1 ? StringUtils.encryptMobile(userInfo.mobile) : userInfo.email)));
            viewBind.authStyleTv.setText(getString(R.string.auth_style, getString(accounts.getRank() == 1 ? R.string.phone_number : R.string.email)));
            viewBind.acTvUserId.setText(getString(R.string.sh_id, accounts.getUid()));
        }

    }

    private void initData() {

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUserInfoAsync(userViewModel.getUserId(), true)
                .observe(getViewLifecycleOwner(), info -> updateUserInfo(info));
        MutableLiveData<List<UserInfo>> mutableLiveData;
        if ((mutableLiveData = userViewModel.userInfoLiveData()) != null) {
            mutableLiveData.observeForever(userInfoLiveDataObserver);
        }
    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.clReceiptCode://收款码
                    RouterManager.goPaymentOrReceiptActivity();
                    break;
                case R.id.clWalletLayout://我的钱包
                    RouterManager.goMyWalletActivity((UserInfo) viewBind.clWalletLayout.getTag());
                    break;
                case R.id.clUserInfo:
                    Intent intent = new Intent(getActivity(), UserInfoActivity.class);
                    intent.putExtra("userInfo", (UserInfo) viewBind.clWalletLayout.getTag());
                    startActivity(intent);
                    break;
                case R.id.acTvMyMoments:
                    // 我的动态
                    UserInfo userInfo2 = (UserInfo) viewBind.clWalletLayout.getTag();
                    if (userInfo2 != null) {
                        Intent intent2 = new Intent(getActivity(), FeedListActivity.class);
                        intent2.putExtra("userInfo", userInfo2);
                        startActivity(intent2);
                    }
                    break;
                case R.id.acTvBankCardManage:
                    //银行卡管理
                    RouterManager.goBankCardManageActivity(0);
                    break;
                case R.id.acTvMyCollection:
                    // 我的收藏
//                    UserInfo userInfo3 = (UserInfo) viewBind.infoRl.getTag();
//                    if (userInfo3 != null) {
//                        RouterManager.goTransferMoneyActivity(userInfo3, getString(R.string.withdraw));
//                    }
//                startActivity(new Intent(getActivity(), MyCollection.class));
                    break;
                case R.id.acTvScan://扫一扫
                    presenter.requestPermission(getRxPermissions());
                    break;
                case R.id.acTvSetting:
                    // 设置
                    RouterManager.goSettingActivity();
                    break;

            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MutableLiveData<List<UserInfo>> mutableLiveData;
        if (userViewModel != null && (mutableLiveData = userViewModel.userInfoLiveData()) != null) {
            mutableLiveData.removeObserver(userInfoLiveDataObserver);
        }

    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {
//            RouterManager.goQrScan();
            getActivity().startActivityForResult(new Intent(getActivity(), QrScanActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);

//            getActivity().startActivityForResult(new Intent(getActivity(), ScanQRCodeActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);
        }
    }
}

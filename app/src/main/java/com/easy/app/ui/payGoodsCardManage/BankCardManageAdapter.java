package com.easy.app.ui.payGoodsCardManage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.PayGoodsCardManage;
import com.easy.utils.EmptyUtils;

import java.util.List;

public class BankCardManageAdapter extends BaseQuickAdapter<PayGoodsCardManage, BaseViewHolder> {


    public BankCardManageAdapter(int layoutResId, @Nullable List<PayGoodsCardManage> data) {
        super(layoutResId, data);


    }

   /* @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);


        AppCompatTextView acTBankName = holder.getView(R.id.acTBankName);
        if (acTBankName == null) {
            return;
        }
        AppCompatTextView acTvBandCardNumber = holder.getView(R.id.acTvBandCardNumber);
        if (position % 2 == 0) {
            acTBankName.setText("中国建设银行");
            acTvBandCardNumber.setText(App.getInst().getString(R.string.bank_card,"8888"));
        } else {
            acTBankName.setText("中国银行");
            acTvBandCardNumber.setText(App.getInst().getString(R.string.bank_card,"6666"));

        }
    }*/

    @Override
    protected void convert(@NonNull BaseViewHolder helper, PayGoodsCardManage item) {

//      EasyLoadImage.loadImage2(context, item.getIcon(), helper.getView(R.id.icon));
        helper.setText(R.id.acTBankName, item.getBank());
        if (EmptyUtils.isNotEmpty(item.getCarnumber()) && item.getCarnumber().length() >= 4) {
            helper.setText(R.id.acTvBandCardNumber, App.getInst().getString(R.string.bank_card, item.getCarnumber().substring(item.getCarnumber().length() - 4)));
        } else {
            helper.setText(R.id.acTvBandCardNumber, "");
        }
    }


}

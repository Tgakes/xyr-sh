package com.easy.app.ui.transferMoney;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Pay;
import com.easy.app.bean.Receipt;
import com.easy.app.bean.TransferReq;
import com.easy.app.databinding.TransferMoneyBinding;
import com.easy.app.ui.dialog.KeyBoad;
import com.easy.app.ui.dialog.VerifyDialog;
import com.easy.app.ui.dialog.payPwd.PayPwdDialogFragment;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.ui.mvp.transfer.TransferPresenter;
import com.easy.app.ui.mvp.transfer.TransferView;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.EditTextUtil;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.dialog.GodDialog;
import com.easy.loadimage.EasyLoadImage;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;
import com.jeremyliao.liveeventbus.LiveEventBus;

import javax.inject.Inject;

@ActivityInject
@Route(path = RouterManager.TRANSFER_MONEY, name = "转账")
public class TransferMoneyActivity extends BaseActivity<TransferMoneyPresenter, TransferMoneyBinding> implements TransferMoneyView, PayPwdDialogFragment.PayPwdListener {

    private KeyBoad keyBoad;

    private boolean isUiCreate = false;

    private String money, words;// 转账金额与转账说明

    @Autowired(name = "transferInfo")
    String transferInfo;

    @Autowired(name = "action")
    String action;

    //1扫一扫 2 转账
    @Autowired(name = "type")
    int type;

    Receipt receipt;





    @Override
    public int getLayoutId() {
        return R.layout.transfer_money;
    }

    @Override
    public void initView() {


        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.transfer_money));
        }
        if (presenter != null) {
            presenter.attachView(context, this, this);
        }
        if (type != 1 && type != 2) {
            finish();
            return;
        }

        if (EmptyUtils.isEmpty(transferInfo) || (receipt = JSON.parseObject(transferInfo, Receipt.class)) == null) {
            finish();
            return;
        }
        EasyLoadImage.loadCircleImage(App.getInst(), receipt.getAvatar(), viewBind.tmIv,0);
        viewBind.tmTv.setText(receipt.getUserName());

        viewBind.transferJeTv.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);// 允许输入数字与小数点
        // 禁止输入框复制粘贴
        EditTextUtil.disableCopyAndPaste(viewBind.etTransfer);
        keyBoad = new KeyBoad(TransferMoneyActivity.this, getWindow().getDecorView(), viewBind.etTransfer);
        viewBind.tvAccountType.setOnClickListener(view -> {
            WalletView.showWalletView(accountType -> viewBind.tvAccountType.setText(accountType), this);
        });
        initEvent();
        initKeyBorad();

        if (EmptyUtils.isNotEmpty(receipt.getMoney())) {
            viewBind.tvAccountType.setEnabled(false);
            viewBind.etTransfer.setEnabled(false);
            if (receipt.getMoney().endsWith(WalletView.USDT_NAME)) {
                viewBind.tvAccountType.setText(WalletView.USDT_NAME);
            }
            viewBind.etTransfer.setText(receipt.getMoney().replace(viewBind.tvAccountType.getText().toString(), "").trim());
        }

    }


    private void initEvent() {

        viewBind.tvAccountType.setOnClickListener(view -> {
            WalletView.showWalletView(accountType -> viewBind.tvAccountType.setText(accountType), this);
        });

        viewBind.transferEditDescTv.setOnClickListener(v -> {
            VerifyDialog verifyDialog = new VerifyDialog(TransferMoneyActivity.this);
            verifyDialog.setVerifyClickListener(getString(R.string.transfer_money_desc), getString(R.string.transfer_desc_max_length_10),
                    words, 10, new VerifyDialog.VerifyClickListener() {
                        @Override
                        public void cancel() {

                        }

                        @Override
                        public void send(String str) {
                            words = str;
                            if (TextUtils.isEmpty(words)) {
                                viewBind.transferDescTv.setText("");
                                viewBind.transferDescTv.setVisibility(View.GONE);
                                viewBind.transferEditDescTv.setText(getString(R.string.transfer_money_desc));
                            } else {
                                viewBind.transferDescTv.setText(str);
                                viewBind.transferDescTv.setVisibility(View.VISIBLE);
                                viewBind.transferEditDescTv.setText(getString(R.string.transfer_modify));
                            }
                            keyBoad.show();
                        }

                    });
            verifyDialog.setOkButton(R.string.sure);
            keyBoad.dismiss();
            Window window = verifyDialog.getWindow();

            if (window != null) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE); // 软键盘弹起
            }
            verifyDialog.show();
        });

        ButtonColorChange.colorChange(this, findViewById(R.id.transfer_btn));
        viewBind.transferBtn.setOnClickListener(v -> {

            if (receipt.getUserId().equals(presenter.accountsDao.get().getUserId())) {

                GodDialog otherDeviceLoginDialog = new GodDialog.Builder(this)
                        .setMessage(getString(R.string.not_support_transfer_self))
                        .setOneButton(getString(R.string.ok), (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .build();
                otherDeviceLoginDialog.show();
                return;
            }


            money = viewBind.etTransfer.getText().toString().trim();
            if (TextUtils.isEmpty(money) || Double.parseDouble(money) <= 0) {
                ToastUtils.showShort(getString(R.string.transfer_input_money));
                return;
            }
            money = Money.getMoneyFormat(money);
            keyBoad.dismiss();
            String data = JSON.toJSONString(new TransferReq(money, words, receipt.getUserId(), viewBind.tvAccountType.getText().toString().equals(WalletView.USDT_NAME)?2:1, 2));
            PayPwdDialogFragment.newInstance(new Pay(action, data, 1), getSupportFragmentManager());
            viewBind.transferBtn.setTag(data);
           /* PaySecureHelper.inputPayPassword(this, getString(R.string.transfer_money_to_someone, mTransferredName), money, password -> {
                transfer(money, words, password, tvAccountType.getText().toString().trim());
//                PayAccountTypeDialog payAccountTypeDialog = new PayAccountTypeDialog(this);
//                payAccountTypeDialog.setOnPayAccountTypeSelectedListener(accountType -> transfer(money, words, password, accountType));
//                payAccountTypeDialog.show();
            });*/
        });
    }

    private void initKeyBorad() {
        viewBind.etTransfer.setFocusable(true);
        viewBind.etTransfer.setOnFocusChangeListener((v, hasFocus) -> {
            if (keyBoad != null && isUiCreate) {
                keyBoad.refreshKeyboardOutSideTouchable(!hasFocus);
            } else if (isUiCreate) {
                keyBoad.show();
            }
            if (hasFocus) {
                keyBoad.show();
                InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(viewBind.etTransfer.getWindowToken(), 0);
            }
        });

        viewBind.etTransfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.etTransfer.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.etTransfer.setText(text.substring(1));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        viewBind.etTransfer.setOnClickListener(v -> {
            if (keyBoad != null) {
                keyBoad.show();
            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        isUiCreate = true;
    }


    @Override
    public void onPaySuccess(String orderId) {
        TransferReq transferReq = JSON.parseObject((String) viewBind.transferBtn.getTag(), TransferReq.class);
        transferReq.setOrderId(orderId);
        LiveEventBus.get("transMoney", String.class).post(JSON.toJSONString(transferReq));
        finish();
    }

    @Override
    public void onPayFail() {

    }
}

package com.easy.app.ui.chargeMoney;


import com.easy.app.bean.ChargeCoinsAddress;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface ChargeCoinsView extends BaseView {

    void permissionReadWriteCallback(Boolean granted, Throwable e);

    void uploadCallback(Response response);

    void chargeCoinAddressCallback(Response<ChargeCoinsAddress> response);

    void chargeCompleteCallback(Response response);

}

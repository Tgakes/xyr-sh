package com.easy.app.ui.bill;

import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Bill;
import com.easy.app.ui.empty.EmptyView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BillPresenter extends AppPresenter<BillView> {

    @Inject
    public BillPresenter() {

    }


    public List<Bill> getTestList() {

        List<Bill> bills = new ArrayList<>();
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        bills.add(new Bill());
        return bills;
    }
}

package com.easy.app.ui.splash;

import android.app.Activity;

import com.easy.app.base.AppPresenter;
import com.easy.app.ui.dialog.PermissionExplainDialog;
import com.easy.app.util.PermissionUtil;

import java.util.List;

import javax.inject.Inject;

public class SplashPresenter extends AppPresenter<SplashView> {


    // 复用请求权限的说明对话框，
    private PermissionExplainDialog permissionExplainDialog;

    public final int REQUEST_CODE = 0x01;

    @Inject
    public SplashPresenter() {

    }


    public void checkDismiss() {

        if (permissionExplainDialog != null) {
            if (permissionExplainDialog.isShowing()) {
                permissionExplainDialog.dismiss();
            }
            permissionExplainDialog = null;
        }
    }


    public PermissionExplainDialog getPermissionExplainDialog(Activity activity) {
        if (permissionExplainDialog == null) {
            permissionExplainDialog = new PermissionExplainDialog(activity);
        }
        return permissionExplainDialog;
    }

    public boolean showPermissionsDialog(Activity activity, String... permissions) {
        List<String> deniedPermission = PermissionUtil.getDeniedPermissions(activity, permissions);
        if (deniedPermission != null) {
            PermissionExplainDialog tip = getPermissionExplainDialog(activity);
            tip.setPermissions(deniedPermission.toArray(new String[0]));
            tip.setOnConfirmListener(() -> PermissionUtil.requestPermissions(activity, REQUEST_CODE, permissions));
            tip.show();
            return false;
        }
        return true;
    }


}

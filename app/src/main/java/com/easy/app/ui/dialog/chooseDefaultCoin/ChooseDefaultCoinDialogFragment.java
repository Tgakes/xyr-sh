package com.easy.app.ui.dialog.chooseDefaultCoin;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;

import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppSharePreferences;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Pay;
import com.easy.app.bean.TransferReq;
import com.easy.app.databinding.DialogChooseDefaultCoinBinding;
import com.easy.app.databinding.PayPwdDialogBinding;
import com.easy.app.helper.RandomHelper;
import com.easy.app.ui.dialog.payPwd.PayPwdDialogPresenter;
import com.easy.app.ui.dialog.payPwd.PayPwdDialogView;
import com.easy.app.util.AnimationUtils;
import com.easy.apt.annotation.FragmentInject;
import com.easy.apt.lib.SharePreference;
import com.easy.framework.base.BottomDialogFragment;
import com.easy.framework.base.CenterDialogFragment;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.Md5Utils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinUtils;
import com.easy.widget.WalletView;
import com.jungly.gridpasswordview.GridPasswordView;

@FragmentInject
public class ChooseDefaultCoinDialogFragment extends BottomDialogFragment<ChooseDefaultCoinDialogPresenter, DialogChooseDefaultCoinBinding> implements ChooseDefaultCoinDialogView, View.OnClickListener {


    ChooseDefaultCoinListener listener;
    AppSharePreferences appSharePreferences;

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();


//        StatusBarUtil.hideNavigationBar(window);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.gravity = Gravity.BOTTOM;
        params.windowAnimations = 0;
        window.setAttributes(params);

        //设置背景半透明
//        DisplayMetrics dm = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
//        getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public static ChooseDefaultCoinDialogFragment newInstance(FragmentManager fragmentManager) {
        ChooseDefaultCoinDialogFragment fragment = new ChooseDefaultCoinDialogFragment();
        fragment.show(fragmentManager);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseDefaultCoinListener) {
            listener = (ChooseDefaultCoinListener) context;
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.dialog_choose_default_coin;
    }


    @Override
    public void initView(View view) {

        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        viewBind.acTvShTextTwo.setText(Md5Utils.md5(WalletView.SH_NAME).toUpperCase());
        viewBind.acTvUsdtTextTwo.setText(Md5Utils.md5(WalletView.USDT_NAME).toUpperCase());
        appSharePreferences = SharePreference.get(App.getInst(), AppSharePreferences.class);
        if (appSharePreferences.getCoinType() == 1) {
            viewBind.vChoose2.setVisibility(View.VISIBLE);
            viewBind.vChoose1.setVisibility(View.GONE);
        }
        viewBind.ivClose.setOnClickListener(this);
        viewBind.clSh.setOnClickListener(this);
        viewBind.clUsdt.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.clSh:
                    if (viewBind.vChoose1.getVisibility() == View.GONE) {
                        viewBind.vChoose1.setVisibility(View.VISIBLE);
                        viewBind.vChoose2.setVisibility(View.GONE);
                        appSharePreferences.setCoinType(ChooseDefaultCoinListener.SH_MODE);
                        if (listener != null) {
                            listener.onChooseDefaultCoin(ChooseDefaultCoinListener.SH_MODE);
                        }
                    }
                    dismissAllowingStateLoss();
                    break;
                case R.id.clUsdt:
                    if (viewBind.vChoose2.getVisibility() == View.GONE) {
                        viewBind.vChoose2.setVisibility(View.VISIBLE);
                        viewBind.vChoose1.setVisibility(View.GONE);
                        appSharePreferences.setCoinType(ChooseDefaultCoinListener.USDT_MODE);
                        if (listener != null) {
                            listener.onChooseDefaultCoin(ChooseDefaultCoinListener.USDT_MODE);
                        }
                    }
                    dismissAllowingStateLoss();
                    break;
                case R.id.ivClose:
                    dismissAllowingStateLoss();
                    break;
            }
        }

    }


    public interface ChooseDefaultCoinListener {

        int SH_MODE = 0;

        int USDT_MODE = 1;

        void onChooseDefaultCoin(int coin);
    }


}

package com.easy.app.ui.home.fragment.discovery;

import android.Manifest;

import androidx.lifecycle.Lifecycle;

import com.easy.app.base.AppPresenter;
import com.easy.app.ui.home.fragment.message.MessageView;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

public class DiscoveryPresenter extends AppPresenter<DiscoveryView> {

    @Inject
    public DiscoveryPresenter() {

    }

    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.READ_PHONE_STATE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }


}

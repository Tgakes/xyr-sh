package com.easy.app.ui.dialog.sellSh;

import com.easy.app.bean.Balance;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;


public interface SellShDialogView extends BaseView {


    void balanceCallback(Response<Balance> response);

    void placeOrderCallback(Response response);

}

package com.easy.app.ui.home.fragment.discovery;


import com.easy.framework.base.BaseView;

public interface DiscoveryView extends BaseView {

    void permissionCallback(Boolean granted, Throwable e);

}

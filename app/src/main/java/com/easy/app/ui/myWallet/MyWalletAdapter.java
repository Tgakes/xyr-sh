package com.easy.app.ui.myWallet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.MyWallet;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.loadimage.EasyLoadImage;
import com.easy.utils.ViewUtils;

import java.util.List;

public class MyWalletAdapter extends BaseQuickAdapter<MyWallet, BaseViewHolder> {



    public MyWalletAdapter(int layoutResId, @Nullable List<MyWallet> data) {
        super(layoutResId, data);

    }


    @Override
    protected void convert(@NonNull BaseViewHolder helper, MyWallet item) {


//        EasyLoadImage.loadImage2(App.getInst(), R.drawable.app_logo, item.getImage(), helper.getView(R.id.ivAvatar));
//        helper.setTextColor(R.id.acTvUserName, SkinUtils.getSkin(App.getInst()).getAccentColor());
        helper.setImageResource(R.id.acIvCoinLogo, item.getLogoRes());
        helper.setText(R.id.acTvCoinName, item.getCoinName());
        helper.setText(R.id.acTvCoinAmount, item.getAmount());
        helper.setText(R.id.acTvCoinAbout, item.getAbout());
//        helper.setText(R.id.acTvAcquisitionAmountRate, new BigDecimal(Double.parseDouble(item.getClinch())).divide(new BigDecimal(Double.parseDouble(item.getCash())), 0, BigDecimal.ROUND_HALF_EVEN).toString() + "%");

//        helper.setImageResource(R.id.ivMineIcon, item.getIconRes());
////        helper.setText(R.id.tvDownloadSize, "21.82MB/63.69MB");
////        helper.setText(R.id.tvSpeed, "0.15MB/S");
////        helper.addOnClickListener(R.id.btnState);
    }


}

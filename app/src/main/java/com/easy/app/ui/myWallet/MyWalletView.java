package com.easy.app.ui.myWallet;


import com.easy.app.bean.Balance;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface MyWalletView extends BaseView {

    void balanceCallback(Response<Balance> response);

}

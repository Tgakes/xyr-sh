package com.easy.app.ui.login;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.store.bean.Accounts;
import com.easy.utils.EmptyUtils;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import cn.wildfire.chat.kit.ChatManagerHolder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter extends AppPresenter<LoginView> {

    @Inject
    public LoginPresenter() {

    }


    /*public void requestSms(String telNum) {

        Map<String, Object> parameter = new HashMap<>();
        parameter.put("actsms", 3);//1注册 2转账 3登录 4兑换 5提现 6修改资料
        parameter.put("number", telNum);//接收方手机号
        RxHttp.post(UrlConstant.SMS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.smsCallback(result);
                        },
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.smsCallback(response);
                        });
    }*/


    public void requestLogin(String userName, String authCode, String pwd) {

        Map<String, Object> parameter = new HashMap<>();

        try {
            parameter.put("clientId", ChatManagerHolder.gChatManager.getClientId());
        } catch (Exception e) {
            e.printStackTrace();
            Response<Accounts> response = new Response<>();
            response.setMsg("android id 获取失败。。。");
            mvpView.loginCallback(response);
            return;
        }
        parameter.put("username", userName);
        if (!EmptyUtils.isEmpty(authCode)) {
            parameter.put("usms", authCode);
        }
        parameter.put("password", pwd);
        parameter.put("business", "login");

        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Accounts.class)
                .map(loginResponse -> {
                    Accounts accounts;
                    if (loginResponse.isSuccess() && (accounts = loginResponse.getResultObj()) != null) {
//                        accounts.setPhone(phoneNumber);
                        accountsDao.get().add(accounts);
                    }
                    return loginResponse;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.loginCallback(result);
                        },
                        throwable -> {
                            Response<Accounts> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                }else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.loginCallback(response);
                        });
    }

}

package com.easy.app.ui.payGoodsCardManage;

import androidx.lifecycle.Lifecycle;

import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.PayGoodsCardManage;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.utils.EmptyUtils;
import com.easy.widget.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BankCardManagePresenter extends AppPresenter<BankCardManageView> {

    @Inject
    public BankCardManagePresenter() {

    }


    public List<PayGoodsCardManage> getTestList() {

        List<PayGoodsCardManage> payGoodsCardManages = new ArrayList<>();

        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());
        payGoodsCardManages.add(new PayGoodsCardManage());

        return payGoodsCardManages;
    }


    public void requestList() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "carlist");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(List.class)
                .map(response -> {

                    if (response.isSuccess()) {
                        List<PayGoodsCardManage> payGoodsCardManageList = JSON.parseArray(response.getResult(), PayGoodsCardManage.class);
                        if (EmptyUtils.isEmpty(PreferenceUtils.getString(App.getInst(), "realName")) && payGoodsCardManageList != null && !payGoodsCardManageList.isEmpty()) {
                            PreferenceUtils.putString(App.getInst(), "realName", payGoodsCardManageList.get(0).getCarname());
                        }
                        response.setResultObj(payGoodsCardManageList);
                    }
                    return response;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.bankCardListCallback(result);
                        },
                        throwable -> {
                            Response<List> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.bankCardListCallback(response);
                        });
    }


}

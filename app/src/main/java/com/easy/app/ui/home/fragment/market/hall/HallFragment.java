package com.easy.app.ui.home.fragment.market.hall;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.MarketNotice;
import com.easy.app.bean.MarketHall;
import com.easy.app.databinding.ListCommomBinding;
import com.easy.app.databinding.MarketSellCoinsHeaderBinding;
import com.easy.app.helper.MarketHelper;
import com.easy.app.util.RxTimerTool;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.ui.RecycleViewDivider;
import com.easy.net.beans.Response;
import com.easy.utils.DimensUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.AutoVerticalScrollTextView;
import com.easy.widget.StateView;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.yanzhenjie.recyclerview.OnItemMenuClickListener;
import com.yanzhenjie.recyclerview.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.SwipeMenuItem;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link HallFragment} subclass.
 * Use the {@link HallFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class HallFragment extends BaseFragment<HallPresenter, ListCommomBinding> implements HallView, OnRefreshListener, View.OnClickListener, com.yanzhenjie.recyclerview.SwipeMenuCreator, OnItemMenuClickListener {


    MarketHallAdapter adapter;


    int page = 1;

    List<MarketHall> marketHallList;


    MarketSellCoinsHeaderBinding headerViewBind;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HallFragment newInstance() {
        HallFragment fragment = new HallFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.list_commom;
    }

    @Override
    public void initView(View view) {

        viewBind.recyclerView.setSwipeMenuCreator(this);
        viewBind.recyclerView.setOnItemMenuClickListener(this);

        viewBind.recyclerView.setBackgroundResource(R.color.white);
//        viewBind.recyclerView.setItemViewSwipeEnabled(true); // 侧滑删除，默认关闭。


        marketHallList = new ArrayList<>();
        adapter = new MarketHallAdapter(this, R.layout.adapter_market3, marketHallList);
        viewBind.recyclerView.setAdapter(adapter);
//        adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        viewBind.recyclerView.addItemDecoration(new RecycleViewDivider(App.getInst(), LinearLayoutManager.VERTICAL, DimensUtils.dp2px(App.getInst(), 15f), StreamUtils.getInstance().resourceToColor(R.color.white, App.getInst())));
        viewBind.recyclerView.setHasFixedSize(true);
        StateView emptyView = new StateView(getActivity());
        emptyView.setOnClickListener(this);
        adapter.setEmptyView(emptyView);
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.market_sell_coins_header, null, false);
        headerViewBind = DataBindingUtil.bind(headerView);
        headerViewBind.acTvCoinExchangeText.setText(getString(R.string.coin_exchange, "SH", "USDT"));
        startNotice(presenter.getMarketNotice(), headerViewBind.verticalScrollPrompt);
        viewBind.recyclerView.addHeaderView(headerView);
        viewBind.recyclerView.post(() -> {
            ViewGroup.LayoutParams params = headerViewBind.clTopLayout.getLayoutParams();
            params.width = DimensUtils.getScreenWidth(App.getInst());
            headerViewBind.clTopLayout.setLayoutParams(params);
        });



        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(false);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
//        adapter.setOnItemClickListener(this);
        presenter.requestList();
    }


    int number;
    RxTimerTool rxTimerTool;


    public void startNotice(List<MarketNotice> marketNoticeList, AutoVerticalScrollTextView verticalScrollPrompt) {

        if (marketNoticeList != null && !marketNoticeList.isEmpty()) {
            if (rxTimerTool != null) {
                rxTimerTool.cancel();
                rxTimerTool = null;
            }
            number = 0;
            verticalScrollPrompt.setText(marketNoticeList.get(number).getTitle());
            int length = marketNoticeList.size();
            if (length > 1) {
                rxTimerTool = RxTimerTool.getInstance();
                rxTimerTool.interval(2500, number1 -> {
                    verticalScrollPrompt.next();
                    number++;
                    String name = marketNoticeList.get(number % length).getTitle();
                    verticalScrollPrompt.setText(name);
                });
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContainer:
                Logger.i("====onClick llContainer 请求====");
                showLoading();
                presenter.requestList();
                break;
        }
    }


    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        presenter.requestList();
    }


    @Override
    public void marketListCallback(Response<List> response) {
        hideLoading();
        if (response.isSuccess()) {
            viewBind.swRefresh.finishRefresh(true);
            List<MarketHall> marketHall = response.getResultObj();
            this.marketHallList.clear();
            if (marketHall != null && !marketHall.isEmpty()) {
                this.marketHallList.addAll(marketHall);
            }
            adapter.notifyDataSetChanged();
        } else {
            viewBind.swRefresh.finishRefresh();
            ToastUtils.showShort(response.getMsg());
        }

    }


    @Override
    public void onCreateMenu(com.yanzhenjie.recyclerview.SwipeMenu leftMenu, com.yanzhenjie.recyclerview.SwipeMenu rightMenu, int position) {
        SwipeMenuItem swipeMenuItem = new SwipeMenuItem(getActivity());
        swipeMenuItem.setBackground(R.drawable.market_hall_sideslip_bg);
        swipeMenuItem.setText(getString(R.string.communicate_with_him));
        swipeMenuItem.setTextColorResource(R.color.white);
        swipeMenuItem.setTextSize(15);
        swipeMenuItem.setHeight(DimensUtils.dp2px(App.getInst(),80f));
        swipeMenuItem.setWidth(DimensUtils.dp2px(App.getInst(),90f));
        rightMenu.addMenuItem(swipeMenuItem); // 在Item左侧添加一个菜单。


        // 注意：哪边不想要菜单，那么不要添加即可。
    }

    @Override
    public void onItemClick(SwipeMenuBridge menuBridge, int adapterPosition) {
        menuBridge.closeMenu();
        // 左侧还是右侧菜单：
        int direction = menuBridge.getDirection();
        // 菜单在Item中的Position：
        int menuPosition = menuBridge.getPosition();
//        adapter.getItem()
        Log.i("logs", "menuPosition===" + menuPosition + ",direction===" + direction + ",adapterPosition===" + adapterPosition);
        MarketHall marketHall = adapter.getItem(adapterPosition);
        if (marketHall != null) {
            if (ViewUtils.canClick() && EmptyUtils.isNotEmpty(marketHall.getUsid())) {
                MarketHelper.goConversation(getActivity(), marketHall.getUsid());
            }
        }
    }
}

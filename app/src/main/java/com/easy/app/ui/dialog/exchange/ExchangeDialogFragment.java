package com.easy.app.ui.dialog.exchange;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.Balance;
import com.easy.app.databinding.DialogExchangeBinding;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.Money;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BottomDialogFragment;
import com.easy.framework.base.common.CommonActivity;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;

import java.math.BigDecimal;

@FragmentInject
public class ExchangeDialogFragment extends BottomDialogFragment<ExchangeDialogPresenter, DialogExchangeBinding> implements ExchangeDialogView, View.OnClickListener, TextWatcher {


    /**
     * 当前币种 1：sh 2: usdt
     */
    int currentCurrency;

    public static ExchangeDialogFragment newInstance(FragmentManager fragmentManager) {
        ExchangeDialogFragment fragment = new ExchangeDialogFragment();
        fragment.show(fragmentManager);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }


    @Override
    public int getLayoutId() {
        return R.layout.dialog_exchange;
    }


    @Override
    public void initView(View view) {

        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        currentCurrency = 2;



        viewBind.acTvAvailable.setText(getString(R.string.available_usdt, getString(R.string.default_amount)));
        viewBind.acTvExchangeRate.setText(getString(R.string.exchange_rate, getString(R.string.exchange_usdt_to_sh_, "0.000", "0.000")));
        ButtonColorChange.colorChange(App.getInst(), viewBind.exchangeBtn);
        viewBind.acTvExchangeAll.setOnClickListener(this);
        viewBind.exchangeBtn.setOnClickListener(this);
        viewBind.acTvExchange.setOnClickListener(this);
        viewBind.ivClose.setOnClickListener(this);
        viewBind.acEvPay.addTextChangedListener(this);
        presenter.requestBalance();


    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.ivClose:
                    dismissAllowingStateLoss();
                    break;
                case R.id.acTvExchangeAll:
                    String amount = (String) viewBind.acTvExchangeAll.getTag(currentCurrency == 1 ? R.id.tag_second : R.id.tag_first);
                    if (EmptyUtils.isNotEmpty(amount) && Double.parseDouble(amount) > 0) {
                        viewBind.acEvPay.setText(amount);
                        viewBind.acEvPay.setSelection(viewBind.acEvPay.getText().toString().length());
                    }
                    break;
                case R.id.exchange_btn:
                    String amountAll = viewBind.acEvPay.getText().toString().trim();
                    if (amountAll.endsWith(SellShDialogFragment.POINT)) {
                        amountAll = amountAll.substring(0, amountAll.length() - 1);
                    }
                    double mMoney = 0;
                    try {
                        mMoney = Double.parseDouble(amountAll);
                    } catch (NumberFormatException e) {
                        ToastUtils.showShort(getString(R.string.input_exchange_num));
                    }

                    if (TextUtils.isEmpty(amountAll) || mMoney <= 0) {
                        ToastUtils.showShort(getString(R.string.input_exchange_num));
                        return;
                    }
                    ((CommonActivity) getActivity()).showLoading();
                    presenter.requestExchange(currentCurrency, amountAll);
                    break;
                case R.id.acTvExchange:
                    viewBind.acEvPay.setText("");
                    if (currentCurrency == 1) {
                        currentCurrency = 2;
                        viewBind.acTvPayCurrency.setText("USDT");
                        viewBind.acTvPayCurrency.setCompoundDrawablesWithIntrinsicBounds(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_usdt, App.getInst()), null, null, null);
                        viewBind.acTvGetCurrency.setText("SH");
                        viewBind.acTvGetCurrency.setCompoundDrawablesWithIntrinsicBounds(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_sh, App.getInst()), null, null, null);

                        String available1 = (String) viewBind.acTvAvailable.getTag(R.id.tag_first);
                        if (EmptyUtils.isNotEmpty(available1)) {
                            viewBind.acTvAvailable.setText(available1);
                        }
                        String rate1 = (String) viewBind.acTvExchangeRate.getTag(R.id.tag_first);
                        if (EmptyUtils.isNotEmpty(rate1)) {
                            viewBind.acTvExchangeRate.setText(rate1);
                        }

                    } else {
                        currentCurrency = 1;
                        viewBind.acTvGetCurrency.setText("USDT");
                        viewBind.acTvGetCurrency.setCompoundDrawablesWithIntrinsicBounds(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_usdt, App.getInst()), null, null, null);
                        viewBind.acTvPayCurrency.setText("SH");
                        viewBind.acTvPayCurrency.setCompoundDrawablesWithIntrinsicBounds(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_sh, App.getInst()), null, null, null);

                        String available2 = (String) viewBind.acTvAvailable.getTag(R.id.tag_second);
                        if (EmptyUtils.isNotEmpty(available2)) {
                            viewBind.acTvAvailable.setText(available2);
                        }
                        String rate2 = (String) viewBind.acTvExchangeRate.getTag(R.id.tag_second);
                        if (EmptyUtils.isNotEmpty(rate2)) {
                            viewBind.acTvExchangeRate.setText(rate2);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void balanceCallback(Response<Balance> response) {

        if (response.isSuccess()) {
            Balance balance = response.getResultObj();
            if (balance != null) {
                viewBind.acTvExchangeAll.setTag(R.id.tag_second, balance.getUsdt());
                viewBind.acTvExchangeAll.setTag(R.id.tag_first, balance.getSh());

                viewBind.acTvAvailable.setText(getString(currentCurrency == 1 ? R.string.available_sh : R.string.available_usdt, Money.getMoneyFormat(currentCurrency == 1 ? balance.getUsdt() : balance.getSh())));
                viewBind.acTvAvailable.setTag(R.id.tag_first, getString(R.string.available_sh, Money.getMoneyFormat(balance.getSh())));
                viewBind.acTvAvailable.setTag(R.id.tag_second, getString(R.string.available_usdt, Money.getMoneyFormat(balance.getUsdt())));

//                BigDecimal a1 = new BigDecimal(Double.parseDouble("1"));
//                BigDecimal b1 = new BigDecimal(Double.parseDouble(balance.getRate()));
//                String num = a1.divide(b1, 4, BigDecimal.ROUND_HALF_EVEN).toString();
                viewBind.acTvExchangeRate.setText(getString(R.string.exchange_rate, getString(currentCurrency == 1 ? R.string.exchange_sh_to_usdt : R.string.exchange_usdt_to_sh, currentCurrency == 2 ? balance.getUtoh() : balance.getHtou())));
                viewBind.acTvExchangeRate.setTag(R.id.tag_first, getString(R.string.exchange_rate, getString(R.string.exchange_usdt_to_sh, balance.getUtoh())));
                viewBind.acTvExchangeRate.setTag(R.id.tag_second, getString(R.string.exchange_rate, getString(R.string.exchange_sh_to_usdt, balance.getHtou())));

                viewBind.acEvGet.setTag(R.id.tag_first, Double.parseDouble(balance.getHtou()));
                viewBind.acEvGet.setTag(R.id.tag_second, Double.parseDouble(balance.getUtoh()));
            }

        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void exchangeCallback(Response response) {
        ((CommonActivity) getActivity()).hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if (!ViewUtils.inputSinglePoint(viewBind.acEvGet, s)) {
            String text = s.toString();
            if (EmptyUtils.isNotEmpty(text)) {
               /* int length = text.length();
                if (length - 1 - text.indexOf(POINT) > 2) {
                    text = text.substring(0, s.toString().indexOf(POINT) + 3);
                    viewBind.acEvSellAmount.setText(text);
                    viewBind.acEvSellAmount.setSelection(text.length());
                } else {

                }*/

                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.acEvPay.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.acEvPay.setText(text.substring(1));
                } else {
                    if (text.endsWith(SellShDialogFragment.POINT)) {
                        text = text.replace(SellShDialogFragment.POINT, "");
                    }
                    Double rate = (Double) viewBind.acEvGet.getTag(currentCurrency == 2 ? R.id.tag_second : R.id.tag_first);
                    if (rate != null) {
                        BigDecimal a1 = new BigDecimal(Double.parseDouble(text));
                        BigDecimal b1 = new BigDecimal(rate);
                        BigDecimal result = a1.multiply(b1);// 相乘结果
                        viewBind.acEvGet.setText(Money.getMoneyFormat(result.toString()));
                    }
                }

            } else {
                viewBind.acEvGet.setText("");
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


}

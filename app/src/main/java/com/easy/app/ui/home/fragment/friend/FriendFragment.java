package com.easy.app.ui.home.fragment.friend;


import android.content.Intent;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentTransaction;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.databinding.FmDiscoveryBinding;
import com.easy.app.databinding.FmFriendBinding;
import com.easy.app.ui.dialog.MessagePopupWindow;
import com.easy.app.ui.home.fragment.discovery.DiscoveryView;
import com.easy.app.ui.home.fragment.message.MessagePresenter;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.utils.StreamUtils;
import com.easy.widget.SkinTitleView;

import javax.inject.Inject;

import cn.wildfire.chat.kit.contact.ContactListFragment;
import cn.wildfire.chat.kit.contact.newfriend.SearchUserActivity;
import cn.wildfire.chat.kit.conversation.CreateConversationActivity;
import cn.wildfire.chat.kit.conversationlist.ConversationListFragment;


/**
 * A simple {@link FriendFragment} subclass.
 * Use the {@link FriendFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class FriendFragment extends BaseFragment<MessagePresenter, FmFriendBinding> implements FriendView, View.OnClickListener {

    @Inject
    MessagePresenter presenter;

    private MessagePopupWindow mMessagePopupWindow;

    ContactListFragment fragment;

    public FriendFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FriendFragment newInstance() {
        FriendFragment fragment = new FriendFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_friend;
    }

    @Override
    public void initView(View view) {
        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewBind.clTitleBar.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        viewBind.clTitleBar.setLayoutParams(layoutParams);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackground(R.color.normal_bg);
            skinTitleView.setTitleText(getString(R.string.contacts));
            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.mipmap.ic_app_add, App.getInst()));
            skinTitleView.hideBackBtn();
            skinTitleView.setRightClickListener(v -> {
                mMessagePopupWindow = new MessagePopupWindow(getActivity(), FriendFragment.this, 2);
                mMessagePopupWindow.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                mMessagePopupWindow.showAsDropDown(v, -(mMessagePopupWindow.getContentView().getMeasuredWidth() - v.getWidth() / 2 - 40),
                        0);
            });
        }


         fragment = new ContactListFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.clFriendLayout, fragment).commit();
        showQuickIndexBar(true);
    }


    public void showQuickIndexBar(boolean show){
        if (fragment != null) {
            fragment.showQuickIndexBar(show);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.create_group:
            case R.id.face_group:
                // 发起群聊
                mMessagePopupWindow.dismiss();
                startActivity(new Intent(getActivity(), CreateConversationActivity.class));
                break;
            case R.id.add_friends:
                // 添加朋友
                mMessagePopupWindow.dismiss();
                startActivity(new Intent(getActivity(), SearchUserActivity.class));
//                startActivity(new Intent(getActivity(), UserSearchActivity.class));
                break;

        }
    }
}

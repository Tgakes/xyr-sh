package com.easy.app.ui.splash;

import android.Manifest;
import android.content.Intent;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatDelegate;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.EmptyBinding;
import com.easy.app.databinding.SplashBinding;
import com.easy.app.ui.dialog.TipDialog;
import com.easy.app.util.PermissionUtil;
import com.easy.app.util.RxTimerTool;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.utils.FileUtils;
import com.easy.utils.RootUtils;
import com.easy.utils.base.FileConstant;
import com.easy.widget.PreferenceUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.wildfire.chat.kit.Config;

@ActivityInject
@Route(path = RouterManager.SPLASH_ACTIVITY, name = "闪屏页面")
public class SplashActivity extends BaseActivity<SplashPresenter, EmptyBinding> implements SplashView, PermissionUtil.OnRequestPermissionsResultCallbacks {


    private ArrayMap<String, Integer> permissionsMap = new ArrayMap<>();

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void initView() {

        //首次启动 Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT 为 0，再次点击图标启动时就不为零了
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        // 如果不是任务栈第一个页面，就直接结束，显示上一个页面，
        // 主要是部分设备上Jitsi_pre页面退后台再回来会打开这个启动页flg=0x10200000，此时应该结束启动页，回到Jitsi_pre,
        if (!isTaskRoot()) {
            finish();
            return;
        }
        if (permissionsMap == null) {
            permissionsMap = new ArrayMap<>();
        }
//        AppCompatDelegate.setDefaultNightMode(PreferenceUtils.getBoolean(App.getInst(), PreferenceUtils.DARK_THEME, false) ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
//        recreate();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        // 手机状态
        permissionsMap.put(Manifest.permission.READ_PHONE_STATE, R.string.permission_phone_status);
        // 照相
        permissionsMap.put(Manifest.permission.CAMERA, R.string.permission_photo);
        // 麦克风
        permissionsMap.put(Manifest.permission.RECORD_AUDIO, R.string.permission_microphone);
        // 存储权限
        permissionsMap.put(Manifest.permission.READ_EXTERNAL_STORAGE, R.string.permission_storage);
        permissionsMap.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, R.string.permission_storage);
        if (presenter.showPermissionsDialog(this, permissionsMap.keySet().toArray(new String[]{}))) {
            new RxTimerTool().timer(1500, number -> ready());
        }
    }

    @Override
    public void onDestroy() {
        presenter.checkDismiss();
        if (permissionsMap != null) {
            permissionsMap.clear();
            permissionsMap = null;
        }
        super.onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == presenter.REQUEST_CODE) {// 设置 手动开启权限 返回 再次判断是否获取全部权限
            ready();
        }
    }

    private void ready() {
        setupCDirs();
        boolean exit = PreferenceUtils.getBoolean(App.getInst(),"exit",false);
        if (exit || !presenter.accountsDao.get().isLogin()){
            if (exit) {
                PreferenceUtils.putBoolean(App.getInst(),"exit",false);
                ((App) App.getInst()).logout(presenter.accountsDao.get(), this,true);
                return;
            }
            RouterManager.goLoginActivity(this);
        }else {
            RouterManager.goHomeActivity(this);
        }
    }

    /**
     *创建需要的文件夹
     */
    private void setupCDirs() {

        Config.VIDEO_SAVE_DIR = FileUtils.getFilePath(FileConstant.TYPE_VIDEO, App.getInst());
        Config.AUDIO_SAVE_DIR = FileUtils.getFilePath(FileConstant.TYPE_AUDIO, App.getInst());
        Config.PHOTO_SAVE_DIR = FileUtils.getFilePath(FileConstant.TYPE_PHOTO, App.getInst());
        Config.OTHER_SAVE_DIR = FileUtils.getFilePath(FileConstant.TYPE_OTHER, App.getInst());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtil.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms, boolean isAllGranted) {
        if (isAllGranted) {// 请求权限返回 已全部授权

            ready();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms, boolean isAllDenied) {
        int size;
        if (perms != null && (size = perms.size()) > 0) {
            // 用set去重，有的复数权限
            Set<String> tipSet = new HashSet<>();
            for (int i = 0; i < size; i++) {
                tipSet.add(getString(permissionsMap.get(perms.get(i))));
            }
            String tipS = TextUtils.join(", ", tipSet);
            boolean onceAgain = PermissionUtil.deniedRequestPermissionsAgain(this, perms.toArray(new String[size]));
            TipDialog mTipDialog = new TipDialog(this);
            if (onceAgain) {// 部分 || 所有权限被拒绝且选择了（选择了不再询问 || 部分机型默认为不在询问）
                mTipDialog.setmConfirmOnClickListener(getString(R.string.tip_reject_permission_place_holder, tipS), () -> PermissionUtil.startApplicationDetailsSettings(SplashActivity.this, presenter.REQUEST_CODE));
            } else {// 部分 || 所有权限被拒绝
                mTipDialog.setTip(getString(R.string.tip_permission_reboot_place_holder, tipS));
            }
            mTipDialog.show();
        }
    }
}

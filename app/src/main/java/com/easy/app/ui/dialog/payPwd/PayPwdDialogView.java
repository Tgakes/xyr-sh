package com.easy.app.ui.dialog.payPwd;

import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;


public interface PayPwdDialogView extends BaseView {

    void transferCallback(Response<String> response);

}

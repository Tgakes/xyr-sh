package com.easy.app.ui.register;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.RequiresApi;
import androidx.core.graphics.drawable.DrawableCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.CountryCode;
import com.easy.app.databinding.RegisterBinding;
import com.easy.app.helper.PasswordHelper;
import com.easy.app.helper.UploadHelp;
import com.easy.app.ui.chargeMoney.ChargeCoinsActivity;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.loadimage.EasyLoadImage;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.FileUtils;
import com.easy.utils.PhotoUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.utils.base.FileConstant;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.lqr.imagepicker.ImagePicker;
import com.lqr.imagepicker.bean.ImageItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

@ActivityInject
@Route(path = RouterManager.REGISTER_ACTIVITY, name = "注册页面")
public class RegisterActivity extends BaseActivity<RegisterPresenter, RegisterBinding> implements RegisterView, View.OnClickListener, RadioGroup.OnCheckedChangeListener, TextWatcher {

    public static final int REQUEST_CODE_PICK_IMAGE = 100;

    /**
     * 裁剪头像
     */
    public static final int START_CUT_AVATAR = 1001;


    /**
     * 注册方式 1:手机号 2:邮箱
     */
    int regType;

    @Override
    public int getLayoutId() {
        return R.layout.register;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initView() {
        regType = 1;
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.register));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }
        LiveEventBus.get("countryCode", CountryCode.class).observe(this, this::chooseCountryCode);
        PasswordHelper.bindPasswordEye(viewBind.passwordEdit, viewBind.tbEyePwd);
        PasswordHelper.bindPasswordEye(viewBind.confirmPasswordEdit, viewBind.tbEyeConfirm);

        ColorStateList tabColor = SkinUtils.getSkin(this).getMainTabColorState();
        for (RadioButton radioButton : Arrays.asList(viewBind.rbPhoneNumber, viewBind.rbEmail)) {
            radioButton.setTextColor(tabColor);
            radioButton.setButtonTintList(tabColor);
        }
        // 图标着色，兼容性解决方案，
        Drawable drawable = viewBind.tvPrefix.getCompoundDrawables()[2];
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTintList(drawable, ColorStateList.valueOf(SkinUtils.getSkin(this).getAccentColor()));
        // 如果是getDrawable拿到的Drawable不能直接调setCompoundDrawables，没有宽高，
        viewBind.tvPrefix.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

        viewBind.rbPhoneNumber.setChecked(true);
        viewBind.sendAgainBtn.setOnClickListener(this);
        viewBind.btnBindOldAccount.setOnClickListener(this);
        viewBind.nextStepBtn.setOnClickListener(this);
        viewBind.ivChoosePicture.setOnClickListener(this);
        viewBind.imageIvRefresh.setOnClickListener(this);
        viewBind.tvPrefix.setOnClickListener(this);
        viewBind.rgRegAuthStyle.setOnCheckedChangeListener(this);
        viewBind.etAccount.addTextChangedListener(this);
    }

    public void chooseCountryCode(CountryCode data) {

        viewBind.tvPrefix.setText("+" + data.getCodeKey());
    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.tv_prefix:
                    RouterManager.goCountryCode();
                    break;
                case R.id.ivChoosePicture://
                    presenter.requestReadWritePermission(getRxPermissions());
                    break;
                case R.id.send_again_btn:
//                    viewBind.sendAgainBtn.setEnabled(false);
//                    viewBind.sendAgainBtn.setText(getString(R.string.getting));
//                    presenter.requestPermission(getRxPermissions());
                    break;
                case R.id.btnBindOldAccount:
                    finish();
                    break;
                case R.id.next_step_btn:
                    presenter.requestPermission(getRxPermissions());
                    break;
                case R.id.image_iv_refresh:
                    break;
            }
        }
    }


    private Map<String, Object> getMap() {

        Map<String, Object> parameter = new HashMap<>();


        String nickName = viewBind.etNickName.getText().toString();
        if (EmptyUtils.isEmpty(nickName)) {
            ToastUtils.showShort(getString(R.string.please_input_name));
            return null;
        }
        parameter.put("display", nickName);

        String account = viewBind.etAccount.getText().toString();
        if (EmptyUtils.isEmpty(account)) {
            ToastUtils.showShort(getString(R.string.please_input_account));
            return null;
        }
        parameter.put("username", account);

        String pwd = viewBind.passwordEdit.getText().toString();
        if (EmptyUtils.isEmpty(pwd)) {
            ToastUtils.showShort(viewBind.passwordEdit.getHint());
            return null;
        }
        parameter.put("password", pwd);

        String confirmPwd = viewBind.confirmPasswordEdit.getText().toString();
        if (EmptyUtils.isEmpty(confirmPwd)) {
            ToastUtils.showShort(viewBind.confirmPasswordEdit.getHint());
            return null;
        }
        if (!pwd.equals(confirmPwd)) {
            ToastUtils.showShort(getString(R.string.tip_change_device_lock_password_input_incorrect));
            return null;
        }
        parameter.put("confirmPassword", confirmPwd);

        if (regType == 1) {//手机号注册
            String phoneNumber = viewBind.phoneNumberEdit.getText().toString();
            if (EmptyUtils.isEmpty(phoneNumber)) {
                ToastUtils.showShort(viewBind.phoneNumberEdit.getHint());
                return null;
            }

            if (phoneNumber.length() != 11) {
                ToastUtils.showShort(getString(R.string.phone_right_number));
                return null;
            }
            parameter.put("phone", phoneNumber);
            parameter.put("countryCode", viewBind.tvPrefix.getText().toString());
        } else {//邮箱注册
            String email = viewBind.emailEdit.getText().toString();
            if (EmptyUtils.isEmpty(email)) {
                ToastUtils.showShort(viewBind.emailEdit.getHint());
                return null;
            }
//            if (ValidatorUtil.isEmail(email)) {
//                ToastUtils.showShort(getString(R.string.email_right_address));
//                return null;
//            }
            parameter.put("email", email);
        }
        parameter.put("regtype", regType);
        if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
            String code = viewBind.authCodeEdit.getText().toString();
            if (EmptyUtils.isEmpty(code)) {
                ToastUtils.showShort(getString(R.string.input_message_code));
                return null;
            }
            parameter.put("verycode", code);
        }
        String avatar = (String) viewBind.ivChoosePicture.getTag(R.id.key_avatar);
        parameter.put("avatar", EmptyUtils.isNotEmpty(avatar) ? avatar : "");
        parameter.put("business", "reg");

        return parameter;

    }


    @Override
    public void regCallback(Response<String> response) {
        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            finish();
        } else {
            if (Response.NEED_MSG_STATUS.equals(response.getStatus())) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void uploadCallback(Response response) {
        hideLoading();
        if (response.isSuccess() && EmptyUtils.isNotEmpty(response.getUrl())) {
            viewBind.ivChoosePicture.setTag(R.id.key_avatar, response.getUrl());
            viewBind.ivChoosePicture.setScaleType(ImageView.ScaleType.CENTER_CROP);
            EasyLoadImage.loadImage2(App.getInst(), R.color.transparent, response.getUrl(), viewBind.ivChoosePicture);
        }
    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {

            Map<String, Object> parameter = getMap();
            if (parameter != null) {
                viewBind.etAccount.setTag(viewBind.etAccount.getText().toString());
                showLoading();
                presenter.requestRegister(parameter);
            }

        }
    }

    @Override
    public void permissionReadWriteCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {
            ImagePicker.picker().pick(this, REQUEST_CODE_PICK_IMAGE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        if (checkedId == R.id.rbEmail) {//
            regType = 2;
            viewBind.tvPrefix.setVisibility(View.GONE);
            viewBind.phoneNumberEdit.setVisibility(View.GONE);
            viewBind.emailEdit.setVisibility(View.VISIBLE);
        } else {
            regType = 1;
            viewBind.tvPrefix.setVisibility(View.VISIBLE);
            viewBind.emailEdit.setVisibility(View.GONE);
            viewBind.phoneNumberEdit.setVisibility(View.VISIBLE);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_PICK_IMAGE:
                    ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    if (images == null || images.isEmpty()) {
                        return;
                    }
                  /*File thumbImgFile = ImageUtils.genThumbImgFile(images.get(0).path);
                    if (thumbImgFile == null) {
                        return;
                    }
                    String imagePath = thumbImgFile.getAbsolutePath();*/
                    File chooseFile = new File(images.get(0).path);
                    if (!FileUtils.checkFileSizeIsLimit(chooseFile.length(), 300, "K")) {
                        PhotoUtils.startCorpImage(this, images.get(0).path, START_CUT_AVATAR);
                    } else {
                        presenter.requestUploadFile(RegisterActivity.this, chooseFile);
                    }
                    break;
                case START_CUT_AVATAR:
                    final String cutPath = FileUtils.getFilePath(FileConstant.TYPE_PHOTO, this) + "cutAvatar.png";
                    UploadHelp.uploadDeal(cutPath, (status, file) -> {
                        if (status == 0) {
                            showLoading();
                        } else if (status == -1) {
                            hideLoading();
                        } else if (status == 1) {
                            presenter.requestUploadFile(this, file);
                        }
                    });
                    break;
            }

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String needShowAccount = (String) viewBind.etAccount.getTag();//需要验证码的手机号
        if (EmptyUtils.isEmpty(needShowAccount)) {//
            if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {//输入框为空隐藏验证码
                viewBind.authCodeLl.setVisibility(View.GONE);
            }
        } else {
            if (needShowAccount.equals(s.toString())) {//显示验证码
                if (viewBind.authCodeLl.getVisibility() == View.GONE) {
                    viewBind.authCodeLl.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                    viewBind.authCodeLl.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

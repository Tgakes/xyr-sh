package com.easy.app.ui.mvp.transfer;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface TransferView extends BaseView {


    void transferCallback(Response<String> response);

}

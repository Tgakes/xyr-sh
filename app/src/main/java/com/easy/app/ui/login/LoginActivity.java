package com.easy.app.ui.login;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppSharePreferences;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.LoginBinding;
import com.easy.app.helper.PasswordHelper;
import com.easy.apt.annotation.ActivityInject;
import com.easy.apt.lib.SharePreference;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.framework.statusbar.StatusBarUtils;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SMSCountDownTimer;
import com.easy.widget.SkinTextView;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;

import cn.wildfire.chat.kit.ChatManagerHolder;

@ActivityInject
@Route(path = RouterManager.LOGIN_ACTIVITY, name = "登录页面")
public class LoginActivity extends BaseActivity<LoginPresenter, LoginBinding> implements LoginView, View.OnClickListener, TextWatcher {

//    SkinTextView skinTvTitle;


    AppSharePreferences appSharePreferences;

    @Override
    public void initStateBar() {

        StatusBarUtils.with(this).init();
        StatusBarUtil.setStatusBarDarkTheme(this, true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.login;
    }

    @Override
    public void initView() {
        if (appSharePreferences == null) {
            appSharePreferences = SharePreference.get(App.getInst(),AppSharePreferences.class);
        }
        ActivityManager.getInstance().finishOtherActivity();
       /* SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTvTitle = skinTitleView.setTitleText(getString(R.string.login));
            skinTitleView.hideBackBtn();
        }*/
        if (EmptyUtils.isNotEmpty(appSharePreferences.getUserName())) {
            viewBind.userNameEdit.setText(appSharePreferences.getUserName());
        }
        viewBind.passwordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        viewBind.changeLoginStyleBtn.setTextColor(SkinUtils.getSkin(this).getAccentColor());
        PasswordHelper.bindPasswordEye(viewBind.passwordEdit, viewBind.tbEye);
        viewBind.sendAgainBtn.setOnClickListener(this);
        viewBind.loginBtn.setOnClickListener(this);
        viewBind.forgetPasswordBtn.setOnClickListener(this);
        viewBind.registerAccountBtn.setOnClickListener(this);
        viewBind.userNameEdit.addTextChangedListener(this);
        viewBind.changeLoginStyleBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.change_login_style_btn://切换登录方式

                    break;
            /*    case R.id.send_again_btn:

                    String userName = viewBind.userNameEdit.getText().toString();
                    if (EmptyUtils.isEmpty(userName)) {
                        ToastUtils.showShort(viewBind.userNameEdit.getHint());
                        return;
                    }

                   *//* if (userName.length() != 11) {
                        ToastUtils.showShort(getString(R.string.phone_right_number));
                        return;
                    }*//*

                    viewBind.sendAgainBtn.setEnabled(false);
                    viewBind.sendAgainBtn.setText(getString(R.string.getting));
                    presenter.requestSms(userName);
                    break;*/
                case R.id.login_btn:
                    String userName = viewBind.userNameEdit.getText().toString();
                    if (EmptyUtils.isEmpty(userName)) {
                        ToastUtils.showShort(viewBind.userNameEdit.getHint());
                        return;
                    }

                    String pwd = viewBind.passwordEdit.getText().toString();
                    if (EmptyUtils.isEmpty(pwd)) {
                        ToastUtils.showShort(getString(R.string.please_input_password));
                        return;
                    }
                    String code = "";
                    if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                        code = viewBind.authCodeEdit.getText().toString();
                        if (EmptyUtils.isEmpty(code)) {
                            ToastUtils.showShort(getString(R.string.input_message_code));
                            return;
                        }
                    }
                    viewBind.userNameEdit.setTag(userName);
                    showLoading();
                    presenter.requestLogin(userName, code, pwd);
                    break;
                case R.id.forget_password_btn:
                    RouterManager.goFindPwdActivity();
                    break;
                case R.id.register_account_btn:
                    RouterManager.goRegisterActivity();
                    break;
            }
        }
    }

    @Override
    public void loginCallback(Response<Accounts> response) {

        hideLoading();
        Accounts accounts;
        if (response.isSuccess() && Response.SUCCESS_STATUS.equals(response.getStatus()) && (accounts = response.getResultObj()) != null) {

            appSharePreferences.setUserName((String) viewBind.userNameEdit.getTag());
            ChatManagerHolder.gChatManager.connect(accounts.getUid(), accounts.getImtoken());
            PreferenceUtils.putString(App.getInst(), "id", accounts.getUid());
            PreferenceUtils.putString(App.getInst(), "token", accounts.getImtoken());
            RouterManager.goHomeActivity(this);
        } else {
            ToastUtils.showShort(response.getMsg());
            if (Response.NEED_MSG_STATUS.equals(response.getStatus())) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
            }
        }
    }

  /*  @Override
    public void smsCallback(Response response) {
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            new SMSCountDownTimer(viewBind.sendAgainBtn, 60000, 1000, getString(R.string.get_msg_code));
        } else {
            viewBind.sendAgainBtn.setEnabled(true);
            viewBind.sendAgainBtn.setText(getString(R.string.get_msg_code));
        }
    }*/

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        String needShowPhone = (String) viewBind.userNameEdit.getTag();//需要验证码的手机号
        if (EmptyUtils.isEmpty(needShowPhone)) {//
            if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {//输入框为空隐藏验证码
                viewBind.authCodeLl.setVisibility(View.GONE);
            }
        } else {
            if (needShowPhone.equals(s.toString())) {//显示验证码
                if (viewBind.authCodeLl.getVisibility() == View.GONE) {
                    viewBind.authCodeLl.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                    viewBind.authCodeLl.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}

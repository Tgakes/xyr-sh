package com.easy.app.ui.changePwd;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface ChangePwdView extends BaseView {


    void changePwdCallback(Response<String> response);
}

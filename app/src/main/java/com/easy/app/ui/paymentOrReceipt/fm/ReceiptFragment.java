package com.easy.app.ui.paymentOrReceipt.fm;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Receipt;
import com.easy.app.databinding.FmReceiptBinding;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.loadimage.EasyLoadImage;
import com.easy.qrcode.CodeUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.EncryptUtils;
import com.easy.utils.FileUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.Constants;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinTitleView;


import cn.wildfire.chat.kit.user.UserViewModel;
import cn.wildfirechat.model.UserInfo;


/**
 * A simple {@link ReceiptFragment} subclass.
 * Use the {@link ReceiptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class ReceiptFragment extends BaseFragment<ReceiptPresenter, FmReceiptBinding> implements ReceiptView {


    private String money, description, mLoginUserId;

    private int resumeCount;


    public ReceiptFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReceiptFragment newInstance() {
        ReceiptFragment fragment = new ReceiptFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_receipt;
    }

    @Override
    public void initView(View view) {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.receipt));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }
        initEvent();
        getSelfInfo();
    }

    private void initEvent() {
        viewBind.rlSetMoney.setOnClickListener(v -> {
            if (EmptyUtils.isNotEmpty(mLoginUserId)) {
                if (!TextUtils.isEmpty(money)) { // 清除金额
                    money = "";
                    description = "";
                    PreferenceUtils.putString(requireContext(), Constants.RECEIPT_SETTING_MONEY + mLoginUserId, money);
                    PreferenceUtils.putString(requireContext(), Constants.RECEIPT_SETTING_DESCRIPTION + mLoginUserId, description);
                    viewBind.rpSetMoneyTv.setText(getString(R.string.rp_receipt_tip2));
                    refreshView();
                    refreshReceiptQRCode();
                } else { // 设置金额

                    RouterManager.goReceiptSetMoneyActivity(mLoginUserId);
                }
            }

        });
        //保存收款码
        viewBind.rlSaveReceiptCode.setOnClickListener(v -> {
            if (ViewUtils.canClick()) {
                if (EmptyUtils.isNotEmpty(mLoginUserId)) {
                    viewBind.llTwoItem.setVisibility(View.GONE);
                    viewBind.rlQrCodeLayout.setBackgroundResource(R.mipmap.background__layer);
                    FileUtils.saveImageToGallery2(requireContext(), getBitmap(viewBind.rlQrCodeLayout), getString(R.string.tip_saved_qr_code));
                    viewBind.rlQrCodeLayout.setBackground(null);
                    viewBind.llTwoItem.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void getSelfInfo() {
        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        String userId = userViewModel.getUserId();
        userViewModel.getUserInfoAsync(userId, false).observe(getViewLifecycleOwner(), userInfo -> {
            if (userInfo == null) {
                return;
            }
            mLoginUserId = userId;
            getSetInfo();
//            EasyLoadImage.loadImage2(App.getInst(), R.drawable.app_logo, userInfo.portrait, viewBind.ivAvatar);
            Glide.with(this)
                    .asBitmap()
                    .load(userInfo.portrait)
                    .placeholder(R.drawable.app_logo)
                    .into(new CustomViewTarget<ImageView, Bitmap>(viewBind.ivAvatar) {
                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            // the errorDrawable will always be bitmapDrawable here
                            if (errorDrawable instanceof BitmapDrawable) {
                                Bitmap bitmap = ((BitmapDrawable) errorDrawable).getBitmap();
                                viewBind.ivAvatar.setImageBitmap(bitmap);
                                viewBind.ivAvatar.setTag(R.id.tag_key_list_item_type,bitmap);
                                setReceiptData(userInfo);

                            }
                        }

                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition transition) {
                            viewBind.ivAvatar.setImageBitmap(resource);
                            viewBind.ivAvatar.setTag(R.id.tag_key_list_item_type,resource);
                            setReceiptData(userInfo);
                        }

                        @Override
                        protected void onResourceCleared(@Nullable Drawable placeholder) {}
                    });



        });
    }

    private void setReceiptData(UserInfo userInfo){

        Receipt receipt = new Receipt();
        receipt.setUserId(mLoginUserId);
        receipt.setUserName(userInfo.displayName);
        receipt.setAvatar(userInfo.portrait);
        viewBind.rpQrCodeIv.setTag(receipt);
        refreshReceiptQRCode();
        refreshView();
    }

    @Override
    public void onResume() {
        super.onResume();

        resumeCount++;
        if (EmptyUtils.isNotEmpty(mLoginUserId) && resumeCount > 1) {
            getSetInfo();
            refreshView();
            refreshReceiptQRCode();
        }
    }


    private void getSetInfo() {
        money = PreferenceUtils.getString(requireContext(), Constants.RECEIPT_SETTING_MONEY + mLoginUserId);
        description = PreferenceUtils.getString(requireContext(), Constants.RECEIPT_SETTING_DESCRIPTION + mLoginUserId);
    }


    private void refreshReceiptQRCode() {
        Receipt receipt = (Receipt) viewBind.rpQrCodeIv.getTag();
        if (receipt == null) {
            return;
        }


        receipt.setMoney(money);
        receipt.setDescription(description);
        String content = JSON.toJSONString(receipt);
        Bitmap qrBitmap = CodeUtils.createQRCode(EncryptUtils.encryptText(content), 400, (Bitmap) viewBind.ivAvatar.getTag(R.id.tag_key_list_item_type));
        viewBind.rpQrCodeIv.setImageBitmap(qrBitmap);
    }


    private void refreshView() {
        viewBind.rpMoneyTv.setText(money);
        viewBind.rpDescTv.setText(description);

        if (!TextUtils.isEmpty(money)) {
            viewBind.rpSetMoneyTv.setText(getString(R.string.rp_receipt_tip3));
            viewBind.rpMoneyTv.setVisibility(View.VISIBLE);
        } else {
            viewBind.rpSetMoneyTv.setText(getString(R.string.rp_receipt_tip2));
            viewBind.rpMoneyTv.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(description)) {
            viewBind.rpDescTv.setVisibility(View.VISIBLE);
        } else {
            viewBind.rpDescTv.setVisibility(View.GONE);
        }
    }

    /**
     * 获取这个view的缓存bitmap,
     */
    private Bitmap getBitmap(View view) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap result = Bitmap.createBitmap(view.getDrawingCache());
        view.destroyDrawingCache();
        view.setDrawingCacheEnabled(false);
        return result;
    }


}

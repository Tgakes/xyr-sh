package com.easy.app.ui.withdrawCoins;


import com.easy.app.bean.Balance;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface WithdrawCoinsView extends BaseView {

    void balanceCallback(Response<Balance> response);

    void withdrawalCallback(Response response);

}

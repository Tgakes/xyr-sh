package com.easy.app.ui.home.fragment.market.hall;

import androidx.lifecycle.Lifecycle;

import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.MarketHall;
import com.easy.app.bean.MarketNotice;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HallPresenter extends AppPresenter<HallView> {

    @Inject
    public HallPresenter() {

    }




    public void requestList() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "market");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(List.class)
                .map(response -> {

                    if (response.isSuccess()) {
                        response.setResultObj(JSON.parseArray(response.getResult(), MarketHall.class));
                    }
                    return response;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.marketListCallback(result);
                        },
                        throwable -> {
                            Response<List> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.marketListCallback(response);
                        });
    }

    public List<MarketNotice> getMarketNotice() {

        List<MarketNotice> marketNoticeList = new ArrayList<>();
        marketNoticeList.add(new MarketNotice("最后4天，Lon创世挖矿即将结束！"));
        return marketNoticeList;
    }
}

package com.easy.app.ui.payCenter;

import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.PayCenterBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

@ActivityInject
@Route(path = RouterManager.PAY_CENTER, name = "支付中心")
public class PayCenterActivity extends BaseActivity<PayCenterPresenter, PayCenterBinding> implements PayCenterView, View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.pay_center;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.payment_center));
        }

        viewBind.resetPassword.setVisibility(View.GONE);
        viewBind.bill.setOnClickListener(this);
        viewBind.settingPassword.setOnClickListener(this);
        viewBind.resetPassword.setOnClickListener(this);
        viewBind.tvPayPwdText.setText(getString(presenter.accountsDao.get().isSetPwd() ? R.string.btn_change_pay_password : R.string.btn_set_pay_password));
    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.bill:
                    RouterManager.goBillActivity();
                    break;
                case R.id.setting_password:
                    RouterManager.goSetPayPwd(presenter.accountsDao.get().isSetPwd() ? 1 : 0, "", "");
                    break;
                case R.id.reset_password:
                    RouterManager.goResetPayPwdActivity();
                    break;
            }
        }
    }
}

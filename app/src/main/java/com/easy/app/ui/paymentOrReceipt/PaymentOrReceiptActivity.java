package com.easy.app.ui.paymentOrReceipt;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.PaymentOrReceiptBinding;
import com.easy.app.ui.paymentOrReceipt.fm.ReceiptFragment;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.widget.SkinTitleView;

import java.util.ArrayList;
import java.util.List;

@ActivityInject
@Route(path = RouterManager.PAYMENT_OR_RECEIPT, name = "收付款")
public class PaymentOrReceiptActivity extends BaseActivity<PaymentOrReceiptPresenter, PaymentOrReceiptBinding> implements PaymentOrReceiptView {




    @Override
    public int getLayoutId() {
        return R.layout.payment_or_receipt;
    }

    @Override
    public void initView() {

        List<String> mTitleList = new ArrayList<>();
//        mTitleList.add(getString(R.string.payment_pay));
        mTitleList.add(getString(R.string.receipt));
        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new PaymentFragment());
        fragments.add(ReceiptFragment.newInstance());
        MyTabAdapter adapter = new MyTabAdapter(getSupportFragmentManager(), fragments, mTitleList);
        viewBind.csvPage.setAdapter(adapter);
        viewBind.sTabLayout.setViewPager(viewBind.csvPage);
        viewBind.csvPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                
            }

            @Override
            public void onPageSelected(int position) {
                notifyImage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        notifyImage(0);
        viewBind.paymentCode.setOnClickListener(v -> viewBind.csvPage.setCurrentItem(0, true));
        viewBind.collectionCode.setOnClickListener(v -> viewBind.csvPage.setCurrentItem(1, true));
        viewBind.sweepCode.setOnClickListener(v -> viewBind.csvPage.setCurrentItem(2, true));
    }


    private void notifyImage(int index) {
        if (index == 0) {
            viewBind.rlPay.setBackground(getResources().getDrawable(R.mipmap.biue_background));
            viewBind.paymentCode.setBackgroundResource(R.mipmap.payment_code_select_icon);
            viewBind.collectionCode.setBackgroundResource(R.mipmap.collection_code_icon);
            viewBind.sweepCode.setBackgroundResource(R.mipmap.sweep_code_icon);
        } else if (index == 1) {
            viewBind.rlPay.setBackground(getResources().getDrawable(R.mipmap.yellow_background));
            viewBind.paymentCode.setBackgroundResource(R.mipmap.payment_code_icon);
            viewBind.collectionCode.setBackgroundResource(R.mipmap.collection_code_selecet_icon);
            viewBind.sweepCode.setBackgroundResource(R.mipmap.sweep_code_icon);
        } else if (index == 2) {
            viewBind.paymentCode.setBackgroundResource(R.mipmap.payment_code_icon);
            viewBind.collectionCode.setBackgroundResource(R.mipmap.collection_code_icon);
            viewBind.sweepCode.setBackgroundResource(R.mipmap.sweep_code_select_icon);
        }
    }


}

package com.easy.app.ui.home.fragment.message;

import android.Manifest;

import androidx.lifecycle.Lifecycle;

import com.easy.app.base.AppPresenter;
import com.easy.store.bean.Accounts;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

public class MessagePresenter extends AppPresenter<MessageView> {

    @Inject
    public MessagePresenter() {

    }


    public Accounts getAccount() {

        return accountsDao.get().getAccounts();
    }

    public String getNickName() {
        Accounts accounts = getAccount();
        return accounts != null ? accounts.getDisplay() : "";
    }


    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.CAMERA)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }

}

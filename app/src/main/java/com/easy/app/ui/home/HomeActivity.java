package com.easy.app.ui.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.easy.app.BuildConfig;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppSharePreferences;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Receipt;
import com.easy.app.bean.Token;
import com.easy.app.databinding.HomeBinding;
import com.easy.app.ui.dialog.SelectionFrame;
import com.easy.app.util.IntentWrapper;
import com.easy.app.util.NotificationStatusUtils;
import com.easy.app.util.PermissionUtil;
import com.easy.apt.annotation.ActivityInject;
import com.easy.apt.lib.SharePreference;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.bean.AppVersion;
import com.easy.framework.manager.update.AppUpdateManager;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.framework.statusbar.StatusBarUtils;
import com.easy.net.beans.Response;
import com.easy.net.function.DataSwitchFunction;
import com.easy.qrcode.ui.qr_code.QrScanActivity;
import com.easy.utils.EmptyUtils;
import com.easy.utils.EncryptUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.utils.bean.DeviceInfoUtil;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cn.wildfire.chat.kit.IMConnectionStatusViewModel;
import cn.wildfire.chat.kit.IMServiceStatusViewModel;
import cn.wildfire.chat.kit.WfcScheme;
import cn.wildfire.chat.kit.WfcUIKit;
import cn.wildfire.chat.kit.channel.ChannelInfoActivity;
import cn.wildfire.chat.kit.contact.ContactViewModel;
import cn.wildfire.chat.kit.conversationlist.ConversationListViewModel;
import cn.wildfire.chat.kit.conversationlist.ConversationListViewModelFactory;
import cn.wildfire.chat.kit.group.GroupInfoActivity;
import cn.wildfire.chat.kit.user.UserInfoActivity;
import cn.wildfire.chat.kit.user.UserViewModel;
import cn.wildfire.chat.kit.viewmodel.MessageViewModel;
import cn.wildfirechat.client.ConnectionStatus;
import cn.wildfirechat.message.Message;
import cn.wildfirechat.message.UnknownMessageContent;
import cn.wildfirechat.message.core.MessageContentType;
import cn.wildfirechat.message.core.MessagePayload;
import cn.wildfirechat.message.core.MessageStatus;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.model.UserInfo;
import cn.wildfirechat.remote.ChatManager;
import me.leolin.shortcutbadger.ShortcutBadger;

@ActivityInject
@Route(path = RouterManager.HOME_ACTIVITY, name = "首页")
public class HomeActivity extends BaseActivity<HomePresenter, HomeBinding> implements HomeView, View.OnClickListener, RadioGroup.OnCheckedChangeListener {


    private int mCurrtTabId = -1;


    private ConversationListViewModel conversationListViewModel;

    ContactViewModel contactViewModel;

    private boolean isInitialized = false;

    public static final int REQUEST_CODE_SCAN_QR_CODE = 100;

    private static final int REQUEST_IGNORE_BATTERY_CODE = 101;


    boolean isShow;


    @Override
    public void initStateBar() {

        StatusBarUtils.with(this).init();
        StatusBarUtil.setStatusBarDarkTheme(this, true);
    }


    @Override
    public int getLayoutId() {
        return R.layout.home;
    }

    @Override
    public void initView() {
        isShow = false;
        closeSwipeBackLayout();
        LiveEventBus.get("logout", String.class).observe(this, this::callBackLogout);
        LiveEventBus.get("market", String.class).observe(this, this::selectMarket);
        LiveEventBus.get("transfer", UserInfo.class).observe(this, this::transferMoney);
        LiveEventBus.get("updateInfo", String.class).observe(this, this::updateInfo);
//       viewBind.rbTab1.setOnClickListener(this);
        viewBind.mainRg.setOnCheckedChangeListener(this);
//       viewBind.rbTab1.setChecked(true);
        //修改白屏bug
        viewBind.rbTab1.toggle();
        // 改皮肤，
       /* ColorStateList tabColor = SkinUtils.getSkin(this).getMainTabColorState();
        for (RadioButton radioButton : Arrays.asList(viewBind.rbTab1, viewBind.rbTab2, viewBind.rbHtml, viewBind.rbTab3, viewBind.rbTab4)) {
            radioButton.setTextColor(tabColor);
            // 图标着色，兼容性解决方案，
            Drawable drawable = radioButton.getCompoundDrawables()[1];
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTintList(drawable, tabColor);
            // 如果是getDrawable拿到的Drawable不能直接调setCompoundDrawables，没有宽高，
            radioButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        }*/

        viewBind.rbTab1.setTag(false);
        presenter.requestReadWritePermission(getRxPermissions());
        afterViews();
        // 检查是否开启通知栏权限
        checkNotifyStatus();
        presenter.requestGetToken();//监测是否在别处登录

    }

    private void updateInfo(String data) {
        Log.i("logs", "data1====" + data);
        String avatar = ",avatar";
        String nickName = ",nickName";
        if (data.endsWith(avatar)) {
            data = data.substring(0, data.length() - avatar.length());
            presenter.cutImage(this, data);
        } else if (data.endsWith(nickName)) {
            data = data.substring(0, data.length() - nickName.length());
            presenter.requestSetInfo("", data);
        }
//        Log.i("logs", "data2====" + data);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (contactViewModel != null) {
            contactViewModel.reloadFriendRequestStatus();
            conversationListViewModel.reloadConversationUnreadStatus();
        }
        updateMomentBadgeView();
        presenter.requestAppVersion();
    }

    private Observer<Boolean> imStatusLiveDataObserver = status -> {
        if (status && !isInitialized) {
            init();
            isInitialized = true;
        }
    };

    private void init() {
//        initView();

        conversationListViewModel = ViewModelProviders.of(this, new ConversationListViewModelFactory(Arrays.asList(Conversation.ConversationType.Single, Conversation.ConversationType.Group, Conversation.ConversationType.Channel), Arrays.asList(0)))
                .get(ConversationListViewModel.class);
        conversationListViewModel.unreadCountLiveData().observe(this, unreadCount -> {

            boolean hasCount = unreadCount != null && unreadCount.unread > 0;
            int unReadCount = 0;
            if (hasCount) {
                unReadCount = unreadCount.unread;
                showUnreadMessageBadgeView(unreadCount.unread);
            } else {
                hideUnreadMessageBadgeView();
            }
            showCount(unReadCount);
        });
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        contactViewModel.friendRequestUpdatedLiveData().observe(this, count -> {

            if (count == null || count == 0) {
                hideUnreadFriendRequestBadgeView();
            } else {
                showUnreadFriendRequestBadgeView(count);
            }
        });

        MessageViewModel messageViewModel = ViewModelProviders.of(this).get(MessageViewModel.class);
        messageViewModel.messageLiveData().observe(this, uiMessage -> {
            if (uiMessage.message.content.getMessageContentType() == MessageContentType.MESSAGE_CONTENT_TYPE_FEED
                    || uiMessage.message.content.getMessageContentType() == MessageContentType.MESSAGE_CONTENT_TYPE_FEED_COMMENT) {
                updateMomentBadgeView();
            }
        });

        ignoreBatteryOption();
    }

   /* private boolean checkDisplayName() {
        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        UserInfo userInfo = userViewModel.getUserInfo(userViewModel.getUserId(), false);
        if (userInfo != null && TextUtils.equals(userInfo.displayName, userInfo.mobile)) {
            if (!PreferenceUtils.getBoolean(App.getInst(), "updatedDisplayName", false)) {
                PreferenceUtils.putBoolean(App.getInst(), "updatedDisplayName", true);
                updateDisplayName();
                return false;
            }
        }
        return true;
    }*/

    private void ignoreBatteryOption() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                Intent intent = new Intent();
                String packageName = BuildConfig.APPLICATION_ID;
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                    intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    intent.setData(Uri.parse("package:" + packageName));
                    startActivityForResult(intent, REQUEST_IGNORE_BATTERY_CODE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 显示桌面角标数量
     */
    private void showCount(int unReadCount) {
        ShortcutBadger.applyCount(this, unReadCount);
    }

    private void showUnreadMessageBadgeView(int count) {

        ViewUtils.updateNum(viewBind.mainTabOneTv, count);
    }

    private void hideUnreadMessageBadgeView() {

        ViewUtils.updateNum(viewBind.mainTabOneTv, 0);

    }

    public void hideUnreadFriendRequestBadgeView() {
        ViewUtils.updateNum(viewBind.mainTabTwoTv, 0);

    }

    private void showUnreadFriendRequestBadgeView(int count) {
        ViewUtils.updateNum(viewBind.mainTabTwoTv, count);


    }


    private void transferMoney(UserInfo userInfo) {
        Receipt receipt = new Receipt();
        receipt.setAvatar(userInfo.portrait);
        receipt.setUserId(userInfo.uid);
        receipt.setUserName(userInfo.displayName);
        RouterManager.goTransferMoneyActivity(JSON.toJSONString(receipt), getString(R.string.transfer_money_to_someone, userInfo.displayName), 2);
    }


    /**
     * OPPO手机：App的通知默认是关闭的，需要检查通知是否开启
     * OPPO手机：App后台时，调用StartActivity方法不起做用，需提示用户至 手机管家-权限隐私-自启动管理 内该App的自启动开启
     * <p>
     * 小米与魅族手机需要开启锁屏显示权限，否则在锁屏时收到音视频消息来电界面无法弹起（其他手机待测试，华为手机无该权限设置，锁屏时弹起后直接干掉弹起页面）
     */
    private void checkNotifyStatus() {


        if (NotificationManagerCompat.from(this).areNotificationsEnabled()) {//通知权限

            boolean isEnter = checkDetailsNotification();

            if (isEnter) {
                String tip = "";
                if (!IntentWrapper.isNotificationEnabled(this)) {
                    tip = getString(R.string.title_notification) + "\n" + getString(R.string.content_notification);
                }
                if (DeviceInfoUtil.isOppoRom()) {// 如果Rom为OPPO，还需要提醒用户开启自启动
                    tip += getString(R.string.open_auto_launcher);
                }
                if (!TextUtils.isEmpty(tip)) {
                    SelectionFrame dialog = new SelectionFrame(this);
                    dialog.setSomething(null, tip, new SelectionFrame.OnSelectionFrameClickListener() {
                        @Override
                        public void cancelClick() {

                        }

                        @Override
                        public void confirmClick() {
                            PermissionUtil.startApplicationDetailsSettings(HomeActivity.this, 0x001);
                        }
                    });
                    dialog.show();
                } else {
                    AppSharePreferences appSharePreferences = SharePreference.get(App.getInst(), AppSharePreferences.class);
                    if (!appSharePreferences.isShowSelfStart()) {//引导用户过去设置开启自启动
                        appSharePreferences.setShowSelfStart(true);
                        goSettingNotification(2, getString(R.string.self_start_des));
                    }
                }
            }

        } else {
            goSettingNotification(1, getString(R.string.notification_start_des));
        }
    }

    /**
     * 检测具体的通知类型是否打开通知栏
     * @return
     */
    private boolean checkDetailsNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            List<NotificationChannel> notificationChannelList = NotificationStatusUtils.getNotificationChannel(App.getInst());
            if (notificationChannelList != null && !notificationChannelList.isEmpty()) {
                for (NotificationChannel notificationChannel : notificationChannelList) {
                    if (!NotificationStatusUtils.checkNotificationsChannelEnabled(App.getInst(), notificationChannel.getId())) {
                        goSettingNotification(3, getString(R.string.notification_details_des, notificationChannel.getName()), notificationChannel.getId());
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void goSettingNotification(int type, String content, String... channelId) {


        SelectionFrame dialog = new SelectionFrame(this);
        dialog.setSomething(getString(R.string.start_set), content, getString(R.string.cancel), getString(R.string.immediately_set), new SelectionFrame.OnSelectionFrameClickListener() {
            @Override
            public void cancelClick() {
            }

            @Override
            public void confirmClick() {
                switch (type) {
                    case 1://未打开外层的通知栏开关
                        NotificationStatusUtils.showInstalledAppDetails(HomeActivity.this, BuildConfig.APPLICATION_ID);
                        break;
                    case 2://引导用户过去设置开启自启动
                        RouterManager.goSettingActivity();
                        break;
                    case 3://打开内层具体通知栏开关
                        if (channelId != null && channelId.length > 0) {
                            NotificationStatusUtils.goSettingNotificationPage(channelId[0], HomeActivity.this);
                        }
                        break;
                }
            }
        });
        dialog.show();
    }


    //解决 moveTaskToBack(false) 无法退出到后台
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {//
            // 退出后台运行
            moveTaskToBack(false);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public synchronized void callBackLogout(String data) {
        Log.i("logs", "data===" + data);
        if (DataSwitchFunction.TOKEN_ERROR.equals(data)) {
            showExitDialog();
        } else {
            presenter.requestGetToken();
        }


    }

    private void selectMarket(Object o) {

        if (!viewBind.rbHtml.isChecked()) {
            viewBind.rbHtml.performClick();
        }

    }

    protected void afterViews() {
//        bottomNavigationView.setItemIconTintList(null);
        IMServiceStatusViewModel imServiceStatusViewModel = ViewModelProviders.of(this).get(IMServiceStatusViewModel.class);
        imServiceStatusViewModel.imServiceStatusLiveData().observe(this, imStatusLiveDataObserver);
        IMConnectionStatusViewModel connectionStatusViewModel = ViewModelProviders.of(this).get(IMConnectionStatusViewModel.class);
        connectionStatusViewModel.connectionStatusLiveData().observe(this, status -> {
            Log.i("logs", "status===" + status);
            if (status == ConnectionStatus.ConnectionStatusTokenIncorrect || status == ConnectionStatus.ConnectionStatusSecretKeyMismatch || status == ConnectionStatus.ConnectionStatusRejected || status == ConnectionStatus.ConnectionStatusLogout) {
                ChatManager.Instance().disconnect(true, true);
                reLogin();
            }
        });
        MessageViewModel messageViewModel = ViewModelProviders.of(this).get(MessageViewModel.class);
        messageViewModel.messageLiveData().observe(this, uiMessage -> {
            Log.i("logs", "message1===" + uiMessage.message);
            if (uiMessage.message.content instanceof UnknownMessageContent) {
                UnknownMessageContent unknownMessageContent = (UnknownMessageContent) uiMessage.message.content;
                MessagePayload messagePayload = unknownMessageContent.getOrignalPayload();
                if (messagePayload.contentType == MessageContentType.OTHER_DEVICE_LOGIN) {
                    callBackLogout(messagePayload.content);
                }
            }
        });
    }


    private void updateMomentBadgeView() {
        if (!WfcUIKit.getWfcUIKit().isSupportMoment()) {
            return;
        }
        List<Message> messages = ChatManager.Instance().getMessagesEx2(Collections.singletonList(Conversation.ConversationType.Single), Collections.singletonList(1), Arrays.asList(MessageStatus.Unread), 0, true, 100, null);
        int count = messages == null ? 0 : messages.size();
        if (count > 0) {
            ViewUtils.updateNum(viewBind.mainTabThreeTv, count);
        } else {
            ViewUtils.updateNum(viewBind.mainTabThreeTv, 0);
        }
    }

    private void reLogin() {

        PreferenceUtils.clearData(App.getInst());
        presenter.accountsDao.get().delete();
        RouterManager.goLoginActivity(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rb_tab_1:
               /* if (ViewUtils.doubleClick()) {
                    String date = DateUtils.getCurrentTimeDate(12);
                    String[] dates = date.split("-");
                    BigDecimal b1 = new BigDecimal(BuildConfig.VERSION_NAME);
                    BigDecimal b2 = new BigDecimal("0.1");
                    String newVersion = String.valueOf(b1.add(b2).doubleValue());
                    AppVersion appVersion = new AppVersion();
                    appVersion.setNewversion(newVersion);
                    appVersion.setDownloadurl("http://myyc.boxzhi.com//uploads/app_file/android/" + dates[0] + "/" + dates[1] + "/" + newVersion + ".apk");
                    showUpdateDialog(appVersion);
                }*/
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (mCurrtTabId != checkedId) {
            mCurrtTabId = checkedId;
            presenter.changeFragment(checkedId, HomeActivity.this);
//            if (checkedId == R.id.rb_tab_1) {
//                updateNumData();
//            }
//            JCVideoPlayer.releaseAllVideos();
        }
    }


    @Override
    public void permissionReadWriteCallback(Boolean granted, Throwable e) {
        if (e != null) {
            ToastUtils.showShort("error：" + e.getMessage());
            viewBind.rbTab1.setTag(false);
        } else {
            viewBind.rbTab1.setTag(granted != null && granted);
            showUpdateDialog((AppVersion) viewBind.mainContent.getTag());
        }
    }

    public void showUpdateDialog(AppVersion appVersion) {
        if (appVersion == null) {
            return;
        }
        AppUpdateManager updateManager = new AppUpdateManager(this);
        updateManager.showUpdateDialog(appVersion, new AppUpdateManager.AppUpdateCallback() {
            @Override
            public boolean permission() {

                return (boolean) viewBind.rbTab1.getTag();
            }

            @Override
            public void permissionCallback() {

                ToastUtils.showShort(getString(R.string.download_need_premission));
                presenter.requestReadWritePermission(getRxPermissions());
            }
        });
    }

    @Override
    public void appVersionCallback(Response<AppVersion> response) {

        if (response.isSuccess() && response.getResultObj() != null) {
            viewBind.mainContent.setTag(response.getResultObj());
            showUpdateDialog(response.getResultObj());
        } else {
//            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void tokenCallback(Response<Token> response) {

        if (!response.isSuccess()) {
            showExitDialog();
        }
    }

    private void showExitDialog() {

        PreferenceUtils.putBoolean(App.getInst(), "exit", true);
        if (presenter.accountsDao != null && !isShow) {
            Activity activity = com.easy.framework.manager.activity.ActivityManager.getInstance().getCurrentActivity();
            if (activity != null) {
                isShow = true;
                new AlertDialog.Builder(activity)
                        .setCancelable(false)
                        .setMessage(getString(R.string.other_device_login))
                        .setPositiveButton(getString(R.string.sure), (d, w) -> {
                            PreferenceUtils.putBoolean(App.getInst(), "exit", false);
                            ((App) App.getInst()).logout(presenter.accountsDao.get(), activity, true);
                        })
                        .show();
            }
        }
    }

    @Override
    public void uploadCallback(Response response) {

        if (response.isSuccess() && EmptyUtils.isNotEmpty(response.getUrl())) {

            presenter.requestSetInfo(response.getUrl(), "");
            /*
            MutableLiveData<OperateResult<Boolean>> resultLiveData = new MutableLiveData<>();
            List<ModifyMyInfoEntry> entries = new ArrayList<>();
            entries.add(new ModifyMyInfoEntry(ModifyMyInfoType.Modify_Portrait, result));
            ChatManager.Instance().modifyMyInfo(entries, new GeneralCallback() {
                @Override
                public void onSuccess() {
                    resultLiveData.setValue(new OperateResult<Boolean>(true, 0));
                }

                @Override
                public void onFail(int errorCode) {
                    resultLiveData.setValue(new OperateResult<>(errorCode));
                }
            });*/

//            viewBind.ivChoosePicture.setTag(R.id.key_avatar, response.getUrl());
//            viewBind.ivChoosePicture.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            EasyLoadImage.loadImage2(App.getInst(), R.color.transparent, response.getUrl(), viewBind.ivChoosePicture);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN_QR_CODE:
                if (resultCode == RESULT_OK) {
                    String result = data.getStringExtra(QrScanActivity.SCAN_RESULT);
                    onScanPcQrCode(result);
                }
                break;
            case REQUEST_IGNORE_BATTERY_CODE:
                if (resultCode == RESULT_CANCELED) {
                    ToastUtils.showShort(getString(R.string.allow_back_run_msg_real));
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void onScanPcQrCode(String qrCodeData) {
        String newData = EncryptUtils.decryptText(qrCodeData);//先进行解密来判断是否可解，可解则表示为扫码支付
        if (qrCodeData.equals(newData)) {
            String prefix = qrCodeData.substring(0, qrCodeData.lastIndexOf('/') + 1);
            String value = qrCodeData.substring(qrCodeData.lastIndexOf("/") + 1);
            switch (prefix) {
                case WfcScheme.QR_CODE_PREFIX_PC_SESSION:
                    pcLogin(value);
                    break;
                case WfcScheme.QR_CODE_PREFIX_USER:
                    showUser(value);
                    break;
                case WfcScheme.QR_CODE_PREFIX_GROUP:
                    joinGroup(value);
                    break;
                case WfcScheme.QR_CODE_PREFIX_CHANNEL:
                    subscribeChannel(value);
                    break;
                default:
                    Log.i("logs", "qrCodeData===" + qrCodeData);
                    ToastUtils.showShort(getString(R.string.scan_error));
                    break;
            }
        } else {
            RouterManager.goReceiptPayMoneyActivity(1, newData);
        }

    }

    private void pcLogin(String token) {
//        Intent intent = new Intent(this, PCLoginActivity.class);
//        intent.putExtra("token", token);
//        startActivity(intent);
    }

    private void showUser(String uid) {

        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        UserInfo userInfo = userViewModel.getUserInfo(uid, true);
        if (userInfo == null) {
            return;
        }
        Intent intent = new Intent(this, UserInfoActivity.class);
        intent.putExtra("userInfo", userInfo);
        startActivity(intent);
    }

    private void joinGroup(String groupId) {
        Intent intent = new Intent(this, GroupInfoActivity.class);
        intent.putExtra("groupId", groupId);
        startActivity(intent);
    }

    private void subscribeChannel(String channelId) {
        Intent intent = new Intent(this, ChannelInfoActivity.class);
        intent.putExtra("channelId", channelId);
        startActivity(intent);
    }


}

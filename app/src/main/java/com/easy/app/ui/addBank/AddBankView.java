package com.easy.app.ui.addBank;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface AddBankView extends BaseView {

    void addBankCardCallback(Response response);


    void realNameCallback(Response response);

}

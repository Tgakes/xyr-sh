package com.easy.app.ui.home.fragment.market;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.graphics.drawable.DrawableCompat;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.utils.DimensUtils;
import com.easy.utils.StreamUtils;
import com.easy.widget.SkinUtils;
import com.google.android.material.tabs.TabLayout;

import java.lang.reflect.Field;

import javax.inject.Inject;

public class MarketPresenter extends AppPresenter<MarketView> {

    @Inject
    public MarketPresenter() {

    }



    public void setTabWidth(final TabLayout tabLayout, final int margin) {
        tabLayout.post(() -> {
            try {
                Field slidingTabIndicatorField = tabLayout.getClass().getDeclaredField("slidingTabIndicator");
                slidingTabIndicatorField.setAccessible(true);
                LinearLayout mTabStrip = (LinearLayout) slidingTabIndicatorField.get(tabLayout);
                int childCount = mTabStrip.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View tabView = mTabStrip.getChildAt(i);
                    Field textViewField = tabView.getClass().getDeclaredField("textView");
                    textViewField.setAccessible(true);
                    TextView mTextView = (TextView) textViewField.get(tabView);
                    tabView.setPadding(0, 0, 0, 0);
                    int width = mTextView.getWidth();
//                    Log.i("logs", "width===" + width + ",i----" + i);
                    if (width == 0) {
                        mTextView.measure(0, 0);
                        width = mTextView.getMeasuredWidth();
                    }

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                    params.width = width + (i == 0 ? DimensUtils.dp2px(App.getInst(), 18f) : 0);
                    params.leftMargin = margin;
                    params.rightMargin = margin;
                    tabView.setLayoutParams(params);
                    tabView.invalidate();
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

    }


    public View getTabView(Activity activity, String title, int res, int... textColors) {

        View view = LayoutInflater.from(activity).inflate(R.layout.custom_tab_item, null);
        TextView tab_text = view.findViewById(R.id.tab_text);
        if (textColors != null && textColors.length > 0) {
            tab_text.setTextColor(StreamUtils.getInstance().resourceToColorStateList(textColors[0], App.getInst()));
        }
        tab_text.setText(title);
        if (res != 0) {
            ImageView tab_icon = view.findViewById(R.id.tab_icon);
            tab_icon.setVisibility(View.VISIBLE);
            Drawable drawable = StreamUtils.getInstance().resourceToDrawable(res,App.getInst());
            DrawableCompat.setTintList(DrawableCompat.wrap(drawable), SkinUtils.getSkin(App.getInst()).getMarketTabColorState());
            tab_icon.setImageDrawable(drawable);

        }
        return view;
    }


}

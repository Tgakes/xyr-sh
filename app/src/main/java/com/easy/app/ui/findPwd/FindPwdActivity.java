package com.easy.app.ui.findPwd;

import android.view.View;
import android.widget.EditText;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.EmptyBinding;
import com.easy.app.databinding.FindPwdBinding;
import com.easy.app.ui.empty.EmptyPresenter;
import com.easy.app.ui.empty.EmptyView;
import com.easy.app.util.ButtonColorChange;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.widget.SkinTitleView;

import java.util.ArrayList;
import java.util.List;

@ActivityInject
@Route(path = RouterManager.FIND_PWD_ACTIVITY, name = "忘记密码页面")
public class FindPwdActivity extends BaseActivity<FindPwdPresenter, FindPwdBinding> implements FindPwdView , View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.find_pwd;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.forget_password));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }
//        ButtonColorChange.colorChange(App.getInst(), viewBind.setPwdBtn);
//        ButtonColorChange.colorChange(App.getInst(), viewBind.sendAgainBtn);

        List<EditText> mEditList = new ArrayList<>();
        mEditList.add(viewBind.passwordEdit);
        mEditList.add(viewBind.confirmPasswordEdit);
        mEditList.add(viewBind.imageTv);
        mEditList.add(viewBind.authCodeEdit);
        presenter.setBound(mEditList);
        viewBind.setPwdBtn.setOnClickListener(this);
        viewBind.sendAgainBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.set_pwd_btn:
                break;
            case R.id.send_again_btn:
                break;
        }
    }
}

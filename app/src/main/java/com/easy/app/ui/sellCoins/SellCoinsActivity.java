package com.easy.app.ui.sellCoins;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Balance;
import com.easy.app.bean.MarketHall;
import com.easy.app.databinding.SellCoinsBinding;
import com.easy.app.helper.MarketHelper;
import com.easy.app.ui.dialog.KeyBoad;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.loadimage.EasyLoadImage;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

import java.math.BigDecimal;

@ActivityInject
@Route(path = RouterManager.SELL_COINS, name = "卖币页面")
public class SellCoinsActivity extends BaseActivity<SellCoinsPresenter, SellCoinsBinding> implements SellCoinsView, View.OnClickListener, TextWatcher, View.OnFocusChangeListener {


    @Autowired(name = "market")
    String market;

    MarketHall marketHallInfo;

    KeyBoad keyBoad;

    private String number;


    private boolean isUiCreate = false;


    @Override
    public int getLayoutId() {
        return R.layout.sell_coins;
    }


    @Override
    public void initView() {

        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setTitleText(getString(R.string.sell_money));
        }

        marketHallInfo = JSON.parseObject(market, MarketHall.class);
        if (marketHallInfo == null) {
            finish();
            return;
        }
        EasyLoadImage.loadImage2(App.getInst(), R.drawable.app_logo, marketHallInfo.getImage(), viewBind.ivAvatar);
        viewBind.acTvUserName.setText(marketHallInfo.getDisplay());
        viewBind.acTvFee.setText(getString(R.string.fee, getString(R.string.default_amount)));
        viewBind.acTvAvailable.setText(getString(R.string.available_sh, getString(R.string.default_amount)));
        ButtonColorChange.colorChange(this, viewBind.sellBtn);
        viewBind.acEvSellNum.setInputType(InputType.TYPE_CLASS_NUMBER);// 允许输入数字与小数点
        keyBoad = new KeyBoad(SellCoinsActivity.this, getWindow().getDecorView(), viewBind.acEvSellNum);
        keyBoad.setDotEnabled(false);
        viewBind.acEvSellNum.setOnClickListener(this);
        viewBind.acEvSellNum.addTextChangedListener(this);
        viewBind.acEvSellNum.setOnFocusChangeListener(this);
        viewBind.acIvBusiness.setOnClickListener(this);
        viewBind.acEvSellAll.setOnClickListener(this);
        viewBind.clChooseBank.setOnClickListener(this);
        viewBind.sellBtn.setOnClickListener(this);
        presenter.requestBalance();

    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.acEvSellNum:
                    if (keyBoad != null) {
                        keyBoad.show();
                    }
                    break;
                case R.id.acIvBusiness:
                    if (marketHallInfo != null && EmptyUtils.isNotEmpty(marketHallInfo.getUsid())) {
                        MarketHelper.goConversation(this, marketHallInfo.getUsid());
                    }
                    break;
                case R.id.acEvSellAll:
                    break;
                case R.id.clChooseBank:
                    RouterManager.goBankCardManageActivity(1);
                    break;
                case R.id.sell_btn:
                    number = viewBind.acEvSellNum.getText().toString().trim();
                    if (number.endsWith(SellShDialogFragment.POINT)) {
                        number = number.substring(0, number.length()-1);
                    }
                    double mMoney = 0;
                    try {
                        mMoney = Double.parseDouble(number);
                    } catch (NumberFormatException e) {
                        ToastUtils.showShort(getString(R.string.transfer_input_money));
                    }

                    if (TextUtils.isEmpty(number) || mMoney <= 0) {
                        ToastUtils.showShort(getString(R.string.transfer_input_money));
                        return;
                    }


                    break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String text = s.toString();
        if (EmptyUtils.isNotEmpty(text)) {
            if (text.contains(SellShDialogFragment.POINT)) {

            }
            if (text.startsWith(SellShDialogFragment.POINT)) {
                viewBind.acEvSellNum.setText("0" + text);
            } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                viewBind.acEvSellNum.setText(text.substring(1));
            } else {
                if (text.endsWith(SellShDialogFragment.POINT)) {
                    text = text.substring(0, text.length()-1);
                }
                BigDecimal a1 = new BigDecimal(Double.parseDouble(text));
                BigDecimal b1 = new BigDecimal(Double.parseDouble(marketHallInfo.getRate()));
                BigDecimal result = a1.multiply(b1);// 相乘结果
                viewBind.acTvFee.setText(getString(R.string.fee, Money.getMoneyFormat(result.toString())));
            }
        } else {
            viewBind.acTvFee.setText(getString(R.string.fee, getString(R.string.default_amount)));
        }


    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (keyBoad != null && isUiCreate) {
            keyBoad.refreshKeyboardOutSideTouchable(!hasFocus);
        } else if (isUiCreate) {
            keyBoad.show();
        }
        if (hasFocus) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(viewBind.acEvSellNum.getWindowToken(), 0);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        isUiCreate = true;
    }

    @Override
    public void balanceCallback(Response<Balance> response) {

        if (response.isSuccess()) {
            Balance balance = response.getResultObj();
            if (balance != null) {
                viewBind.acTvAvailable.setText(getString(R.string.available_sh, Money.getMoneyFormat(balance.getSh())));
            }

        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }
}

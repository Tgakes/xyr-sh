package com.easy.app.ui.dialog.sellSh;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import com.easy.app.R;
import com.easy.app.base.App;

/**
 * 短信验证码倒计时
 */
public class DismissCountDownTimer extends CountDownTimer {
    AppCompatTextView view;//显示倒计时的控件

    SellShDialogFragment sellShDialogFragment;

    /**
     *
     */
    public DismissCountDownTimer(AppCompatTextView view, SellShDialogFragment sellShDialogFragment) {
        super(60000, 1000);
        this.view = view;
        this.sellShDialogFragment = sellShDialogFragment;
        start();
    }

    @Override
    public void onTick(long millisUntilFinished) {
        //防止计时过程中重复点击
//       view.setText(millisUntilFinished / 1000 + "s后重新获取");
        this.view.setText(App.getInst().getString(R.string.auto_cancel, millisUntilFinished / 1000 + ""));
    }


    @Override
    public void onFinish() {
        //重新给Button设置文字
        //view.setText("重新获取");
        //设置可点击
        if (sellShDialogFragment != null) {
            sellShDialogFragment.dismissAllowingStateLoss();
        }
    }


}

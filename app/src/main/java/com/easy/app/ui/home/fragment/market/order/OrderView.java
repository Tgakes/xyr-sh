package com.easy.app.ui.home.fragment.market.order;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

import java.util.List;

public interface OrderView extends BaseView {

    void orderListCallback(Response<List> response);

}

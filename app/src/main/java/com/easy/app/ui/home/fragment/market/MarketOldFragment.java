package com.easy.app.ui.home.fragment.market;


import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.databinding.FmMarketOldBinding;
import com.easy.app.ui.home.fragment.market.history.HistoryFragment;
import com.easy.app.ui.home.fragment.market.hall.HallFragment;
import com.easy.app.ui.home.fragment.market.order.OrderFragment;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.utils.DimensUtils;
import com.easy.widget.SkinTitleView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link MarketOldFragment} subclass.
 * Use the {@link MarketOldFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@Deprecated
@FragmentInject
public class MarketOldFragment extends BaseFragment<MarketPresenter, FmMarketOldBinding> implements MarketView {





    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarketOldFragment newInstance() {
        MarketOldFragment fragment = new MarketOldFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_market_old;
    }

    @Override
    public void initView(View view) {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.app_name));
            skinTitleView.setBackground(R.color.normal_bg);
//            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.drawable.chat_more, App.getInst()));
            skinTitleView.hideBackBtn();
        }

        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewBind.clTopLayout.getLayoutParams();
        params.height = statusHeight + DimensUtils.dp2px(App.getInst(), 55f);
        viewBind.clTopLayout.setLayoutParams(params);


//        int width = DimensUtils.getScreenWidth(App.getInst());
//        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewBind.rpvPage.getLayoutParams();
//        params.height = width * 383 / 900 - statusHeight;
//        viewBind.rpvPage.setLayoutParams(params);

       /* RotationAdapter rotationAdapter = new RotationAdapter(getActivity());
        viewBind.rpvPage.setAdapter(rotationAdapter);
        viewBind.rpvPage.setHintView(new ColorPointHintView(App.getInst(), StreamUtils.getInstance().resourceToColor(R.color.color_FF3600, App.getInst()), StreamUtils.getInstance().resourceToColor(R.color.white_tran, App.getInst())));
        List<RotationModule> rotationModuleList = new ArrayList<>();
        rotationModuleList.add(new RotationModule(R.drawable.lunbo1));
        rotationModuleList.add(new RotationModule(R.drawable.lunbo2));
        rotationAdapter.setData(rotationModuleList);*/


        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(HallFragment.newInstance());
        fragmentList.add(OrderFragment.newInstance());
        fragmentList.add(HistoryFragment.newInstance());

        TitleViewPagerAdapter adapter = new TitleViewPagerAdapter(getChildFragmentManager(), fragmentList, new String[]{getString(R.string.market_tab_1), getString(R.string.market_tab_2), getString(R.string.market_tab_3)});
        viewBind.mViewPager.setAdapter(adapter);
        viewBind.tabLayout.setupWithViewPager(viewBind.mViewPager);

        int count = fragmentList.size();
        viewBind.mViewPager.setOffscreenPageLimit(count);
        presenter.setTabWidth(viewBind.tabLayout, DimensUtils.dp2px(App.getInst(), 7f));
        for (int i = 0; i < count; i++) {
            TabLayout.Tab tab = viewBind.tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(presenter.getTabView(getActivity(), adapter.getPageTitle(i).toString(), i == 0 ? R.drawable.tab_market_normal_bg : 0));
            }
        }

       /* viewBind.tabLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewBind.tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this::onGlobalLayout);

            }
        });*/

    }









}

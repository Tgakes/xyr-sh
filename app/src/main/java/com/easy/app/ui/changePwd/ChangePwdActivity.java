package com.easy.app.ui.changePwd;

import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.ChangePwdBinding;
import com.easy.app.databinding.EmptyBinding;
import com.easy.app.helper.PasswordHelper;
import com.easy.app.ui.empty.EmptyPresenter;
import com.easy.app.ui.empty.EmptyView;
import com.easy.app.util.ButtonColorChange;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.StringUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

@ActivityInject
@Route(path = RouterManager.CHANGE_PWD, name = "修改密码")
public class ChangePwdActivity extends BaseActivity<ChangePwdPresenter, ChangePwdBinding> implements ChangePwdView {



    @Override
    public int getLayoutId() {
        return R.layout.change_pwd;
    }

    @Override
    public void initView() {
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.change_password));
        }
        ButtonColorChange.colorChange(this, viewBind.changeBtn);
//        viewBind.phoneNumberEdit.setText(presenter.accountsDao.get().getPhoneNumber());
        PasswordHelper.bindPasswordEye(viewBind.oldPasswordEdit, viewBind.tbEyeOld);
        PasswordHelper.bindPasswordEye(viewBind.passwordEdit, viewBind.tbEye);
        PasswordHelper.bindPasswordEye(viewBind.confirmPasswordEdit, viewBind.tbEyeConfirm);

        viewBind.changeBtn.setOnClickListener(v -> {
            if (ViewUtils.canClick() && configPassword()) {
                showLoading();
                String authCode = "";
                if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                    authCode = viewBind.authCodeEdit.getText().toString();
                }
                presenter.requestChangePwd(viewBind.oldPasswordEdit.getText().toString().trim(), viewBind.passwordEdit.getText().toString().trim(), authCode);
            }
        });
    }


    /**
     * 确认两次输入的密码是否一致
     */
    private boolean configPassword() {

        String oldPassword = viewBind.oldPasswordEdit.getText().toString().trim();
        if (EmptyUtils.isEmpty(oldPassword)) {
            ToastUtils.showShort(viewBind.oldPasswordEdit.getHint().toString());
            return false;
        }

        String password = viewBind.passwordEdit.getText().toString().trim();
        String confirmPassword = viewBind.confirmPasswordEdit.getText().toString().trim();
        if (EmptyUtils.isEmpty(password) || password.length() < 6) {
            viewBind.passwordEdit.requestFocus();
            viewBind.passwordEdit.setError(StringUtils.editTextHtmlErrorTip(this, R.string.password_empty_error));
            return false;
        }
        if (EmptyUtils.isEmpty(confirmPassword) || confirmPassword.length() < 6) {
            viewBind.confirmPasswordEdit.requestFocus();
            viewBind.confirmPasswordEdit.setError(StringUtils.editTextHtmlErrorTip(this, R.string.confirm_password_empty_error));
            return false;
        }
        if (confirmPassword.equals(password)) {
            return true;
        } else {
            viewBind.confirmPasswordEdit.requestFocus();
            viewBind.confirmPasswordEdit.setError(StringUtils.editTextHtmlErrorTip(this, R.string.password_confirm_password_not_match));
            return false;
        }
    }


    @Override
    public void changePwdCallback(Response<String> response) {

        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            finish();
        }else {
            if (Response.NEED_MSG_STATUS.equals(response.getStatus())) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
                viewBind.vLine.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.easy.app.ui.home.fragment.market.order;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.OrderList;
import com.easy.app.helper.MarketHelper;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.loadimage.EasyLoadImage;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ViewUtils;

import java.math.BigDecimal;
import java.util.List;

public class OrderAdapter extends BaseQuickAdapter<OrderList, BaseViewHolder> {

    Fragment fragment;

    public OrderAdapter(Fragment fragment, int layoutResId, @Nullable List<OrderList> data) {
        super(layoutResId, data);
        this.fragment = fragment;

    }


    @Override
    protected void convert(@NonNull BaseViewHolder helper, OrderList item) {

//        AppCompatTextView exchangeBtn = helper.getView(R.id.exchange_btn);
//        exchangeBtn.setOnClickListener(v -> {
//            if (ViewUtils.canClick()) {
//                SellShDialogFragment.newInstance(JSON.toJSONString(item), fragment.getActivity().getSupportFragmentManager());
//            }
//        });
//        helper.getView(R.id.acIvBusiness).setOnClickListener(v -> {
//            if (ViewUtils.canClick() && EmptyUtils.isNotEmpty(item.getUsid())) {
//                MarketHelper.goConversation(fragment.getActivity(), item.getUsid());
//            }
//        });
//        EasyLoadImage.loadImage2(App.getInst(), R.drawable.app_logo, item.getImage(), helper.getView(R.id.ivAvatar));
//        helper.setText(R.id.acTvUserName, item.getDisplay());
//        helper.setText(R.id.acTvAcquisitionAmount, item.getCash());
//        helper.setText(R.id.acTvRate, item.getRate());
//        helper.setText(R.id.acTvDealDone, item.getClinch());
//        helper.setText(R.id.acTvAcquisitionAmountRate, new BigDecimal(Double.parseDouble(item.getClinch())).divide(new BigDecimal(Double.parseDouble(item.getCash())), 0, BigDecimal.ROUND_HALF_EVEN).toString() + "%");



    }


}

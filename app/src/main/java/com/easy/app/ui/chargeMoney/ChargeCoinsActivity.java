package com.easy.app.ui.chargeMoney;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.ChargeCoinsAddress;
import com.easy.app.databinding.ChargeCoinsBinding;
import com.easy.app.helper.UploadHelp;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.ui.register.RegisterActivity;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.loadimage.EasyLoadImage;
import com.easy.net.beans.Response;
import com.easy.qrcode.CodeUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.FileUtils;
import com.easy.utils.PhotoUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.utils.base.FileConstant;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;
import com.lqr.imagepicker.ImagePicker;
import com.lqr.imagepicker.bean.ImageItem;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Logger;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

@ActivityInject
@Route(path = RouterManager.CHARGE_MONEY, name = "充币页面")
public class ChargeCoinsActivity extends BaseActivity<ChargeCoinsPresenter, ChargeCoinsBinding> implements ChargeCoinsView, View.OnClickListener, TextWatcher {


    @Override
    public int getLayoutId() {
        return R.layout.charge_coins;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.charge_money));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }


        ButtonColorChange.colorChange(this, viewBind.chargeMoneyCompleteBtn);


        viewBind.acTvQrCode.setOnClickListener(this);
        viewBind.acTvCopy.setOnClickListener(this);
        viewBind.chargeMoneyCompleteBtn.setOnClickListener(this);
        viewBind.acTvUpload.setOnClickListener(this);
        viewBind.acEvMoney.addTextChangedListener(this);
        showLoading();
        presenter.requestAddress();
    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.acTvUpload://上传凭证
                    presenter.requestReadWritePermission(getRxPermissions());
                    break;
                case R.id.charge_money_complete_btn://充值完成
                    String money = viewBind.acEvMoney.getText().toString();
                    if (money.endsWith(SellShDialogFragment.POINT)) {
                        money = money.substring(0, money.length() - 1);
                    }
                    double mMoney = 0;
                    try {
                        mMoney = Double.parseDouble(money);
                    } catch (NumberFormatException e) {
                        ToastUtils.showShort(getString(R.string.need_input_money));
                        return;
                    }

                    if (TextUtils.isEmpty(money) || mMoney <= 0) {
                        ToastUtils.showShort(getString(R.string.need_input_money));
                        return;
                    }

                    ChargeCoinsAddress chargeCoinsAddress = (ChargeCoinsAddress) viewBind.chargeMoneyCompleteBtn.getTag();
                    if (chargeCoinsAddress == null) {
                        ToastUtils.showShort(getString(R.string.please_first) + getString(R.string.recharge));
                        return;
                    }

                    String voucher = (String) viewBind.acTvUpload.getTag(R.id.key_avatar);
                    if (EmptyUtils.isEmpty(voucher)) {
                        ToastUtils.showShort(getString(R.string.please_first) + getString(R.string.upload_voucher));
                        return;
                    }
                    presenter.requestChargeComplete(chargeCoinsAddress.getId(), money, voucher);
                    break;
                case R.id.acTvQrCode:
                    Bitmap qrBitmap = (Bitmap) v.getTag();
                    if (qrBitmap != null) {
                        FileUtils.saveImageToGallery2(this, qrBitmap, getString(R.string.tip_saved_qr_code));
                    }
                    break;
                case R.id.acTvCopy://复制
                    String content = (String) v.getTag();
                    if (!EmptyUtils.isEmpty(content)) {
                        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clipData = ClipData.newPlainText("text", content);
                        clipboardManager.setPrimaryClip(clipData);
                        ToastUtils.showShort(getString(R.string.copy_success));
                    }
                    break;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RegisterActivity.REQUEST_CODE_PICK_IMAGE:
                    ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    if (images == null || images.isEmpty()) {
                        return;
                    }

                    UploadHelp.uploadDeal(images.get(0).path, (status, file) -> {
                        if (status == 0) {
                            showLoading();
                        } else if (status == -1) {
                            hideLoading();
                        } else if (status == 1) {
                            presenter.requestUploadFile(ChargeCoinsActivity.this, file);
                        }
                    });
                    break;

            }

        }

    }

    @Override
    public void uploadCallback(Response response) {
        hideLoading();
        if (response.isSuccess() && EmptyUtils.isNotEmpty(response.getUrl())) {
            viewBind.acTvUpload.setTag(R.id.key_avatar, response.getUrl());
            viewBind.acTvUpload.setScaleType(ImageView.ScaleType.CENTER);
            EasyLoadImage.loadImage2(App.getInst(), R.color.transparent, response.getUrl(), viewBind.acTvUpload);
        }
    }

    @Override
    public void chargeCoinAddressCallback(Response<ChargeCoinsAddress> response) {

        hideLoading();
        if (response.isSuccess() && response.getResultObj() != null) {
            String content = response.getResultObj().getDizhi();
            Bitmap qrBitmap = CodeUtils.createQRCode(content, 400, null);
            viewBind.ivQrCode.setTag(qrBitmap);
            viewBind.ivQrCode.setImageBitmap(qrBitmap);
            viewBind.acTvChargeAddress.setText(content);
            viewBind.acTvCopy.setTag(content);
            viewBind.chargeMoneyCompleteBtn.setTag(response.getResultObj());
        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void chargeCompleteCallback(Response response) {
        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            finish();
        }
    }

    @Override
    public void permissionReadWriteCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {
            ImagePicker.picker().pick(this, RegisterActivity.REQUEST_CODE_PICK_IMAGE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if (!ViewUtils.inputSinglePoint(viewBind.acEvMoney, s)) {
            String text = s.toString();
            if (EmptyUtils.isNotEmpty(text)) {
               /* int length = text.length();
                if (length - 1 - text.indexOf(POINT) > 2) {
                    text = text.substring(0, s.toString().indexOf(POINT) + 3);
                    viewBind.acEvSellAmount.setText(text);
                    viewBind.acEvSellAmount.setSelection(text.length());
                } else {

                }*/

                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.acEvMoney.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.acEvMoney.setText(text.substring(1));
                }

            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

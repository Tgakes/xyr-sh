package com.easy.app.ui.setPayPwd;

import android.content.res.ColorStateList;
import android.view.View;

import androidx.core.view.ViewCompat;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.SetPayPwdBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.even.ChooseFileEvent;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.jungly.gridpasswordview.GridPasswordView;

@ActivityInject
@Route(path = RouterManager.SET_PAY_PWD, name = "设置支付密码")
public class SetPayPwdActivity extends BaseActivity<SetPayPwdPresenter, SetPayPwdBinding> implements SetPayPwdView, GridPasswordView.OnPasswordChangedListener, View.OnClickListener {


    //0:设置密码 1:修改密码 2:第一次输入新密码 3:确认密码
    @Autowired(name = "pageFlag")
    int pageFlag;

    @Autowired(name = "oldPwd")
    String oldPwd;

    @Autowired(name = "newPwd")
    String newPwd;

    String inputPwd;


    @Override
    public void initView() {
        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.pay_pwd));
        }

        viewBind.tvPayDes.setText(getString(pageFlag == 0 ? R.string.input_6_pay_pwd : pageFlag == 1 ? R.string.input_6_old_pay_pwd : pageFlag == 2 ? R.string.input_6_new_pay_pwd : R.string.again_input_6_pay_pwd));
        viewBind.gpvPayPassword.setOnPasswordChangedListener(this);
        viewBind.btnComplete.setOnClickListener(this);
        viewBind.btnComplete.setText(getString(pageFlag == 3 ? (EmptyUtils.isEmpty(oldPwd) ? R.string.set_pay_pwd : R.string.confirm_modify) : R.string.next_step));

        ViewCompat.setBackgroundTintList(viewBind.btnComplete, ColorStateList.valueOf(SkinUtils.getSkin(this).getAccentColor()));
        if (pageFlag != 4) {
            LiveEventBus.get("setPayPwd", String.class).observe(this, this::setPayPwdSuccess);
        }

    }

    private void setPayPwdSuccess(String s) {
        if (!isFinishing()) {
            finish();
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.set_pay_pwd;
    }

    @Override
    public void onTextChanged(String psw) {
        inputPwd = psw;
        if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
            String savePwd = (String) viewBind.authCodeLl.getTag();
            if (!inputPwd.equals(savePwd)) {
                viewBind.authCodeLl.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void onInputFinish(String psw) {
        inputPwd = psw;
        if (viewBind.authCodeLl.getVisibility() == View.GONE) {
            String savePwd = (String) viewBind.authCodeLl.getTag();
            if (inputPwd.equals(savePwd)) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (EmptyUtils.isEmpty(inputPwd) || inputPwd.length() != 6) {
            ToastUtils.showShort(getString(R.string.input_6_pay_pwd));
            return;
        }
        if (pageFlag == 0) {
            RouterManager.goSetPayPwd(4, inputPwd, "");
        } else if (pageFlag == 1) {
            RouterManager.goSetPayPwd(2, "", inputPwd);
        } else if (pageFlag == 2) {
            RouterManager.goSetPayPwd(3, inputPwd, oldPwd);
        } else {//开始执行修改密码，设置密码
            if (!inputPwd.equals(newPwd)) {
                ToastUtils.showShort(getString(R.string.tip_change_pay_password_input_incorrect));
                return;
            }
            viewBind.authCodeLl.setTag(inputPwd);

            showLoading();
            String authCode = "";
            if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                authCode = viewBind.authCodeEdit.getText().toString();
            }
            presenter.requestSetPayPwd(inputPwd, oldPwd, authCode);
        }
    }

    @Override
    public void setPayPwdCallback(Response<String> response) {

        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            presenter.accountsDao.get().setUpdatePayPwd();
            LiveEventBus.get("setPayPwd", String.class).post("");
            finish();
        } else {
            if (Response.NEED_MSG_STATUS.equals(response.getStatus())) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
            }
        }
    }
}

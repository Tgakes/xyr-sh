package com.easy.app.ui.dialog.payPwd;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;

import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Pay;
import com.easy.app.bean.TransferReq;
import com.easy.app.databinding.PayPwdDialogBinding;
import com.easy.app.helper.RandomHelper;
import com.easy.app.ui.mvp.transfer.TransferPresenter;
import com.easy.app.ui.mvp.transfer.TransferView;
import com.easy.app.util.AnimationUtils;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.CenterDialogFragment;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.widget.SkinUtils;
import com.easy.widget.WalletView;
import com.jungly.gridpasswordview.GridPasswordView;

import javax.inject.Inject;

@FragmentInject
public class PayPwdDialogFragment extends CenterDialogFragment<PayPwdDialogPresenter, PayPwdDialogBinding> implements PayPwdDialogView, View.OnClickListener, GridPasswordView.OnPasswordChangedListener {

    PayPwdListener listener;

    public static final String PAY = "pay";


    Animation rotateAnimation;//得到动画

    String pwd;


    Pay pay;


    public static PayPwdDialogFragment newInstance(Pay pay, FragmentManager fragmentManager) {
        PayPwdDialogFragment fragment = new PayPwdDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PAY, pay);
        fragment.setArguments(bundle);
        fragment.show(fragmentManager);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof PayPwdListener) {
            listener = (PayPwdListener) context;
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.pay_pwd_dialog;
    }


    @Override
    public void initView(View view) {
        //todo 先判断是否设置过支付密码
        if (!presenter.accountsDao.get().isSetPwd()) {
            ToastUtils.showShort(getString(R.string.tip_no_pay_password));
            RouterManager.goSetPayPwd(0, "", "");
            dismissAllowingStateLoss();
            return;
        }

        pwd = "";
//      transferPresenter.requestTransfer(new TransferReq(money,words,receipt.getUserId(),receipt.getMoney().endsWith(WalletView.USDT_NAME) ? WalletView.USDT_NAME : WalletView.SH_NAME,2,pwd));
        rotateAnimation = AnimationUtils.getAnimation();
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

        Bundle bundle = getArguments();
        pay = (Pay) bundle.getSerializable(PAY);
        viewBind.tvAction.setText(pay.getAction());

        if (pay.getPayStyle() == Pay.TRANSFER) {//转账
            TransferReq transferReq = JSON.parseObject(pay.getData(), TransferReq.class);
            viewBind.tvMoney.setText(transferReq.getMoney());
            viewBind.tvLabelYuan.setText(transferReq.getCurrency() == 2? WalletView.USDT_NAME : WalletView.SH_NAME);
            viewBind.llConfirmPayLayout.setTag(transferReq);
        }

        ViewCompat.setBackgroundTintList(viewBind.llConfirmPayLayout, ColorStateList.valueOf(SkinUtils.getSkin(getActivity()).getAccentColor()));

        viewBind.gpvPayPassword.setOnPasswordChangedListener(this);
        viewBind.ivClose.setOnClickListener(this);
        viewBind.llConfirmPayLayout.setOnClickListener(this);

        viewBind.gpvPayPassword.postDelayed(() -> {
            viewBind.gpvPayPassword.setFocusable(true);
            viewBind.gpvPayPassword.setFocusableInTouchMode(true);
            viewBind.gpvPayPassword.requestFocus();
            viewBind.gpvPayPassword.performClick();
        }, 50);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_confirm_pay_layout:

                if (EmptyUtils.isEmpty(pwd)) {
                    ToastUtils.showShort(getString(R.string.title_input_pay_password));
                    return;
                }

                if (pwd.length() < 6) {
                    ToastUtils.showShort(getString(R.string.intact_pay_password));
                    return;
                }

                if (pay.getPayStyle() == Pay.TRANSFER) {

                    String code = "";
                    if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                        code = viewBind.authCodeEdit.getText().toString();
                        if (EmptyUtils.isEmpty(code)) {
                            ToastUtils.showShort(getString(R.string.input_message_code));
                            return;
                        }
                    }
                    String orderId = (String) viewBind.gpvPayPassword.getTag();
                    if (EmptyUtils.isEmpty(orderId)) {
                        orderId = RandomHelper.getOrderId();
                        viewBind.gpvPayPassword.setTag(orderId);
                    }

                    TransferReq transferReq = (TransferReq) viewBind.llConfirmPayLayout.getTag();
                    transferReq.setOrderId(orderId);
                    transferReq.setVrCode(code);
                    transferReq.setAuthentication(pwd);
                    setLoadingStart();
                    presenter.requestTransfer(transferReq);
                }


                break;
            case R.id.ivClose:
                PayPwdDialogFragment.this.dismiss();
                break;
        }

    }


    //动画开始
    private void setLoadingStart() {
        viewBind.llConfirmPayLayout.setClickable(false);
        viewBind.ivConfirmPayLoading.setVisibility(View.VISIBLE);
        viewBind.ivConfirmPayLoading.startAnimation(rotateAnimation);
        rotateAnimation.start();
        viewBind.tvConfirmPayText.setText(getString(R.string.paying));

    }

    //动画结束
    private void setLoadingEnd() {
        viewBind.llConfirmPayLayout.setClickable(true);
        rotateAnimation.cancel();
        viewBind.ivConfirmPayLoading.clearAnimation();
        viewBind.ivConfirmPayLoading.setVisibility(View.GONE);
        viewBind.tvConfirmPayText.setText(getString(R.string.confirm_pay));

    }


    @Override
    public void onTextChanged(String psw) {
        pwd = psw;
    }

    @Override
    public void onInputFinish(String psw) {
        pwd = psw;
        /*if (listener != null) {
            listener.onPayComplete(psw);
            PayPwdDialogFragment.this.dismiss();
        }*/
    }

    @Override
    public void transferCallback(Response<String> response) {

        setLoadingEnd();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            PayPwdDialogFragment.this.dismiss();
            if (listener != null) {
                listener.onPaySuccess((String) viewBind.gpvPayPassword.getTag());
            } else {
                getActivity().finish();
            }
        } else {
            if (Response.NEED_MSG_STATUS.equals(response.getStatus())) {
                viewBind.authCodeLl.setVisibility(View.VISIBLE);
            } else {
                PayPwdDialogFragment.this.dismiss();
                if (listener != null) {
                    listener.onPayFail();
                } else {
                    getActivity().finish();
                }
            }
        }
    }


    public interface PayPwdListener {

        int PAY_SUCCESS_STATUS = 1;

        int PAY_FAIL_STATUS = 0;

        void onPaySuccess(String orderId);

        void onPayFail();
    }


}

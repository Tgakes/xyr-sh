package com.easy.app.ui.dialog.sellSh;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Balance;
import com.easy.app.bean.MarketHall;
import com.easy.app.bean.PayGoodsCardManage;
import com.easy.app.databinding.DialogSellShBinding;
import com.easy.app.util.Money;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BottomDialogFragment;
import com.easy.framework.base.common.CommonActivity;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.math.BigDecimal;

@FragmentInject
public class SellShDialogFragment extends BottomDialogFragment<SellShDialogPresenter, DialogSellShBinding> implements SellShDialogView, View.OnClickListener, TextWatcher {

    public static final String KEY = "key";

    public final static String POINT = ".";

    MarketHall marketHallInfo;

    public static SellShDialogFragment newInstance(String market, FragmentManager fragmentManager) {
        SellShDialogFragment fragment = new SellShDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, market);
        fragment.setArguments(bundle);
        fragment.show(fragmentManager);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }


    @Override
    public int getLayoutId() {
        return R.layout.dialog_sell_sh;
    }


    @Override
    public void initView(View view) {

        Bundle bundle = getArguments();
        LiveEventBus.get("chooseBank", PayGoodsCardManage.class).observe(this, this::chooseBank);
        marketHallInfo = JSON.parseObject(bundle.getString(KEY), MarketHall.class);
        if (marketHallInfo == null) {
            dismiss();
            return;
        }

        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        viewBind.acTvAutoCancel.setText(getString(R.string.auto_cancel, "60"));
        viewBind.acTvAvailable.setText(getString(R.string.available_sh, getString(R.string.default_amount)));
        viewBind.avTvSinglePrice.setText(Money.getMoneyFormat(marketHallInfo.getRate()));
        viewBind.acTvHelp.setOnClickListener(this);
        viewBind.acTvSellAll.setOnClickListener(this);
        viewBind.clChooseBank.setOnClickListener(this);
        viewBind.acTvAutoCancel.setOnClickListener(this);
        viewBind.acTvPlaceOrder.setOnClickListener(this);
        viewBind.acEvSellAmount.addTextChangedListener(this);
        presenter.requestBalance();
    }

    private void chooseBank(PayGoodsCardManage payGoodsCardManage) {

        if (EmptyUtils.isNotEmpty(payGoodsCardManage.getCarnumber()) && payGoodsCardManage.getCarnumber().length() >= 4) {
            viewBind.acTvReceiptBank.setText(payGoodsCardManage.getBank());
            viewBind.acTvBank.setText("(" + payGoodsCardManage.getCarnumber().substring(payGoodsCardManage.getCarnumber().length() - 4) + ")");
        }
        viewBind.acTvBank.setTag(payGoodsCardManage);

    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.clChooseBank:
                    RouterManager.goBankCardManageActivity(1);
                    break;
                case R.id.acTvAutoCancel:
                    SellShDialogFragment.this.dismiss();
                    break;
                case R.id.acTvSellAll:
                    String amount = (String) viewBind.acTvSellAll.getTag();
                    if (EmptyUtils.isNotEmpty(amount) && Double.parseDouble(amount) > 0) {
                        viewBind.acEvSellAmount.setText(amount);
                        viewBind.acEvSellAmount.setSelection(viewBind.acEvSellAmount.getText().toString().length());
                    }
                    break;
                case R.id.acTvHelp:

                    break;
                case R.id.acTvPlaceOrder:
                    String amountAll = viewBind.acEvSellAmount.getText().toString().trim();
                    if (amountAll.endsWith(POINT)) {
                        amountAll = amountAll.substring(0, amountAll.length() - 1);
                    }
                    double mMoney = 0;
                    try {
                        mMoney = Double.parseDouble(amountAll);
                    } catch (NumberFormatException e) {
                        ToastUtils.showShort(getString(R.string.transfer_input_money));
                    }

                    if (TextUtils.isEmpty(amountAll) || mMoney <= 0) {
                        ToastUtils.showShort(getString(R.string.transfer_input_money));
                        return;
                    }


                    String balanceAmount = (String) viewBind.acTvSellAll.getTag();
                    if (EmptyUtils.isNotEmpty(balanceAmount) && new BigDecimal(Double.parseDouble(balanceAmount)).compareTo(new BigDecimal(Double.parseDouble(amountAll))) < 0) {
                        ToastUtils.showShort(getString(R.string.title_balance_not_enough));
                        return;
                    }

                    PayGoodsCardManage payGoodsCardManage = (PayGoodsCardManage) viewBind.acTvBank.getTag();
                    if (payGoodsCardManage == null) {
                        ToastUtils.showShort(getString(R.string.please_choose) + getString(R.string.receipt_bank));
                        return;
                    }

                    ((CommonActivity) getActivity()).showLoading();
                    presenter.requestPlaceOrder(payGoodsCardManage.getId(), marketHallInfo.getUsid(), marketHallInfo.getCdid(), amountAll, marketHallInfo.getRate(), (String) viewBind.acTvReceiptAmount.getTag());
                    break;
            }
        }

    }

    @Override
    public void balanceCallback(Response<Balance> response) {
        new DismissCountDownTimer(viewBind.acTvAutoCancel, this);
        if (response.isSuccess()) {
            Balance balance = response.getResultObj();
            if (balance != null) {
                viewBind.acTvAvailable.setText(getString(R.string.available_sh, Money.getMoneyFormat(balance.getSh())));
                viewBind.acTvSellAll.setTag(Money.getMoneyFormat(balance.getSh()));
            }
        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void placeOrderCallback(Response response) {
        ((CommonActivity) getActivity()).hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            dismissAllowingStateLoss();
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }


    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (!ViewUtils.inputSinglePoint(viewBind.acEvSellAmount, s)) {
            String text = s.toString();
            if (EmptyUtils.isNotEmpty(text)) {
               /* int length = text.length();
                if (length - 1 - text.indexOf(POINT) > 2) {
                    text = text.substring(0, s.toString().indexOf(POINT) + 3);
                    viewBind.acEvSellAmount.setText(text);
                    viewBind.acEvSellAmount.setSelection(text.length());
                } else {

                }*/

                if (text.startsWith(POINT)) {
                    viewBind.acEvSellAmount.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(POINT) && text.length() > 1) {
                    viewBind.acEvSellAmount.setText(text.substring(1));
                } else {
                    if (text.endsWith(POINT)) {
                        text = text.replace(POINT, "");
                    }
                    BigDecimal a1 = new BigDecimal(Double.parseDouble(text));
                    BigDecimal b1 = new BigDecimal(Double.parseDouble(marketHallInfo.getRate()));
                    BigDecimal result = a1.subtract(a1.multiply(b1));// 相减结果
                    viewBind.acTvReceiptAmount.setText(Money.getMoneyFormat(result.toString()));
                    viewBind.acTvReceiptAmount.setTag(Money.getMoneyFormat(result.toString()));
                }

            } else {
                viewBind.acTvReceiptAmount.setText(getString(R.string.default_amount));
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

package com.easy.app.ui.paymentOrReceipt;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

class MyTabAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments;
    private List<String> mTitleList;
//    private Fragment mCurrentFragment;
    MyTabAdapter(FragmentManager fm, List<Fragment> fragments,List<String> mTitleList) {
        super(fm);
        this.mFragments = fragments;
        this.mTitleList = mTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        if (mFragments != null) {
            return mFragments.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }

    //用于区分具体属于哪个fragment
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
//        mCurrentFragment = (Fragment) object;
        super.setPrimaryItem(container, position, object);
    }

//    public Fragment getCurrentFragment() {
//        return mCurrentFragment;
//    }
}
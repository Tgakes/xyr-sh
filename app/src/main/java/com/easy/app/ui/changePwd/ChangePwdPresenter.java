package com.easy.app.ui.changePwd;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.utils.EmptyUtils;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChangePwdPresenter extends AppPresenter<ChangePwdView> {

    @Inject
    public ChangePwdPresenter() {

    }


    public void requestChangePwd(String oldPwd, String pwd, String vrCode) {
        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "setpasswd");
        parameter.put("oldpw", oldPwd);
        parameter.put("passwd", pwd);
        parameter.put("vrcode", vrCode);
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.changePwdCallback(result),
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.changePwdCallback(response);
                        });
    }


}

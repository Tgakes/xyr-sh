package com.easy.app.ui.myWallet;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppSharePreferences;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Balance;
import com.easy.app.bean.MyWallet;
import com.easy.app.databinding.MyWalletBinding;
import com.easy.app.helper.MarketHelper;
import com.easy.app.ui.dialog.chooseDefaultCoin.ChooseDefaultCoinDialogFragment;
import com.easy.app.ui.dialog.exchange.ExchangeDialogFragment;
import com.easy.app.ui.home.fragment.market.hall.MarketHallAdapter;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.apt.lib.SharePreference;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.ui.RecycleViewDivider;
import com.easy.net.beans.Response;
import com.easy.utils.DimensUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.Md5Utils;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.yanzhenjie.recyclerview.OnItemMenuClickListener;
import com.yanzhenjie.recyclerview.SwipeMenu;
import com.yanzhenjie.recyclerview.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.SwipeMenuItem;

import java.math.BigDecimal;

import cn.wildfirechat.model.UserInfo;

@ActivityInject
@Route(path = RouterManager.MY_WALLET_ACTIVITY, name = "我的钱包")
public class MyWalletActivity extends BaseActivity<MyWalletPresenter, MyWalletBinding> implements MyWalletView, OnRefreshListener, View.OnClickListener, SwipeMenuCreator, OnItemMenuClickListener, ChooseDefaultCoinDialogFragment.ChooseDefaultCoinListener {

    UserInfo userInfo;

    MyWalletAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.my_wallet;
    }

    @Override
    public void initView() {
        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.my_purse));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.mipmap.navigation, App.getInst()));
            skinTitleView.setRightClickListener(v -> {
                RouterManager.goPayCenterActivity();
            });
        }
        userInfo = getIntent().getParcelableExtra("userInfo");

        viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, getString(R.string.default_amount)));

        viewBind.recyclerView.setBackgroundResource(R.color.white);
        viewBind.recyclerView.setSwipeMenuCreator(this);
        viewBind.recyclerView.setOnItemMenuClickListener(this);
        adapter = new MyWalletAdapter(R.layout.adapter_my_wallet, presenter.getWalletData());
        viewBind.recyclerView.setAdapter(adapter);
//        adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        viewBind.recyclerView.addItemDecoration(new RecycleViewDivider(App.getInst(), LinearLayoutManager.VERTICAL, DimensUtils.dp2px(App.getInst(), 15f), StreamUtils.getInstance().resourceToColor(R.color.white, App.getInst())));
        viewBind.recyclerView.setHasFixedSize(true);

        AppSharePreferences appSharePreferences = SharePreference.get(App.getInst(), AppSharePreferences.class);
        initMode(appSharePreferences.getCoinType());

        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(false);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
        viewBind.acTvMore.setOnClickListener(this);
        showLoading(getString(R.string.please_wait));
        presenter.requestBalance();
    }

    private void initMode(int mode) {

        if (mode == ChooseDefaultCoinDialogFragment.ChooseDefaultCoinListener.SH_MODE) {//sh
            viewBind.acTvNickName.setText(WalletView.SH_NAME);
            viewBind.acTvWalletAddress.setText(Md5Utils.md5(WalletView.SH_NAME).toUpperCase());
            viewBind.clFistView.setBackgroundResource(R.drawable.wallet_sh_top_bg);

//            MyWallet myWallet = adapter.getItem(0);
//            if (myWallet != null) {
//                if (!getString(R.string.default_amount).equals(myWallet.getAmount())) {
//                    viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, myWallet.getAmount()));
//                }
//            }
        } else {
            viewBind.acTvNickName.setText(WalletView.USDT_NAME);
            viewBind.acTvWalletAddress.setText(Md5Utils.md5(WalletView.USDT_NAME).toUpperCase());
            viewBind.clFistView.setBackgroundResource(R.drawable.wallet_usdt_top_bg);
//            MyWallet myWallet = adapter.getItem(1);
//            if (myWallet != null) {
//                if (!getString(R.string.default_amount).equals(myWallet.getAmount())) {
//                    viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, myWallet.getAmount()));
//                }
//            }
        }
    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.acTvMore://
                    ChooseDefaultCoinDialogFragment.newInstance(getSupportFragmentManager());
                    break;
                case R.id.tvShExchange://兑换
//                    RouterManager.goExchange();
                    ExchangeDialogFragment.newInstance(getSupportFragmentManager());
                    break;
                case R.id.tvShSellMoney:
                    LiveEventBus.get("market", String.class).post("");
                    finish();
                    break;
                case R.id.tvUsdtRecharge://充币
                    RouterManager.goChargeMoney();
                    break;
                case R.id.tvUsdtWithdraw://提币
                    RouterManager.goWithdrawMoney();
                    break;
            }
        }
    }

    @Override
    public void balanceCallback(Response<Balance> response) {
        hideLoading();
        if (response.isSuccess()) {
            viewBind.swRefresh.finishRefresh(true);
            Balance balance = response.getResultObj();
            MyWallet myWallet = adapter.getItem(0);
            myWallet.setAmount(Money.getMoneyFormat(balance.getSh()));
            myWallet.setAbout("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getSh())).multiply(new BigDecimal(Double.parseDouble(balance.getHtou()))).toString()) + " " + WalletView.USDT_NAME);
            adapter.setData(0, myWallet);

            myWallet = adapter.getItem(1);
            myWallet.setAmount(Money.getMoneyFormat(balance.getUsdt()));
            myWallet.setAbout("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getUsdt())).multiply(new BigDecimal(Double.parseDouble(balance.getUtoh()))).toString()) + " " + WalletView.SH_NAME);
            adapter.setData(1, myWallet);


            // viewBind.tvShBalance.setText(Money.getMoneyFormat(balance.getSh()));
//            viewBind.tvUsdtBalance.setText(Money.getMoneyFormat(balance.getUsdt()));
//            viewBind.tvShBalanceAbout.setText("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getSh())).multiply(new BigDecimal(Double.parseDouble(balance.getHtou()))).toString())+" "+ WalletView.USDT_NAME);
//            viewBind.tvUsdtBalanceAbout.setText("≈" + Money.getMoneyFormat(new BigDecimal(Double.parseDouble(balance.getUsdt())).multiply(new BigDecimal(Double.parseDouble(balance.getUtoh()))).toString())+" "+ WalletView.SH_NAME);
            BigDecimal allAdd = new BigDecimal(Double.parseDouble(balance.getRate())).multiply(new BigDecimal(Double.parseDouble(balance.getUsdt()))).add(new BigDecimal(Double.parseDouble(balance.getSh())));
            viewBind.acTvAllAmount.setText(getString(R.string.amount_unit, Money.getMoneyFormat(allAdd.toString())));


        } else {
            viewBind.swRefresh.finishRefresh();
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        presenter.requestBalance();
    }

    @Override
    public void onItemClick(SwipeMenuBridge menuBridge, int adapterPosition) {
        menuBridge.closeMenu();
        // 左侧还是右侧菜单：
        int direction = menuBridge.getDirection();
        // 菜单在Item中的Position：
        int menuPosition = menuBridge.getPosition();
        if (ViewUtils.canClick()) {
            if (adapterPosition == 0) {

                if (direction == 1) {//兑换
                    ExchangeDialogFragment.newInstance(getSupportFragmentManager());
                } else {//卖币
                    LiveEventBus.get("market", String.class).post("");
                    finish();
                }
            } else {
                if (direction == 1) {//充币
                    RouterManager.goChargeMoney();
                } else {//提币
                    RouterManager.goWithdrawMoney();
                }
            }
        }
//        adapter.getItem()
        Log.i("logs", "menuPosition===" + menuPosition + ",direction===" + direction + ",adapterPosition===" + adapterPosition);
    }

    @Override
    public void onCreateMenu(SwipeMenu leftMenu, SwipeMenu rightMenu, int position) {
        if (position == 0) {
            SwipeMenuItem swipeMenuItem = new SwipeMenuItem(this);
            swipeMenuItem.setBackground(R.drawable.market_hall_sideslip_bg);
            swipeMenuItem.setText(getString(R.string.exchange));
            swipeMenuItem.setImage(R.mipmap.ic_wallet_exchange);
            swipeMenuItem.setTextColorResource(R.color.white);
            swipeMenuItem.setDrawablePadding(DimensUtils.dp2px(App.getInst(), 7f));
            swipeMenuItem.setIconDirection(0);
            swipeMenuItem.setTextSize(15);
            swipeMenuItem.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
            swipeMenuItem.setWidth(DimensUtils.dp2px(App.getInst(), 90f));
            leftMenu.addMenuItem(swipeMenuItem); // 在Item左侧添加一个菜单。


            swipeMenuItem = new SwipeMenuItem(this);
            swipeMenuItem.setBackground(R.color.color_1790EE);
            swipeMenuItem.setText(getString(R.string.sell_money));
            swipeMenuItem.setImage(R.mipmap.ic_wallet_sell_coin);
            swipeMenuItem.setTextColorResource(R.color.white);
            swipeMenuItem.setDrawablePadding(DimensUtils.dp2px(App.getInst(), 7f));
            swipeMenuItem.setIconDirection(0);
            swipeMenuItem.setTextSize(15);
            swipeMenuItem.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
            swipeMenuItem.setWidth(DimensUtils.dp2px(App.getInst(), 90f));
            rightMenu.addMenuItem(swipeMenuItem); // 在Item左侧添加一个菜单。

        } else {

            SwipeMenuItem swipeMenuItem = new SwipeMenuItem(this);
            swipeMenuItem.setBackground(R.drawable.market_hall_sideslip_bg);
            swipeMenuItem.setText(getString(R.string.recharge));
            swipeMenuItem.setImage(R.mipmap.ic_wallet_recharge);
            swipeMenuItem.setTextColorResource(R.color.white);
            swipeMenuItem.setDrawablePadding(DimensUtils.dp2px(App.getInst(), 7f));
            swipeMenuItem.setIconDirection(0);
            swipeMenuItem.setTextSize(15);
            swipeMenuItem.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
            swipeMenuItem.setWidth(DimensUtils.dp2px(App.getInst(), 90f));
            leftMenu.addMenuItem(swipeMenuItem); // 在Item左侧添加一个菜单。


            swipeMenuItem = new SwipeMenuItem(this);
            swipeMenuItem.setBackground(R.color.color_1790EE);
            swipeMenuItem.setText(getString(R.string.withdraw));
            swipeMenuItem.setImage(R.mipmap.ic_wallet_withdrawal);
            swipeMenuItem.setTextColorResource(R.color.white);
            swipeMenuItem.setDrawablePadding(DimensUtils.dp2px(App.getInst(), 7f));
            swipeMenuItem.setIconDirection(0);
            swipeMenuItem.setTextSize(15);
            swipeMenuItem.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
            swipeMenuItem.setWidth(DimensUtils.dp2px(App.getInst(), 90f));
            rightMenu.addMenuItem(swipeMenuItem); // 在Item左侧添加一个菜单。
        }
    }

    @Override
    public void onChooseDefaultCoin(int coin) {
        initMode(coin);
    }
}

package com.easy.app.ui.receiptSetMoney;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.ReceiptSetMoneyBinding;
import com.easy.app.ui.dialog.KeyBoad;
import com.easy.app.ui.dialog.VerifyDialog;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.util.ButtonColorChange;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.utils.ToastUtils;
import com.easy.widget.Constants;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;

@ActivityInject
@Route(path = RouterManager.RECEIPT_SET_MONEY, name = "设置金额")
public class ReceiptSetMoneyActivity extends BaseActivity<ReceiptSetMoneyPresenter, ReceiptSetMoneyBinding> implements ReceiptSetMoneyView {

    KeyBoad keyBoad;

    private boolean isUiCreate = false;
    private String money, words;

    @Autowired(name = "userId")
    String userId;


    @Override
    public int getLayoutId() {
        return R.layout.receipt_set_money;
    }

    @Override
    public void initView() {
        ARouter.getInstance().inject(this);

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.receipt_set_money));
        }
        ButtonColorChange.colorChange(this, viewBind.transferBtn);
        viewBind.transferJeTv.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);// 允许输入数字与小数点
        keyBoad = new KeyBoad(ReceiptSetMoneyActivity.this, getWindow().getDecorView(), viewBind.etTransfer);
        initEvent();
        initKeyBoad();
    }

    private void initEvent() {

        viewBind.tvAccountType.setOnClickListener(view -> {
            WalletView.showWalletView(accountType -> viewBind.tvAccountType.setText(accountType), this);
        });

        viewBind.transferEditDescTv.setOnClickListener(v -> {
            VerifyDialog verifyDialog = new VerifyDialog(ReceiptSetMoneyActivity.this);
            verifyDialog.setVerifyClickListener(getString(R.string.receipt_add_desc), getString(R.string.transfer_desc_max_length_10),
                    words, 10, new VerifyDialog.VerifyClickListener() {
                        @Override
                        public void cancel() {

                        }

                        @Override
                        public void send(String str) {
                            words = str;
                            if (TextUtils.isEmpty(words)) {
                                viewBind.transferDescTv.setText("");
                                viewBind.transferDescTv.setVisibility(View.GONE);
                                viewBind.transferEditDescTv.setText(getString(R.string.receipt_add_desc));
                            } else {
                                viewBind.transferDescTv.setText(str);
                                viewBind.transferDescTv.setVisibility(View.VISIBLE);
                                viewBind.transferEditDescTv.setText(getString(R.string.transfer_modify));
                            }
                            keyBoad.show();
                        }
                    });
            verifyDialog.setOkButton(R.string.sure);
            keyBoad.dismiss();
            Window window = verifyDialog.getWindow();
            if (window != null) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE); // 软键盘弹起
            }
            verifyDialog.show();
        });

//        findViewById(R.id.transfer_btn).setBackgroundColor(SkinUtils.getSkin(this).getAccentColor());


        viewBind.transferBtn.setOnClickListener(v -> {
            money = viewBind.etTransfer.getText().toString().trim();
            if (money.endsWith(SellShDialogFragment.POINT)) {
                money = money.substring(0, money.length() - 1);
            }
            double mMoney = 0;
            try {
                mMoney = Double.parseDouble(money);
            } catch (NumberFormatException e) {
                ToastUtils.showShort(getString(R.string.transfer_input_money));
                return;
            }

            if (TextUtils.isEmpty(money) || mMoney <= 0) {
                ToastUtils.showShort(getString(R.string.transfer_input_money));
                return;
            }



            PreferenceUtils.putString(App.getInst(), Constants.RECEIPT_SETTING_MONEY + userId, money + " " + viewBind.tvAccountType.getText().toString());
            if (!TextUtils.isEmpty(words)) {
                PreferenceUtils.putString(App.getInst(), Constants.RECEIPT_SETTING_DESCRIPTION + userId, words);
            }

            finish();
        });
    }

    private void initKeyBoad() {
        viewBind.etTransfer.setOnFocusChangeListener((View.OnFocusChangeListener) (v, hasFocus) -> {
            if (keyBoad != null && isUiCreate) {
                keyBoad.refreshKeyboardOutSideTouchable(!hasFocus);
            } else if (isUiCreate) {
                keyBoad.show();
            }
            if (hasFocus) {
                InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(viewBind.etTransfer.getWindowToken(), 0);
            }
        });

        viewBind.etTransfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.etTransfer.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.etTransfer.setText(text.substring(1));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        viewBind.etTransfer.setOnClickListener(v -> {
            if (keyBoad != null) {
                keyBoad.show();
            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        isUiCreate = true;
    }

}

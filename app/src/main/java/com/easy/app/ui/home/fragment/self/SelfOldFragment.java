package com.easy.app.ui.home.fragment.self;


import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.FmSelfBinding;
import com.easy.app.databinding.FmSelfOldBinding;
import com.easy.app.ui.home.fragment.message.MessagePresenter;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.loadimage.EasyLoadImage;
import com.easy.store.bean.Accounts;
import com.easy.utils.DimensUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

import java.util.List;

import cn.wildfire.chat.kit.WfcScheme;
import cn.wildfire.chat.kit.qrcode.QRCodeActivity;
import cn.wildfire.chat.kit.user.UserInfoActivity;
import cn.wildfire.chat.kit.user.UserViewModel;
import cn.wildfire.chat.moment.FeedListActivity;
import cn.wildfirechat.model.UserInfo;


/**
 * A simple {@link SelfOldFragment} subclass.
 * Use the {@link SelfOldFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@Deprecated
@FragmentInject
public class SelfOldFragment extends BaseFragment<MessagePresenter, FmSelfOldBinding> implements SelfView, View.OnClickListener {

//    @Inject
//    MessagePresenter presenter;


    public SelfOldFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelfOldFragment newInstance() {
        SelfOldFragment fragment = new SelfOldFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_self_old;
    }

    @Override
    public void initView(View view) {

        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewBind.clTitleBar.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        viewBind.clTitleBar.setLayoutParams(layoutParams);

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackground(R.color.normal_bg);
            skinTitleView.setTitleText(getString(R.string.me));
            skinTitleView.hideBackBtn();
        }


        // TODO: 2020/9/22 0022 不使用对方的钱包
        viewBind.myMonry.setVisibility(View.GONE);

        initData();
        viewBind.mySpaceRl.setOnClickListener(this);
        viewBind.myCollectionRl.setOnClickListener(this);
        viewBind.localCourseRl.setOnClickListener(this);
        viewBind.settingRl.setOnClickListener(this);
        viewBind.llWallte.setOnClickListener(this);
        viewBind.llPayCome.setOnClickListener(this);
        viewBind.infoRl.setOnClickListener(this);
        viewBind.ivQrCode.setOnClickListener(this);
        viewBind.myBankCardRl.setOnClickListener(this);
    }

    private UserViewModel userViewModel;


    private Observer<List<UserInfo>> userInfoLiveDataObserver = new Observer<List<UserInfo>>() {
        @Override
        public void onChanged(@Nullable List<UserInfo> userInfos) {
            if (userInfos == null) {
                return;
            }
            for (UserInfo info : userInfos) {
                if (info.uid.equals(userViewModel.getUserId())) {

                    updateUserInfo(info);
                    break;
                }
            }
        }
    };

    private void updateUserInfo(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        viewBind.infoRl.setTag(userInfo);
        EasyLoadImage.loadRoundCornerImage(App.getInst(), R.drawable.avatar_normal, userInfo.portrait, viewBind.avatarImg, DimensUtils.dp2px(App.getInst(), 5f), 0);
        viewBind.nickNameTv.setText(userInfo.displayName);
        Accounts accounts = presenter.accountsDao.get().getAccounts();
        if (accounts != null) {
            viewBind.authStyleTv.setText(getString(R.string.auth_style) + "：" + getString(accounts.getRank() == 1 ? R.string.phone_number : R.string.email));
        }
//        if (EmptyUtils.isNotEmpty(userInfo.email) || EmptyUtils.isNotEmpty(userInfo.mobile)) {
//            viewBind.phoneNumberTv.setText(EmptyUtils.isEmpty(userInfo.mobile) ? userInfo.email : userInfo.mobile);
//        } else {
//
//
//        }

    }

    private void initData() {

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUserInfoAsync(userViewModel.getUserId(), true)
                .observe(getViewLifecycleOwner(), info -> updateUserInfo(info));
        MutableLiveData<List<UserInfo>> mutableLiveData;
        if ((mutableLiveData = userViewModel.userInfoLiveData()) != null) {
            mutableLiveData.observeForever(userInfoLiveDataObserver);
        }
    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.llPayCome://首付款
                    RouterManager.goPaymentOrReceiptActivity();
                    break;
                case R.id.llWallte://我的钱包
                    RouterManager.goMyWalletActivity((UserInfo) viewBind.infoRl.getTag());
                    break;
                case R.id.ivQrCode:
                    UserInfo userInfo = (UserInfo) viewBind.infoRl.getTag();
                    if (userInfo != null) {
                        String qrCodeValue = WfcScheme.QR_CODE_PREFIX_USER + userInfo.uid;
                        startActivity(QRCodeActivity.buildQRCodeIntent(getActivity(), getString(R.string.qrcode), userInfo.portrait, qrCodeValue));
                    }

                    break;
                case R.id.search_public_number:
                    break;
                case R.id.info_rl:
                    Intent intent = new Intent(getActivity(), UserInfoActivity.class);
                    intent.putExtra("userInfo", (UserInfo) v.getTag());
                    startActivity(intent);
                    break;
                case R.id.my_space_rl:
                    // 我的动态
                    UserInfo userInfo2 = (UserInfo) viewBind.infoRl.getTag();
                    if (userInfo2 != null) {
                        Intent intent2 = new Intent(getActivity(), FeedListActivity.class);
                        intent2.putExtra("userInfo", userInfo2);
                        startActivity(intent2);
                    }
                    break;
                case R.id.my_bank_card_rl:
                    //银行卡管理
                    RouterManager.goBankCardManageActivity(0);
                    break;
                case R.id.my_collection_rl:
                    // 我的收藏
//                    UserInfo userInfo3 = (UserInfo) viewBind.infoRl.getTag();
//                    if (userInfo3 != null) {
//                        RouterManager.goTransferMoneyActivity(userInfo3, getString(R.string.withdraw));
//                    }
//                startActivity(new Intent(getActivity(), MyCollection.class));
                    break;
                case R.id.local_course_rl:
                    // 我的课件
//                startActivity(new Intent(getActivity(), LocalCourseActivity.class));
                    break;
                case R.id.setting_rl:
                    // 设置
                    RouterManager.goSettingActivity();
//                startActivity(new Intent(getActivity(), SettingActivity.class));
                    break;

            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MutableLiveData<List<UserInfo>> mutableLiveData;
        if (userViewModel != null && (mutableLiveData = userViewModel.userInfoLiveData()) != null) {
            mutableLiveData.removeObserver(userInfoLiveDataObserver);
        }

    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {

    }
}

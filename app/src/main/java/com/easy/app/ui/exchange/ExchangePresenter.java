package com.easy.app.ui.exchange;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Balance;
import com.easy.app.helper.RandomHelper;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;

import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ExchangePresenter extends AppPresenter<ExchangeView> {

    @Inject
    public ExchangePresenter() {

    }


    public void requestBalance() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "balance");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Balance.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.balanceCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.balanceCallback(response);
                        });
    }


    public void requestExchange(int type, String amount) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "shinfo");
        parameter.put("busin", "duihuan");
        parameter.put("orderid", RandomHelper.getOrderId());
        parameter.put("amount", amount);
        parameter.put("atype", type == 1 ? 0 : 1);//币种 0 sh 1 usdt
        parameter.put("btype", type == 1 ? 1 : 0);//币种 0 sh 1 usdt
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.exchangeCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.exchangeCallback(response);
                        });
    }


}

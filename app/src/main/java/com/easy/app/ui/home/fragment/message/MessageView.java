package com.easy.app.ui.home.fragment.message;


import com.easy.framework.base.BaseView;

public interface MessageView extends BaseView {


    void permissionCallback(Boolean granted, Throwable e);
}

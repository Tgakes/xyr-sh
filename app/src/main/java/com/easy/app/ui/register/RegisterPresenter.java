package com.easy.app.ui.register;

import android.Manifest;
import android.app.Activity;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.callback.UploadCallback;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.easy.utils.Md5Utils;
import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RegisterPresenter extends AppPresenter<RegisterView> {

    @Inject
    public RegisterPresenter() {

    }

    public void requestRegister(Map<String,Object> parameter) {



        //    String json = JSON.toJSONString(parameter);
//            Logger.i("requestRegister 加密前 json===="+json);
//            json = RsaUtils.encryptRSA(URLEncoder.encode(json, "UTF-8"));
//            Logger.i("requestRegister 加密后===="+json);
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.regCallback(result);
                        },
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                }else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.regCallback(response);
                        });


    }


    /**
     * 验证视频
     */
    public void requestUploadFile(Activity activity, File finalFile) {


        Map<String, File> fileMap = new HashMap<>();
        fileMap.put("imgfile", finalFile);
        RxHttp.post(UrlConstant.UPLOAD)
                .addHeader(getHeaderMap())
                .file(fileMap)
                .upload(new UploadCallback() {
                    @Override
                    public void handleSuccess(Response t) {
                        activity.runOnUiThread(() -> mvpView.uploadCallback(t));

                    }


                    @Override
                    public void handleError(ApiException exception) {
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });

                    }

                    @Override
                    public void onProgress(File file, long currentSize, long totalSize, float progress, int currentIndex, int totalFile) {
                        Logger.i("onProgress currentSize===" + currentSize + ",totalSize===" + totalSize + ",====progress=====" + progress);
                    }

                    @Override
                    public Response onConvert(String data) {
                        Logger.i("onConvert data===" + data);
                        return null;
                    }

                    @Override
                    public void onSuccess(Object value) {
                        Logger.i("onSuccess value===" + value);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setCode(Response.SUCCESS_CODE);
                            response.setStatus(Response.SUCCESS_STATUS);
                            response.setMsg((String) value);
                            response.setData((String) value);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onError(int code, String desc) {
                        Logger.i("onError code===" + code + ",desc===" + desc);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(desc);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onCancel() {
                        Logger.i("onCancel value===");
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });
                    }


                });
    }

    /**
     *
     * @param telNum
     */
  /*  public void requestSms(String telNum) {

        Map<String, Object> parameter = new HashMap<>();
        parameter.put("actsms", 1);//1注册 2转账 3登录 4兑换 5提现 6修改资料
        parameter.put("number", telNum);//接收方手机号
        RxHttp.post(UrlConstant.SMS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.smsCallback(result);
                        },
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.smsCallback(response);
                        });
    }*/



    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.READ_PHONE_STATE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }


    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestReadWritePermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionReadWriteCallback(granted, null),
                        throwable -> mvpView.permissionReadWriteCallback(null, throwable));
    }
}

package com.easy.app.ui.findPwd;

import android.graphics.drawable.Drawable;
import android.widget.EditText;

import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.ui.empty.EmptyView;
import com.easy.utils.DimensUtils;

import java.util.List;

import javax.inject.Inject;

public class FindPwdPresenter extends AppPresenter<FindPwdView> {

    @Inject
    public FindPwdPresenter() {

    }


    public void setBound(List<EditText> mEditList) {// 为Edit内的drawableLeft设置大小
        int size = mEditList.size();
        for (int i = 0; i < size; i++) {
            Drawable[] compoundDrawable = mEditList.get(i).getCompoundDrawables();
            Drawable drawable = compoundDrawable[0];
            if (drawable != null) {
                drawable.setBounds(0, 0, DimensUtils.dp2px(App.getInst(), 20), DimensUtils.dp2px(App.getInst(), 20));
                mEditList.get(i).setCompoundDrawables(drawable, null, null, null);
            }
        }
    }


}

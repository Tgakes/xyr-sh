package com.easy.app.ui.countryCode;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.UICountryCode;
import com.easy.app.databinding.CountryCodeBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ValidatorUtil;
import com.easy.widget.SkinTitleView;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;
import java.util.List;

import cn.wildfire.chat.kit.contact.model.UIUserInfo;
import cn.wildfire.chat.kit.widget.QuickIndexBar;

@ActivityInject
@Route(path = RouterManager.COUNTRY_CODE, name = "选择国家和地区代码")
public class CountryCodeActivity extends BaseActivity<CountryCodePresenter, CountryCodeBinding> implements CountryCodeView, QuickIndexBar.OnLetterUpdateListener, BaseQuickAdapter.OnItemClickListener, TextWatcher {


    CountryCodeAdapter adapter;

    List<UICountryCode> uiCountryCodeList;

    LinearLayoutManager linearLayoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.country_code;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.country_code));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }

        uiCountryCodeList = presenter.getList();
        List<UICountryCode> uiCountryCode = new ArrayList<>();
        uiCountryCode.addAll(uiCountryCodeList);
        viewBind.acEvSearch.setTag(uiCountryCode);
        adapter = new CountryCodeAdapter(R.layout.adapter_country_code, uiCountryCodeList);
        viewBind.recyclerView.setAdapter(adapter);
//      adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        linearLayoutManager = new LinearLayoutManager(this);
        viewBind.recyclerView.setLayoutManager(linearLayoutManager);
        viewBind.recyclerView.setHasFixedSize(true);
        viewBind.quickIndexBar.setVisibility(View.VISIBLE);
        viewBind.quickIndexBar.setOnLetterUpdateListener(this);
        adapter.setOnItemClickListener(this);
        viewBind.acEvSearch.addTextChangedListener(this);
    }


    @Override
    public void onLetterUpdate(String letter) {
        if ("↑".equalsIgnoreCase(letter) || "☆".equalsIgnoreCase(letter)) {
            linearLayoutManager.scrollToPositionWithOffset(0, 0);
        } else if ("#".equalsIgnoreCase(letter)) {
            int size = uiCountryCodeList.size();
            for (int i = 0; i < size; i++) {
                UICountryCode uiCountryCode = uiCountryCodeList.get(i);
                if ("#".equals(uiCountryCode.getCategory())) {
                    linearLayoutManager.scrollToPositionWithOffset(i, 0);
                    break;
                }
            }
        } else {
            int size;
            if (uiCountryCodeList != null && (size = uiCountryCodeList.size()) > 0) {
                for (int i = 0; i < size; i++) {
                    UICountryCode friend = uiCountryCodeList.get(i);
                    if (friend.getCategory().compareTo(letter) >= 0) {
                        linearLayoutManager.scrollToPositionWithOffset(i, 0);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onLetterCancel() {
        viewBind.indexLetterTextView.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        LiveEventBus.get("countryCode").post(uiCountryCodeList.get(position).getCountryCode());
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String content = s.toString();
        List<UICountryCode> tag = (List<UICountryCode>) viewBind.acEvSearch.getTag();
        this.uiCountryCodeList.clear();
        if (EmptyUtils.isEmpty(content)) {
            this.uiCountryCodeList.addAll(tag);
        } else {
            List<UICountryCode> newData = new ArrayList<>();
            boolean isChinese = ValidatorUtil.isChinese(content);
            for (UICountryCode uiCountryCode : tag) {
                if (isChinese) {
                    if (uiCountryCode.getCountryCode().getCnKey().contains(content)) {
                        newData.add(uiCountryCode);
                    }
                }else {
                    if (uiCountryCode.getCountryCode().getEnKey().contains(content)) {
                        newData.add(uiCountryCode);
                    }
                }
            }
            this.uiCountryCodeList.addAll(newData);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

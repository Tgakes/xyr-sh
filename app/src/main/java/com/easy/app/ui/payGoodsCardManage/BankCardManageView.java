package com.easy.app.ui.payGoodsCardManage;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

import java.util.List;

public interface BankCardManageView extends BaseView {

    void bankCardListCallback(Response<List> response);

}

package com.easy.app.ui.dialog.sellSh;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Balance;
import com.easy.app.bean.TransferReq;
import com.easy.app.helper.RandomHelper;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SellShDialogPresenter extends AppPresenter<SellShDialogView> {


    @Inject
    public SellShDialogPresenter() {

    }

    public void requestPlaceOrder(String bankId, String businessId, String markId,String amount, String rate,String realPay) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "shinfo");
        parameter.put("busin", "chushou");
        parameter.put("bankid", bankId);//收款的uid
        parameter.put("cdusid", businessId);//收款的uid
        parameter.put("amount", amount);
        parameter.put("rate", rate);
        parameter.put("realPay", realPay);
        parameter.put("usid", markId);
        parameter.put("orderid", RandomHelper.getOrderId());
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.placeOrderCallback(result),
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.placeOrderCallback(response);
                        });
    }

    public void requestBalance() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "balance");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Balance.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.balanceCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.balanceCallback(response);
                        });
    }


}

package com.easy.app.ui.chargeMoney;

import android.Manifest;
import android.app.Activity;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.ChargeCoinsAddress;
import com.easy.app.helper.RandomHelper;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.callback.UploadCallback;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;
import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChargeCoinsPresenter extends AppPresenter<ChargeCoinsView> {

    @Inject
    public ChargeCoinsPresenter() {

    }


    /**
     * 验证视频
     */
    public void requestUploadFile(Activity activity, File finalFile) {


        Map<String, File> fileMap = new HashMap<>();
        fileMap.put("imgfile", finalFile);
        RxHttp.post(UrlConstant.UPLOAD)
                .addHeader(getHeaderMap())
                .file(fileMap)
                .upload(new UploadCallback() {
                    @Override
                    public void handleSuccess(Response t) {
                        activity.runOnUiThread(() -> mvpView.uploadCallback(t));

                    }


                    @Override
                    public void handleError(ApiException exception) {
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });

                    }

                    @Override
                    public void onProgress(File file, long currentSize, long totalSize, float progress, int currentIndex, int totalFile) {
                        Logger.i("onProgress currentSize===" + currentSize + ",totalSize===" + totalSize + ",====progress=====" + progress);
                    }

                    @Override
                    public Response onConvert(String data) {
                        Logger.i("onConvert data===" + data);
                        return null;
                    }

                    @Override
                    public void onSuccess(Object value) {
                        Logger.i("onSuccess value===" + value);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setCode(Response.SUCCESS_CODE);
                            response.setStatus(Response.SUCCESS_STATUS);
                            response.setMsg((String) value);
                            response.setData((String) value);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onError(int code, String desc) {
                        Logger.i("onError code===" + code + ",desc===" + desc);
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(desc);
                            mvpView.uploadCallback(response);
                        });
                    }

                    @Override
                    public void onCancel() {
                        Logger.i("onCancel value===");
                        activity.runOnUiThread(() -> {

                            Response<String> response = new Response<>();
                            response.setMsg(App.getInst().getString(R.string.request_fail));
                            mvpView.uploadCallback(response);
                        });
                    }


                });
    }



    public void requestAddress() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "usdtinfo");
        parameter.put("busin", "huoqu");
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(ChargeCoinsAddress.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.chargeCoinAddressCallback(result);
                        },
                        throwable -> {
                            Response<ChargeCoinsAddress> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.chargeCoinAddressCallback(response);
                        });
    }


    public void requestChargeComplete(String id,String money,String pic) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "usdtinfo");
        parameter.put("busin", "chonzhi");
        parameter.put("bankid", id);
        parameter.put("money", money);
        parameter.put("mpic", pic);
        parameter.put("orderid", RandomHelper.getOrderId());
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.chargeCompleteCallback(result);
                        },
                        throwable -> {
                            Response response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.UNKNOWN_HOST || apiException.getCode() == ExceptionEngine.NET_ERROR)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.chargeCompleteCallback(response);
                        });
    }

    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestReadWritePermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionReadWriteCallback(granted, null),
                        throwable -> mvpView.permissionReadWriteCallback(null, throwable));
    }


}

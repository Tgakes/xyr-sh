package com.easy.app.ui.countryCode;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.CountryCode;
import com.easy.app.bean.PayGoodsCardManage;
import com.easy.app.bean.UICountryCode;

import java.util.List;

public class CountryCodeAdapter extends BaseQuickAdapter<UICountryCode, BaseViewHolder> {


    public CountryCodeAdapter(int layoutResId, @Nullable List<UICountryCode> data) {
        super(layoutResId, data);

    }


    @Override
    protected void convert(@NonNull BaseViewHolder helper, UICountryCode item) {

        AppCompatTextView acTvLetter = helper.getView(R.id.acTvLetter);
        if (item.isShowCategory()) {
            acTvLetter.setVisibility(View.VISIBLE);
            acTvLetter.setText(item.getCategory());
        } else {
            acTvLetter.setVisibility(View.GONE);
        }
        helper.setText(R.id.acTCnName, item.getCountryCode().getCnKey());
        helper.setText(R.id.acTEnName, item.getCountryCode().getEnKey());
        helper.setText(R.id.acTvCountryCode, "+" + item.getCountryCode().getCodeKey());
    }


}

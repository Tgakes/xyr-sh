package com.easy.app.ui.bill;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.bean.Bill;
import com.easy.utils.StreamUtils;

import java.util.List;

public class BillAdapter extends BaseQuickAdapter<Bill, BaseViewHolder> {


    public BillAdapter(int layoutResId, @Nullable List<Bill> data) {
        super(layoutResId, data);


    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

     /*   TextView textView = holder.getView(R.id.loading_text);
        if (textView != null) {
//            ProgressBar progressBar =  holder.getView(R.id.loading_progress);
//            textView.setTextColor(StreamUtils.getInstance().resourceToColor(R.color.white, App.getInst()));
            return;
        }*/

        AppCompatTextView tvMoney = holder.getView(R.id.tv_money);
        AppCompatTextView tvName = holder.getView(R.id.tv_name);
        AppCompatTextView tvTime = holder.getView(R.id.tv_time);
        if (tvMoney == null || tvName == null || tvTime == null) {
            return;
        }
        if (position % 2 == 0) {
            tvMoney.setTextColor(StreamUtils.getInstance().resourceToColor(R.color.records_of_consumption, App.getInst()));
            tvMoney.setText("-666.0");

        } else {
            tvMoney.setTextColor(StreamUtils.getInstance().resourceToColor(R.color.ji_jian_lan, App.getInst()));
            tvMoney.setText("+888.0");

        }
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Bill item) {


//        EasyLoadImage.loadImage2(context, item.getIcon(), helper.getView(R.id.icon));
//        helper.setText(R.id.tvMineName, item.getName());
//        helper.setImageResource(R.id.ivMineIcon, item.getIconRes());
////        helper.setText(R.id.tvDownloadSize, "21.82MB/63.69MB");
////        helper.setText(R.id.tvSpeed, "0.15MB/S");
////        helper.addOnClickListener(R.id.btnState);
    }


}

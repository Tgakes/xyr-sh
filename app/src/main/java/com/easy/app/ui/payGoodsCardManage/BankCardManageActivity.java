package com.easy.app.ui.payGoodsCardManage;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.PayGoodsCardManage;
import com.easy.app.databinding.BankCardManageBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.net.beans.Response;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.StateView;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

@ActivityInject
@Route(path = RouterManager.BANK_CARD_MANAGE, name = "银行卡管理")
public class BankCardManageActivity extends BaseActivity<BankCardManagePresenter, BankCardManageBinding> implements BankCardManageView, OnRefreshListener, View.OnClickListener, BaseQuickAdapter.OnItemClickListener {

    BankCardManageAdapter adapter;

    int page = 1;

    List<PayGoodsCardManage> payGoodsCardManageList;

    /**
     * 1:选择银行卡
     */
    @Autowired(name = "skipPage")
    int skipPage;


    @Override
    public int getLayoutId() {
        return R.layout.bank_card_manage;
    }

    @Override
    public void initView() {
        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setTitleText(getString(R.string.bank_card_manage));
            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_add_blue, App.getInst()));
            skinTitleView.setRightClickListener(v -> {
                if (ViewUtils.canClick()) {
                    RouterManager.goAddBankActivity();
                }
            });
        }
        payGoodsCardManageList = new ArrayList<>();
        adapter = new BankCardManageAdapter(R.layout.adapter_bank_card_manage, payGoodsCardManageList);
        viewBind.recyclerView.setAdapter(adapter);

//        adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewBind.recyclerView.setHasFixedSize(true);
        StateView emptyView = new StateView(this);
        emptyView.setOnClickListener(this);
        adapter.setEmptyView(emptyView);

        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(false);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
//        viewBind.swRefresh.setOnLoadMoreListener(this);
        adapter.setOnItemClickListener(this);
        showLoading();
        presenter.requestList();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContainer:
                Logger.i("====onClick llContainer 请求====");
                showLoading();
                presenter.requestList();
                break;
        }
    }


    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        presenter.requestList();
    }




    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        Log.i("logs", "position===" + position);
        if (skipPage == 1) {
            LiveEventBus.get("chooseBank").post(payGoodsCardManageList.get(position));
            finish();
        }
    }

    @Override
    public void bankCardListCallback(Response<List> response) {
        hideLoading();
        if (response.isSuccess()) {
            viewBind.swRefresh.finishRefresh(true);
            List<PayGoodsCardManage> payGoodsCardManages = response.getResultObj();
            this.payGoodsCardManageList.clear();
            if (payGoodsCardManages != null && !payGoodsCardManages.isEmpty()) {
                this.payGoodsCardManageList.addAll(payGoodsCardManages);
            }
            adapter.notifyDataSetChanged();
        } else {
            viewBind.swRefresh.finishRefresh();
            ToastUtils.showShort(response.getMsg());
        }
    }
}

package com.easy.app.ui.sellCoins;


import com.easy.app.bean.Balance;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface SellCoinsView extends BaseView {

    void balanceCallback(Response<Balance> response);

}

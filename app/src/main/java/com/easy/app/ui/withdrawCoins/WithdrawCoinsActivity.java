package com.easy.app.ui.withdrawCoins;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Balance;
import com.easy.app.databinding.WithdrawCoinsBinding;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTitleView;

import java.math.BigDecimal;

@ActivityInject
@Route(path = RouterManager.WITHDRAW_MONEY, name = "提币页面")
public class WithdrawCoinsActivity extends BaseActivity<WithdrawCoinsPresenter, WithdrawCoinsBinding> implements WithdrawCoinsView, View.OnClickListener, TextWatcher {


    @Override
    public int getLayoutId() {
        return R.layout.withdraw_coins;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.withdraw_money));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }

        ButtonColorChange.colorChange(this, viewBind.withdrawMoneyBtn);
        viewBind.acTvAvailable.setText(getString(R.string.available_usdt,"0"));
        viewBind.acTvFee.setText(getString(R.string.fee,"0"));
        viewBind.acTvWithdrawAll.setOnClickListener(this);
        viewBind.withdrawMoneyBtn.setOnClickListener(this);
        viewBind.acEvWithdrawNum.addTextChangedListener(this);
        presenter.requestBalance();
    }


    @Override
    public void onClick(View v) {

        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.acTvWithdrawAll:
                    String amount = (String) viewBind.acTvAvailable.getTag();
                    if (EmptyUtils.isNotEmpty(amount) && Double.parseDouble(amount) > 0) {
                        viewBind.acEvWithdrawNum.setText(amount);
                        viewBind.acEvWithdrawNum.setSelection(viewBind.acEvWithdrawNum.getText().toString().length());
                    }
                    break;
                case R.id.withdraw_money_btn:

                    String address = viewBind.acEvReceiptAddress.getText().toString();
                    if (EmptyUtils.isEmpty(address)) {
                        ToastUtils.showShort(getString(R.string.input_withdraw_num));
                        return;
                    }

                    String money = viewBind.acEvWithdrawNum.getText().toString();
                    if (money.endsWith(SellShDialogFragment.POINT)) {
                        money = money.substring(0, money.length() - 1);
                    }
                    double mMoney = 0;
                    try {
                        mMoney = Double.parseDouble(money);
                    } catch (NumberFormatException e) {
                        ToastUtils.showShort(getString(R.string.input_withdraw_num));
                        return;
                    }

                    if (TextUtils.isEmpty(money) || mMoney <= 0) {
                        ToastUtils.showShort(getString(R.string.input_withdraw_num));
                        return;
                    }

                    showLoading();
                    presenter.requestWithdrawal(address, money, viewBind.acEvAlign.getText().toString());


                    break;
            }
        }
    }

    @Override
    public void balanceCallback(Response<Balance> response) {
        if (response.isSuccess()) {
            Balance balance = response.getResultObj();
            if (balance != null) {
                viewBind.acTvAvailable.setTag(balance.getUsdt());
                viewBind.acTvAvailable.setText(getString(R.string.available_usdt, Money.getMoneyFormat(balance.getUsdt())));

            }

        } else {
            ToastUtils.showShort(response.getMsg());
        }
    }

    @Override
    public void withdrawalCallback(Response response) {
        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            finish();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!ViewUtils.inputSinglePoint(viewBind.acEvWithdrawNum, s)) {
            String text = s.toString();
            if (EmptyUtils.isNotEmpty(text)) {
               /* int length = text.length();
                if (length - 1 - text.indexOf(POINT) > 2) {
                    text = text.substring(0, s.toString().indexOf(POINT) + 3);
                    viewBind.acEvSellAmount.setText(text);
                    viewBind.acEvSellAmount.setSelection(text.length());
                } else {

                }*/

                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.acEvWithdrawNum.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.acEvWithdrawNum.setText(text.substring(1));
                }

            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

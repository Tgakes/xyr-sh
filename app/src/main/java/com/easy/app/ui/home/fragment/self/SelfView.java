package com.easy.app.ui.home.fragment.self;


import com.easy.framework.base.BaseView;

public interface SelfView extends BaseView {

    void permissionCallback(Boolean granted, Throwable e);

}

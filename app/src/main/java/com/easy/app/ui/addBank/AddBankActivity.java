package com.easy.app.ui.addBank;

import android.content.res.ColorStateList;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.view.ViewCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.AddBankBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.dialog.GodDialog;
import com.easy.net.beans.Response;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinTextView;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;

import java.util.Map;

@ActivityInject
@Route(path = RouterManager.ADD_BANK, name = "添加银行卡")
public class AddBankActivity extends BaseActivity<AddBankPresenter, AddBankBinding> implements AddBankView {


    @Override
    public int getLayoutId() {
        return R.layout.add_bank;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setTitleText(getString(R.string.add_bank));



        }
        ViewCompat.setBackgroundTintList(viewBind.btnComplete, ColorStateList.valueOf(SkinUtils.getSkin(this).getAccentColor()));
        viewBind.btnComplete.setOnClickListener(v -> {
            if (ViewUtils.canClick()) {
                Map<String, Object> map = getMap();
                if (map != null) {
                    showLoading();
                    presenter.requestRealName(map);
                }
            }
        });

        initCenterView();
    }


    private Map<String, Object> getMap() {

        if (viewBind.llAddBankItem.getChildCount() == 5) {
            Map<String, Object> map = presenter.getBodyMap();
            for (int i = 0; i < 5; i++) {
                View view = viewBind.llAddBankItem.getChildAt(i);
                EditText acEvItemVal = view.findViewById(R.id.acEvItemVal);
                String val = acEvItemVal.getText().toString();
                if (EmptyUtils.isEmpty(val)) {
                    ToastUtils.showShort(acEvItemVal.getHint());
                    return null;
                }
                switch (i) {
                    case 0:
                        map.put("carname", val);
                        viewBind.mainContent.setTag(val);
                        break;
                    case 1:
                        map.put("bankname", val);
                        break;
                    case 2:
                        map.put("rutename", val);
                        break;
                    case 3:
                        map.put("carnum", val);
                        break;
                    case 4:
                        map.put("carnumconfirm", val);
                        break;
                }
            }
            map.put("business", "bankcar");
            map.put("profession", "addcar");
            return map;
        }

        return null;
    }

    private void initCenterView() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        String[] items = getResources().getStringArray(R.array.addBankList);
        String[] itemsHint = getResources().getStringArray(R.array.addBankListHint);

        int length = items.length;
        for (int i = 0; i < length; i++) {
            View view = layoutInflater.inflate(R.layout.add_bank_item, null);
            TextView acTvItemName = view.findViewById(R.id.acTvItemName);
            EditText acEvItemVal = view.findViewById(R.id.acEvItemVal);
            switch (i) {
                case 0:
                    acEvItemVal.setInputType(InputType.TYPE_CLASS_TEXT);
                    acEvItemVal.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    String realName = PreferenceUtils.getString(App.getInst(),"realName");
                    if (EmptyUtils.isNotEmpty(realName)) {
                        acEvItemVal.setText(realName);
                        acEvItemVal.setEnabled(false);
                    }
                    break;
                case 1:
                case 2:
                    acEvItemVal.setInputType(InputType.TYPE_CLASS_TEXT);
                    acEvItemVal.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    break;
                case 3:
                    acEvItemVal.setKeyListener(DigitsKeyListener.getInstance("1234567890"));
                    acEvItemVal.setInputType(InputType.TYPE_CLASS_NUMBER);
                    acEvItemVal.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    break;
                case 4:
                    acEvItemVal.setKeyListener(DigitsKeyListener.getInstance("1234567890"));
                    acEvItemVal.setInputType(InputType.TYPE_CLASS_NUMBER);
                    acEvItemVal.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    break;
            }
            acTvItemName.setText(items[i]);
            acEvItemVal.setHint(itemsHint[i]);
            viewBind.llAddBankItem.addView(view);
        }
    }


    @Override
    public void addBankCardCallback(Response response) {
        hideLoading();

        if (response.isSuccess()) {
            ToastUtils.showShort(response.getMsg());
            finish();
        } else {
            String userRealName;
            if (Response.NEED_REAL_NAME_STATUS.equals(response.getStatus()) && EmptyUtils.isNotEmpty(userRealName = (String) viewBind.mainContent.getTag())) {
                GodDialog logoutDialog = new GodDialog.Builder(this)
                        .setMessage(getString(R.string.real_name_des, userRealName))
                        .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                            dialog.dismiss();
                            showLoading();
                            presenter.requestRealName(userRealName);
                        })
                        .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                        .build();
                logoutDialog.show();
            }else {
                ToastUtils.showShort(response.getMsg());
            }
        }
    }

    @Override
    public void realNameCallback(Response response) {
        if (response.isSuccess()) {
            PreferenceUtils.putString(App.getInst(), "realName", (String) viewBind.mainContent.getTag());
            Map<String, Object> map = getMap();
            if (map != null) {
                presenter.requestRealName(map);
            } else {
                hideLoading();
            }
        } else {
            hideLoading();
            ToastUtils.showShort(response.getMsg());

        }
    }
}

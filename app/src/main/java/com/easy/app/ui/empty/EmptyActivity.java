package com.easy.app.ui.empty;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.EmptyBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;

@ActivityInject
@Route(path = RouterManager.EMPTY_ACTIVITY, name = "空白页")
public class EmptyActivity extends BaseActivity<EmptyPresenter, EmptyBinding> implements EmptyView {



    @Override
    public int getLayoutId() {
        return R.layout.empty;
    }

    @Override
    public void initView() {


    }



}

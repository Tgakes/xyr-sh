package com.easy.app.ui.register;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface RegisterView extends BaseView {

    void regCallback(Response<String> response);

    void uploadCallback(Response response);

    void permissionCallback(Boolean granted, Throwable e);

    void permissionReadWriteCallback(Boolean granted, Throwable e);

}

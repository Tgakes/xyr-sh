package com.easy.app.ui.receiptPayMoney;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.bean.Pay;
import com.easy.app.bean.Receipt;
import com.easy.app.bean.TransferReq;
import com.easy.app.databinding.ReceiptPayMoneyBinding;
import com.easy.app.ui.dialog.KeyBoad;
import com.easy.app.ui.dialog.VerifyDialog;
import com.easy.app.ui.dialog.payPwd.PayPwdDialogFragment;
import com.easy.app.ui.dialog.sellSh.SellShDialogFragment;
import com.easy.app.ui.setting.SettingActivity;
import com.easy.app.util.ButtonColorChange;
import com.easy.app.util.Money;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.dialog.GodDialog;
import com.easy.loadimage.EasyLoadImage;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.WalletView;

@ActivityInject
@Route(path = RouterManager.RECEIPT_PAY_MONEY, name = "付款页")
public class ReceiptPayMoneyActivity extends BaseActivity<ReceiptPayMoneyPresenter, ReceiptPayMoneyBinding> implements ReceiptPayMoneyView, PayPwdDialogFragment.PayPwdListener {


    Receipt receipt;

    private KeyBoad keyBoad;

    private boolean isUiCreate = false;

    private String money, words;

    private boolean isFixedMoney;// 收款方是否固定了金额

    @Autowired(name = "receiptPayMoney")
    String receiptPayMoney;

    //1扫一扫 2 转账
    @Autowired(name = "type")
    int type;

    @Override
    public int getLayoutId() {
        return R.layout.receipt_pay_money;
    }

    @Override
    public void initView() {
        ARouter.getInstance().inject(this);
        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackIcon(R.mipmap.return_icon);
            skinTitleView.setTitleText(getString(R.string.rp_payment));
        }



        if (EmptyUtils.isEmpty(receiptPayMoney) || (receipt = JSON.parseObject(receiptPayMoney, Receipt.class)) == null) {
            finish();
            return;
        }
        isFixedMoney = !EmptyUtils.isEmpty(receipt.getMoney());
        EasyLoadImage.loadCircleImage(App.getInst(), receipt.getAvatar(), viewBind.payAvatarIv,0);
        viewBind.payNameTv.setText(receipt.getUserName());
        ButtonColorChange.colorChange(this, viewBind.transferBtn);

        viewBind.transferJeTv.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);// 允许输入数字与小数点
        // 禁止输入框复制粘贴
//        EditTextUtil.disableCopyAndPaste(viewBind.etTransfer);
        keyBoad = new KeyBoad(ReceiptPayMoneyActivity.this, getWindow().getDecorView(), viewBind.etTransfer);
        if (isFixedMoney) {
            viewBind.ll1.setVisibility(View.GONE);
            viewBind.ll2.setVisibility(View.VISIBLE);
            viewBind.fixedMoneyTv.setText(receipt.getMoney());
            if (!TextUtils.isEmpty(receipt.getDescription())) {
                viewBind.fixedDescTv.setText(receipt.getDescription());
            } else {
                viewBind.fixedDescTv.setVisibility(View.GONE);
            }
        }
        initEvent();
        initKeyBoad();
    }


    private void initEvent() {

        viewBind.tvAccountType.setOnClickListener(view -> {
            WalletView.showWalletView(accountType -> viewBind.tvAccountType.setText(accountType), this);
        });

        viewBind.transferEditDescTv.setOnClickListener(v -> {
            VerifyDialog verifyDialog = new VerifyDialog(ReceiptPayMoneyActivity.this);
            verifyDialog.setVerifyClickListener(getString(R.string.receipt_add_remake), getString(R.string.transfer_desc_max_length_10),
                    words, 10, new VerifyDialog.VerifyClickListener() {
                        @Override
                        public void cancel() {

                        }

                        @Override
                        public void send(String str) {
                            words = str;


                            if (TextUtils.isEmpty(words)) {
                                viewBind.transferDescTv.setText("");
                                viewBind.transferDescTv.setVisibility(View.GONE);
                                viewBind.transferEditDescTv.setText(getString(R.string.receipt_add_remake));
                            } else {
                                viewBind.transferDescTv.setText(str);
                                viewBind.transferDescTv.setVisibility(View.VISIBLE);
                                viewBind.transferEditDescTv.setText(getString(R.string.transfer_modify));
                            }

                            if (!isFixedMoney) {
                                keyBoad.show();
                            }
                        }
                    });
            verifyDialog.setOkButton(R.string.sure);
            keyBoad.dismiss();
            Window window = verifyDialog.getWindow();
            if (window != null) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE); // 软键盘弹起
            }
            verifyDialog.show();
        });

//        findViewById(R.id.transfer_btn).setBackgroundColor(SkinUtils.getSkin(this).getAccentColor());

        viewBind.transferBtn.setOnClickListener(v -> {

            if (receipt.getUserId().equals(presenter.accountsDao.get().getUserId())) {

                GodDialog otherDeviceLoginDialog = new GodDialog.Builder(this)
                        .setMessage(getString(R.string.not_support_transfer_self))
                        .setOneButton(getString(R.string.ok), (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .build();
                otherDeviceLoginDialog.show();
                return;
            }

            if (isFixedMoney) {
                String currency = receipt.getMoney().endsWith(WalletView.USDT_NAME) ? WalletView.USDT_NAME : WalletView.SH_NAME;
                String data = JSON.toJSONString(new TransferReq(Money.getMoneyFormat(receipt.getMoney().replace(currency, "")), receipt.getDescription(), receipt.getUserId(), receipt.getMoney().endsWith(WalletView.USDT_NAME)?2:1, 1));
                PayPwdDialogFragment.newInstance(new Pay(getString(R.string.rp_payment), data, Pay.TRANSFER), getSupportFragmentManager());
            } else {
                 double mMoney = 0;
                money = viewBind.etTransfer.getText().toString().trim();
                try {
                    mMoney = Double.parseDouble(money);
                } catch (NumberFormatException e) {
                    ToastUtils.showShort(getString(R.string.transfer_input_money));
                    return;
                }
                if (TextUtils.isEmpty(money) || mMoney <= 0) {
                    ToastUtils.showShort(getString(R.string.transfer_input_money));
                    return;
                }

                money = Money.getMoneyFormat(money);
                keyBoad.dismiss();
                String data = JSON.toJSONString(new TransferReq(money, words, receipt.getUserId(), viewBind.tvAccountType.getText().toString().endsWith(WalletView.USDT_NAME) ? 2:1, 1));
                PayPwdDialogFragment.newInstance(new Pay(getString(R.string.rp_payment), data, Pay.TRANSFER), getSupportFragmentManager());
            }
        });
    }

    private void initKeyBoad() {
        viewBind.etTransfer.setOnFocusChangeListener((v, hasFocus) -> {
            if (keyBoad != null && isUiCreate) {
                keyBoad.refreshKeyboardOutSideTouchable(!hasFocus);
            } else if (isUiCreate) {
                keyBoad.show();
            }
            if (hasFocus) {
                InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(viewBind.etTransfer.getWindowToken(), 0);
            }
        });

        viewBind.etTransfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.startsWith(SellShDialogFragment.POINT)) {
                    viewBind.etTransfer.setText("0" + text);
                } else if (text.startsWith("0") && !text.contains(SellShDialogFragment.POINT) && text.length() > 1) {
                    viewBind.etTransfer.setText(text.substring(1));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        viewBind.etTransfer.setOnClickListener(v -> {
            if (keyBoad != null) {
                keyBoad.show();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        isUiCreate = true;
    }



    @Override
    public void onPaySuccess(String orderId) {
        finish();
    }

    @Override
    public void onPayFail() {

    }
}

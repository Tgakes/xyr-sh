package com.easy.app.ui.home.fragment.discovery;


import android.content.Intent;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.FmDiscoveryBinding;
import com.easy.app.databinding.FmMessageBinding;
import com.easy.app.ui.dialog.MessagePopupWindow;
import com.easy.app.ui.home.HomeActivity;
import com.easy.app.ui.home.fragment.message.MessagePresenter;
import com.easy.app.ui.home.fragment.message.MessageView;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.qrcode.ui.qr_code.QrScanActivity;
import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.SkinTextView;
import com.easy.widget.SkinTitleView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import cn.wildfire.chat.kit.WfcUIKit;
import cn.wildfire.chat.kit.qrcode.ScanQRCodeActivity;
import cn.wildfire.chat.kit.viewmodel.MessageViewModel;
import cn.wildfire.chat.moment.FeedListActivity;
import cn.wildfirechat.message.Message;
import cn.wildfirechat.message.core.MessageStatus;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.remote.ChatManager;


/**
 * A simple {@link DiscoveryFragment} subclass.
 * Use the {@link DiscoveryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class DiscoveryFragment extends BaseFragment<DiscoveryPresenter, FmDiscoveryBinding> implements DiscoveryView, View.OnClickListener {


    public DiscoveryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DiscoveryFragment newInstance() {
        DiscoveryFragment fragment = new DiscoveryFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_discovery;
    }

    @Override
    public void initView(View view) {

        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewBind.clTitleBar.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        viewBind.clTitleBar.setLayoutParams(layoutParams);

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackground(R.color.normal_bg);
            skinTitleView.setTitleText(getString(R.string.find));
//            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.drawable.chat_more, App.getInst()));
            skinTitleView.hideBackBtn();
        }
        viewBind.scanning.setOnClickListener(this);
        viewBind.relFind.setOnClickListener(this);


        viewBind.takeOutOrderRl.setOnClickListener(this);
        viewBind.acceptorRl.setOnClickListener(this);
        viewBind.didiTaxiRl.setOnClickListener(this);
        viewBind.recruitmentRl.setOnClickListener(this);
        viewBind.paymentBehalfBusiness.setOnClickListener(this);
        initMoment();
    }


    private void initMoment() {
        if (!WfcUIKit.getWfcUIKit().isSupportMoment()) {
//            momentOptionItemView.setVisibility(View.GONE);
            return;
        }
        MessageViewModel messageViewModel = ViewModelProviders.of(this).get(MessageViewModel.class);
        messageViewModel.messageLiveData().observe(getViewLifecycleOwner(), uiMessage -> updateMomentBadgeView());
        messageViewModel.clearMessageLiveData().observe(getViewLifecycleOwner(), o -> updateMomentBadgeView());
    }

    private void updateMomentBadgeView() {
        List<Message> messages = ChatManager.Instance().getMessagesEx2(Collections.singletonList(Conversation.ConversationType.Single), Collections.singletonList(1), Arrays.asList(MessageStatus.Unread), 0, true, 100, null);
        int count = messages == null ? 0 : messages.size();
        ViewUtils.updateNum(viewBind.tvMomentsCount, count);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (WfcUIKit.getWfcUIKit().isSupportMoment()) {
            updateMomentBadgeView();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.scanning:
                // 扫一扫
                presenter.requestPermission(getRxPermissions());
//                MainActivity.requestQrCodeScan(getActivity());
                break;
            case R.id.rel_find:
//                ToastUtils.showShort(getString(R.string.after_online));
                Intent intent = new Intent(getActivity(), FeedListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.take_out_order_rl:
            case R.id.acceptor_rl:
            case R.id.recruitment_rl:
            case R.id.didi_taxi_rl:
            case R.id.payment_behalf_business:
                ToastUtils.showShort(getString(R.string.after_online));
                break;
        }
    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {
//            RouterManager.goQrScan();
            getActivity().startActivityForResult(new Intent(getActivity(), QrScanActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);

//            getActivity().startActivityForResult(new Intent(getActivity(), ScanQRCodeActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);
        }
    }
}

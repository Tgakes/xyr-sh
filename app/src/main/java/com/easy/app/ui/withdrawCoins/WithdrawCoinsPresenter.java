package com.easy.app.ui.withdrawCoins;

import androidx.lifecycle.Lifecycle;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.AppPresenter;
import com.easy.app.bean.Balance;
import com.easy.app.helper.RandomHelper;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.net.exception.ApiException;
import com.easy.net.exception.ExceptionEngine;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WithdrawCoinsPresenter extends AppPresenter<WithdrawCoinsView> {

    @Inject
    public WithdrawCoinsPresenter() {

    }


    public void requestBalance() {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "balance");
        parameter.put("orderid", RandomHelper.getOrderId());
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Balance.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.balanceCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.balanceCallback(response);
                        });
    }


    public void requestWithdrawal(String address, String amount, String remarks) {

        Map<String, Object> parameter = getBodyMap();
        parameter.put("business", "usdtinfo");
        parameter.put("busin", "tixian");
        parameter.put("sdizhi", address);
        parameter.put("amount", amount);
        parameter.put("make", remarks);
        RxHttp.post(UrlConstant.API_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.withdrawalCallback(result);
                        },
                        throwable -> {
                            Response<Balance> response = new Response<>();
                            if (throwable != null) {
                                ApiException apiException;
                                if (throwable instanceof ApiException && ((apiException = (ApiException) throwable).getCode() == ExceptionEngine.CONNECT_ERROR || apiException.getCode() == ExceptionEngine.TIME_OUT_ERROR || apiException.getCode() == ExceptionEngine.UNKNOWN_HOST)) {
                                    response.setMsg(App.getInst().getString(R.string.net_exception));
                                } else {
                                    response.setMsg(App.getInst().getString(R.string.request_fail));
                                }
                            }
                            mvpView.withdrawalCallback(response);
                        });
    }


}

package com.easy.app.ui.home.fragment.market;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class TitleViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList;
    String[] titles;


    public TitleViewPagerAdapter(FragmentManager fm, List<Fragment> fragmentList, String[] titles) {
        super(fm);
        this.fragmentList = fragmentList;
        this.titles  = titles;

    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return titles != null ? titles.length : 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles != null && position < titles.length ? titles[position] : "";
    }

}
package com.easy.app.ui.about;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.BuildConfig;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.AboutUsBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.widget.SkinTitleView;

@ActivityInject
@Route(path = RouterManager.ABOUT_US_ACTIVITY, name = "关于我们")
public class AboutUsActivity extends BaseActivity<AboutUsPresenter, AboutUsBinding> implements AboutUsView {


    @Override
    public int getLayoutId() {
        return R.layout.about_us;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.about_us));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }
        viewBind.versionTv.setText(getString(R.string.tip_version_disabled).substring(0, 4) + "：V" + BuildConfig.VERSION_NAME);
    }


}

package com.easy.app.ui.setting;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;

import androidx.core.view.ViewCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.SettingBinding;
import com.easy.app.util.IntentWrapper;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.dialog.GodDialog;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.utils.ToastUtils;
import com.easy.utils.ViewUtils;
import com.easy.widget.PreferenceUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.SkinUtils;

import java.io.File;
import java.util.Locale;

import cn.wildfire.chat.kit.third.utils.FileUtils;

import static com.easy.app.util.IntentWrapper.NO_EXECUTABLE_INTENT;

@ActivityInject
@Route(path = RouterManager.SETTING_ACTIVITY, name = "设置页")
public class SettingActivity extends BaseActivity<SettingPresenter, SettingBinding> implements SettingView, View.OnClickListener {

    private My_BroadcastReceiver mMyBroadcastReceiver = new My_BroadcastReceiver();

    @Override
    public int getLayoutId() {
        return R.layout.setting;
    }

    @Override
    public void initView() {


        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.setting));
            skinTitleView.setBackIcon(R.mipmap.return_icon);
        }

        viewBind.exitBtn.setText(getString(R.string.setting_logout));

        viewBind.cacheText.setText(getString(R.string.clear_cache));

        File file = getExternalFilesDir(null);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        if (file == null) {
            // 不能为空，
            file = new File(getFilesDir(), "external");
        }
        viewBind.clearCacheRl.setTag(file.getAbsolutePath());
        long cacheSize = FileUtils.getFileSize(new File(file.getAbsolutePath()));
        viewBind.cacheTv.setText(FileUtils.formatFileSize(cacheSize));
        viewBind.tvCencelChat.setText(getString(R.string.clean_all_chat_history));
        viewBind.passwoedtv.setText(getString(R.string.change_password));
        viewBind.privacySettingText.setText(getString(R.string.privacy_setting));
        viewBind.aboutUsText.setText(getString(R.string.about_us));
        viewBind.switchLanguageTv.setText(getString(R.string.switch_language));
        viewBind.switchSkinTv.setText(getString(R.string.change_skin));
        viewBind.sbNightMode.setChecked(PreferenceUtils.getBoolean(App.getInst(), PreferenceUtils.DARK_THEME, false));
        viewBind.sbNightMode.setOnCheckedChangeListener((view, isChecked) -> {
            ActivityManager.getInstance().finishOtherActivity(SettingActivity.this);
            PreferenceUtils.putBoolean(App.getInst(), PreferenceUtils.DARK_THEME, isChecked);
            RouterManager.goSplashActivity(SettingActivity.this);
        });
        String lan = Locale.getDefault().getLanguage(); /*LocaleHelper.getLanguage(this)*/
        String language;
        if ("zh".equals(lan)) {
            language = getResources().getString(R.string.simplified_chinese);
        } else if ("en".equals(lan)) {
            language = getResources().getString(R.string.english);
        } else {
            language = getResources().getString(R.string.traditional_chinese);
        }
        viewBind.tvLanguageScan.setText(language);
        SkinUtils.Skin skin = SkinUtils.getSkin(this);
        viewBind.tvSkinScan.setText(skin.getColorName());

        viewBind.tvPayPwdText.setText(getString(presenter.accountsDao.get().isSetPwd() ? R.string.btn_change_pay_password : R.string.btn_set_pay_password));
        viewBind.bindAccountRl.setVisibility(View.GONE);
        viewBind.setPayPasswordRl.setOnClickListener(this);
        viewBind.clearCacheRl.setOnClickListener(this);
        viewBind.rlCencelChat.setOnClickListener(this);
        viewBind.rlBackupChat.setOnClickListener(this);
        viewBind.changePasswordRl.setOnClickListener(this);
        viewBind.switchLanguage.setOnClickListener(this);
        viewBind.skinRl.setOnClickListener(this);
        viewBind.chatFontSizeRl.setOnClickListener(this);
        viewBind.sendGMessageRl.setOnClickListener(this);
        viewBind.privacySetttingRl.setOnClickListener(this);
        viewBind.secureSettingRl.setOnClickListener(this);
        viewBind.aboutUsRl.setOnClickListener(this);
        viewBind.exitBtn.setOnClickListener(this);
        viewBind.tuisongmsg.setOnClickListener(this);
        ViewCompat.setBackgroundTintList(viewBind.exitBtn, ColorStateList.valueOf(SkinUtils.getSkin(this).getAccentColor()));

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NO_EXECUTABLE_INTENT);
        registerReceiver(mMyBroadcastReceiver, intentFilter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMyBroadcastReceiver != null) {
            unregisterReceiver(mMyBroadcastReceiver);
        }
    }


    @Override
    public void onClick(View v) {
        if (ViewUtils.canClick()) {
            switch (v.getId()) {
                case R.id.clear_cache_rl:
                    // 清除缓存

                    new ClearCacheAsyncTask((String) v.getTag()).execute(true);
                    break;
                case R.id.rl_cencel_chat:
                  /*  SelectionFrame selectionFrame = new SelectionFrame(this);
                    selectionFrame.setSomething(null, getString(R.string.is_empty_all_chat), new SelectionFrame.OnSelectionFrameClickListener() {
                        @Override
                        public void cancelClick() {

                        }

                        @Override
                        public void confirmClick() {
                            emptyServerMessage();

                            // 清除所有聊天记录
                            delAllChatRecord();
                        }
                    });
                    selectionFrame.show();*/
                    break;
                case R.id.rl_backup_chat:
//                    BackupHistoryActivity.start(this);
                    break;
                case R.id.change_password_rl:

                    RouterManager.goChangePwdActivity();
                    // 修改密码
//                    startActivity(new Intent(mContext, ChangePasswordActivity.class));
                    break;
                case R.id.set_pay_password_rl://设置支付密码
                    RouterManager.goSetPayPwd(presenter.accountsDao.get().isSetPwd() ? 1 : 0, "", "");
                    break;
                case R.id.switch_language:
                    // 切换语言
//                    startActivity(new Intent(this, SwitchLanguage.class));
                    break;
                case R.id.skin_rl:
                    // 更换皮肤
//                    startActivity(new Intent(this, SkinStore.class));
                    break;
                case R.id.chat_font_size_rl:
                    // 更换聊天字体
//                    startActivity(new Intent(this, FontSizeActivity.class));
                    break;
                case R.id.send_gMessage_rl:
                    // 群发消息
//                    startActivity(new Intent(this, SelectFriendsActivity.class));
                    break;
                case R.id.privacy_settting_rl:
                    // 开启验证
//                    startActivity(new Intent(mContext, PrivacySettingActivity.class));
                    break;
                case R.id.secure_setting_rl:
                    // 安全设置，
//                    startActivity(new Intent(mContext, SecureSettingActivity.class));
                    break;
                case R.id.bind_account_rl:
                    // 绑定第三方
//                    startActivity(new Intent(mContext, BandAccountActivity.class));
                    break;

                case R.id.tuisongmsg:
                    IntentWrapper.whiteListMatters(this, "");
                    break;
                case R.id.about_us_rl:
                    // 关于我们
                    RouterManager.goAboutUsActivity();
                    break;
                case R.id.exit_btn:
                    GodDialog logoutDialog = new GodDialog.Builder(this)
                            .setMessage(getString(R.string.sure_exit_account))
                            .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                dialog.dismiss();
//                                //不要清除session，这样再次登录时能够保留历史记录。如果需要清除掉本地历史记录和服务器信息这里使用true
//                                ChatManagerHolder.gChatManager.disconnect(true, false);
//                                presenter.accountsDao.get().delete();
//                                PreferenceUtils.clearData(App.getInst());
//                                RouterManager.goLoginActivity(this);
                                ((App) App.getInst()).logout(presenter.accountsDao.get(), SettingActivity.this, true);
                            })
                            .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                            .build();
                    logoutDialog.show();
                    break;
            }
        }
    }


    /*private void showSelectSexDialog() {
        String[] sexs = new String[]{getString(R.string.sex_man), getString(R.string.sex_woman)};
        new AlertDialog.Builder(this).setTitle(getString(R.string.gender_selection))
                .setSingleChoiceItems(sexs, mTempData.getSex() == 1 ? 0 : 1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            mTempData.setSex(1);
                            mSexTv.setText(getString(R.string.sex_man));
                        } else {
                            mTempData.setSex(0);
                            mSexTv.setText(getString(R.string.sex_woman));
                        }
                        dialog.dismiss();
                    }
                }).setCancelable(true).create().show();
    }*/


    private class ClearCacheAsyncTask extends AsyncTask<Boolean, String, Integer> {

        private File rootFile;
        private ProgressDialog progressDialog;

        private int filesNumber = 0;
        private boolean canceled = false;
        private long notifyTime = 0;

        public ClearCacheAsyncTask(String filePath) {
            this.rootFile = new File(filePath);
        }

        @Override
        protected void onPreExecute() {
            filesNumber = FileUtils.getFolderSubFilesNumber(rootFile);
            progressDialog = new ProgressDialog(SettingActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.deleteing));
            progressDialog.setMax(filesNumber);
            progressDialog.setProgress(0);
            // 设置取消按钮
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, i) -> canceled = true);
            progressDialog.show();
        }

        /**
         * 返回true代表删除完成，false表示取消了删除
         */
        @Override
        protected Integer doInBackground(Boolean... params) {
            if (filesNumber == 0) {
                return 0;
            }
            // 是否删除已清空的子文件夹
            boolean deleteSubFolder = params[0];
            return deleteFolder(rootFile, true, deleteSubFolder, 0);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            // String filePath = values[0];
            int progress = Integer.parseInt(values[1]);
            // progressDialog.setMessage(filePath);
            progressDialog.setProgress(progress);
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (!canceled && result == filesNumber) {
                ToastUtils.showShort(getString(R.string.clear_completed));
            }
            long cacheSize = FileUtils.getFileSize(rootFile);
            viewBind.cacheTv.setText(FileUtils.formatFileSize(cacheSize));
        }

        /**
         * 是否删除完毕
         *
         * @param file
         * @param deleteSubFolder
         * @return
         */
        private int deleteFolder(File file, boolean rootFolder, boolean deleteSubFolder, int progress) {
            if (file == null || !file.exists() || !file.isDirectory()) {
                return 0;
            }
            File flist[] = file.listFiles();
            for (File subFile : flist) {
                if (canceled) {
                    return progress;
                }
                if (subFile.isFile()) {
                    subFile.delete();
                    progress++;
                    long current = System.currentTimeMillis();
                    if (current - notifyTime > 200) {// 200毫秒更新一次界面
                        notifyTime = current;
                        publishProgress(subFile.getAbsolutePath(), String.valueOf(progress));
                    }
                } else {
                    progress = deleteFolder(subFile, false, deleteSubFolder, progress);
                    if (deleteSubFolder) {
                        subFile.delete();
                    }
                }
            }
            return progress;
        }
    }

    private class My_BroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action)) {
                if (action.equals(NO_EXECUTABLE_INTENT)) {// 无可执行的intent 需提醒用户
                    ToastUtils.showShort(context.getString(R.string.no_executable_intent));
                }
            }
        }
    }
}

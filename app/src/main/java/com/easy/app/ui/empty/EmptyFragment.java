package com.easy.app.ui.empty;



import android.view.View;

import com.easy.app.R;
import com.easy.app.databinding.EmptyBinding;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;


/**
 * A simple {@link EmptyFragment} subclass.
 * Use the {@link EmptyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class EmptyFragment extends BaseFragment<EmptyPresenter,EmptyBinding> implements EmptyView {



    public EmptyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmptyFragment newInstance() {
        EmptyFragment fragment = new EmptyFragment();
        return fragment;
    }




    @Override
    public int getLayoutId() {
        return R.layout.empty;
    }

    @Override
    public void initView(View view) {

    }

}

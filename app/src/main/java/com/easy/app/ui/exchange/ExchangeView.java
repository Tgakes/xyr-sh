package com.easy.app.ui.exchange;


import com.easy.app.bean.Balance;
import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface ExchangeView extends BaseView {

    void balanceCallback(Response<Balance> response);

    void exchangeCallback(Response response);
}

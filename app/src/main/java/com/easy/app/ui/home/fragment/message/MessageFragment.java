package com.easy.app.ui.home.fragment.message;


import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentTransaction;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.databinding.FmMessageBinding;
import com.easy.app.ui.dialog.MessagePopupWindow;
import com.easy.app.ui.home.HomeActivity;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.framework.statusbar.StatusBarUtil;
import com.easy.qrcode.ui.qr_code.QrScanActivity;
import com.easy.utils.DimensUtils;
import com.easy.utils.StreamUtils;
import com.easy.widget.SkinTextView;
import com.easy.widget.SkinTitleView;

import javax.inject.Inject;

import cn.wildfire.chat.kit.contact.newfriend.SearchUserActivity;
import cn.wildfire.chat.kit.conversation.CreateConversationActivity;
import cn.wildfire.chat.kit.conversationlist.ConversationListFragment;
import cn.wildfire.chat.kit.qrcode.ScanQRCodeActivity;


/**
 * A simple {@link MessageFragment} subclass.
 * Use the {@link MessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class MessageFragment extends BaseFragment<MessagePresenter, FmMessageBinding> implements MessageView, View.OnClickListener {


    private MessagePopupWindow mMessagePopupWindow;

    public MessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessageFragment newInstance() {
        MessageFragment fragment = new MessageFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fm_message;
    }

    @Override
    public void initView(View view) {

        int statusHeight = StatusBarUtil.getStatusBarHeight(getActivity());
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewBind.clTitleBar.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        viewBind.clTitleBar.setLayoutParams(layoutParams);

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setBackground(R.color.normal_bg);
            SkinTextView skinTextView = skinTitleView.setTitleText(getString(R.string.app_name_zh));
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) skinTextView.getLayoutParams();
            params.leftMargin = DimensUtils.dp2px(App.getInst(), 15f);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            skinTextView.setLayoutParams(params);
            skinTitleView.setRightImage(StreamUtils.getInstance().resourceToDrawable(R.drawable.ic_add, App.getInst()));

            skinTitleView.hideBackBtn();
            skinTitleView.setRightClickListener(v -> {
                mMessagePopupWindow = new MessagePopupWindow(getActivity(), MessageFragment.this, 1);
                mMessagePopupWindow.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                mMessagePopupWindow.showAsDropDown(v, -(mMessagePopupWindow.getContentView().getMeasuredWidth() - v.getWidth() / 2 - 40),
                        0);
            });
        }

        ConversationListFragment fragment = new ConversationListFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.clMsgLayout, fragment).commit();


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_public_number:
                // 搜索公众号
                mMessagePopupWindow.dismiss();
//                PublicNumberSearchActivity.start(requireContext());
                break;
            case R.id.create_group:
            case R.id.face_group:
                // 发起群聊
                mMessagePopupWindow.dismiss();
                startActivity(new Intent(getActivity(), CreateConversationActivity.class));
                break;
            // 面对面建群
//                startActivity(new Intent(getActivity(), FaceToFaceGroup.class));
            case R.id.add_friends:
                // 添加朋友
                mMessagePopupWindow.dismiss();
                startActivity(new Intent(getActivity(), SearchUserActivity.class));
                break;
            case R.id.scanning:
                // 扫一扫
                mMessagePopupWindow.dismiss();
                presenter.requestPermission(getRxPermissions());
//                MainActivity.requestQrCodeScan(getActivity());
                break;
        }
    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (granted != null && granted) {
            getActivity().startActivityForResult(new Intent(getActivity(), QrScanActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);

//            getActivity().startActivityForResult(new Intent(getActivity(), ScanQRCodeActivity.class), HomeActivity.REQUEST_CODE_SCAN_QR_CODE);
        }
    }
}

package com.easy.app.ui.home.fragment.market.history;


import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.easy.app.R;
import com.easy.app.bean.HistoryOrderList;
import com.easy.app.databinding.ListCommomBinding;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.BaseFragment;
import com.easy.net.beans.Response;
import com.easy.utils.ToastUtils;
import com.easy.widget.StateView;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link HistoryFragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@FragmentInject
public class HistoryFragment extends BaseFragment<HistoryPresenter, ListCommomBinding> implements HistoryView, OnRefreshListener, View.OnClickListener, BaseQuickAdapter.OnItemClickListener {


    HistoryAdapter adapter;


    int page = 1;

    List<HistoryOrderList> historyOrderLists;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }


    @Override
    public int getLayoutId() {
        return R.layout.list_commom;
    }

    @Override
    public void initView(View view) {


        historyOrderLists = new ArrayList<>();
        adapter = new HistoryAdapter(this, R.layout.adapter_market2, historyOrderLists);
        viewBind.recyclerView.setAdapter(adapter);

//        adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        viewBind.recyclerView.setHasFixedSize(true);
        StateView emptyView = new StateView(getActivity());
        emptyView.setOnClickListener(this);
        adapter.setEmptyView(emptyView);

        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(false);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void lazyView(View view) {
        presenter.requestList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContainer:
                Logger.i("====onClick llContainer 请求====");
                showLoading();
                presenter.requestList();
                break;
        }
    }


    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        presenter.requestList();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        Log.i("logs", "position===" + position);
    }

    @Override
    public void historyListCallback(Response<List> response) {
        hideLoading();
        if (response.isSuccess()) {
            viewBind.swRefresh.finishRefresh(true);
            List<HistoryOrderList> historyOrderLists = response.getResultObj();
            this.historyOrderLists.clear();
            if (historyOrderLists != null && !historyOrderLists.isEmpty()) {
                this.historyOrderLists.addAll(historyOrderLists);
            }
            adapter.notifyDataSetChanged();
        } else {
            viewBind.swRefresh.finishRefresh();
            ToastUtils.showShort(response.getMsg());
        }

    }






}

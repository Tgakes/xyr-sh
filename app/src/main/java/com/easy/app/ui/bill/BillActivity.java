package com.easy.app.ui.bill;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.BillBinding;
import com.easy.app.databinding.EmptyBinding;
import com.easy.app.ui.empty.EmptyPresenter;
import com.easy.app.ui.empty.EmptyView;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.ui.RecycleViewDivider;
import com.easy.utils.DimensUtils;
import com.easy.utils.StreamUtils;
import com.easy.widget.SkinTitleView;
import com.easy.widget.StateView;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

@ActivityInject
@Route(path = RouterManager.BILL, name = "账单")
public class BillActivity extends BaseActivity<BillPresenter, BillBinding> implements BillView, OnRefreshListener, OnLoadMoreListener, View.OnClickListener {


    BillAdapter adapter;


    int page = 1;

    @Override
    public int getLayoutId() {
        return R.layout.bill;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.bill));
        }

        adapter = new BillAdapter(R.layout.adapter_bill, presenter.getTestList());
        viewBind.recyclerView.setAdapter(adapter);
//        adapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewBind.recyclerView.addItemDecoration(new RecycleViewDivider(App.getInst(), LinearLayoutManager.VERTICAL, DimensUtils.dp2px(App.getInst(),0.5f), StreamUtils.getInstance().resourceToColor(R.color.divider, App.getInst())));

        viewBind.recyclerView.setHasFixedSize(true);
        StateView emptyView = new StateView(this);
        emptyView.setOnClickListener(this);
        adapter.setEmptyView(emptyView);

        //是否启用下拉刷新（默认启用）
        viewBind.swRefresh.setEnableRefresh(true);
//      adapter.setOnLoadMoreListener(this, viewBind.recyclerView);
        //设置是否启用上拉加载更多（默认启用）
        viewBind.swRefresh.setEnableLoadMore(true);
        //内容不满一页时不能开启上拉加载功能
        viewBind.swRefresh.setEnableLoadMoreWhenContentNotFull(false);
        viewBind.swRefresh.setOnRefreshListener(this);
        viewBind.swRefresh.setOnLoadMoreListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContainer:
                Logger.i("====onClick llContainer 请求====");
                showLoading();
//                presenter.requestChannelsList(id, token);
                break;
        }
    }


    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

    }
}

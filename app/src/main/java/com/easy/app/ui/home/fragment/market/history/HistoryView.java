package com.easy.app.ui.home.fragment.market.history;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

import java.util.List;

public interface HistoryView extends BaseView {

    void historyListCallback(Response<List> response);

}

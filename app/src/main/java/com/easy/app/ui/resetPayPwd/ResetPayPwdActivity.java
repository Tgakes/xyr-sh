package com.easy.app.ui.resetPayPwd;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.EmptyBinding;
import com.easy.app.databinding.ResetPayPwdBinding;
import com.easy.app.ui.empty.EmptyPresenter;
import com.easy.app.ui.empty.EmptyView;
import com.easy.app.util.ButtonColorChange;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.widget.SkinTitleView;

@ActivityInject
@Route(path = RouterManager.RESET_PAY_PWD, name = "重置支付密码")
public class ResetPayPwdActivity extends BaseActivity<ResetPayPwdPresenter, ResetPayPwdBinding> implements ResetPayPwdView {


    @Override
    public int getLayoutId() {
        return R.layout.reset_pay_pwd;
    }

    @Override
    public void initView() {

        SkinTitleView skinTitleView = addTitleView();
        if (skinTitleView != null) {
            skinTitleView.setTitleText(getString(R.string.reset_pay_password));
        }
        ButtonColorChange.colorChange(this, viewBind.sendAgainBtn);
        ButtonColorChange.colorChange(this, viewBind.changeBtn);

    }


}

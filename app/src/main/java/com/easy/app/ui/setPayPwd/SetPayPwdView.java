package com.easy.app.ui.setPayPwd;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

public interface SetPayPwdView extends BaseView {


    void setPayPwdCallback(Response<String> response);

}

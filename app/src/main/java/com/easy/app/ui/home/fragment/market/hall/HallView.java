package com.easy.app.ui.home.fragment.market.hall;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;

import java.util.List;

public interface HallView extends BaseView {

    void marketListCallback(Response<List> response);

}

/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.easy.utils.EncryptUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.wildfire.chat.kit.conversation.ConversationActivity;
import cn.wildfirechat.message.Message;
import cn.wildfirechat.message.TextMessageContent;
import cn.wildfirechat.message.core.MessageContentType;
import cn.wildfirechat.message.core.MessageDirection;
import cn.wildfirechat.message.notification.RecallMessageContent;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.model.ConversationInfo;
import cn.wildfirechat.model.GroupInfo;
import cn.wildfirechat.remote.ChatManager;
import me.leolin.shortcutbadger.ShortcutBadger;

import static androidx.core.app.NotificationCompat.CATEGORY_MESSAGE;
import static androidx.core.app.NotificationCompat.DEFAULT_ALL;
import static cn.wildfirechat.message.core.PersistFlag.Persist_And_Count;
import static cn.wildfirechat.model.Conversation.ConversationType.Single;

public class WfcNotificationManager {
    private WfcNotificationManager() {

    }

    private static WfcNotificationManager notificationManager;


    public static final String CHANNEL_ID = "sh_notification";

    public synchronized static WfcNotificationManager getInstance() {
        if (notificationManager == null) {
            notificationManager = new WfcNotificationManager();
        }
        return notificationManager;
    }

    public void clearAllNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        notificationConversations.clear();
    }


    private void showNotification(Context context, String tag, int id, String title, String content, PendingIntent pendingIntent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.sh_chat_message),
                    NotificationManager.IMPORTANCE_HIGH);

            channel.enableLights(true); //是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN); //小红点颜色
            channel.setShowBadge(true); //是否在久按桌面图标时显示此渠道的通知
            notificationManager.createNotificationChannel(channel);
        }


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_logo)
                .setAutoCancel(true)
                .setCategory(CATEGORY_MESSAGE)
                .setDefaults(DEFAULT_ALL);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(title);
        builder.setContentText(content);
        Notification n = builder.build();
//        ShortcutBadger.applyNotification(context, n, unreadCount + 1);
        notificationManager.notify(tag, id, n);
    }

    public void handleRecallMessage(Context context, Message message) {
        handleReceiveMessage(context, Collections.singletonList(message));
    }

    public void handleReceiveMessage(Context context, List<Message> messages) {

        if (messages == null || messages.isEmpty()) {
            return;
        }

        if (ChatManager.Instance().isGlobalSilent()) {
            return;
        }

        boolean hiddenNotificationDetail = ChatManager.Instance().isHiddenNotificationDetail();

        for (Message message : messages) {
            if (message.direction == MessageDirection.Send || (message.content.getPersistFlag() != Persist_And_Count && !(message.content instanceof RecallMessageContent))) {
                continue;
            }
            ConversationInfo conversationInfo = ChatManager.Instance().getConversation(message.conversation);
            if (conversationInfo.isSilent) {
                continue;
            }

            /*if (message.content.getMessageContentType() == MessageContentType.ContentType_Text) {
                TextMessageContent textMessageContent = (TextMessageContent) message.content;
                textMessageContent.setContent(EncryptUtils.decryptText(textMessageContent.getContent()));
                message.content = textMessageContent;
            }*/

            String pushContent = hiddenNotificationDetail ? context.getString(R.string.new_msg) : message.content.pushContent;
            if (TextUtils.isEmpty(pushContent)) {
                pushContent = message.content.digest(message);
            }

            int unreadCount = ChatManager.Instance().getUnreadCount(message.conversation).unread;
            if (unreadCount > 1) {
                pushContent = context.getString(R.string.msg_count, String.valueOf(unreadCount)) + pushContent;
            }

            String title = "";
            if (message.conversation.type == Single) {
                String name = ChatManager.Instance().getUserDisplayName(message.conversation.target);
                title = TextUtils.isEmpty(name) ? context.getString(R.string.new_msg) : name;
            } else if (message.conversation.type == Conversation.ConversationType.Group) {
                GroupInfo groupInfo = ChatManager.Instance().getGroupInfo(message.conversation.target, false);
                title = groupInfo == null ? context.getString(R.string.group_chat) : groupInfo.name;
            } else {
                title = context.getString(R.string.new_msg);
            }

            Intent mainIntent = null;
            try {
                mainIntent = new Intent(context, Class.forName("com.easy.app.ui.home.HomeActivity"));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            ShortcutBadger.applyCount(context, unreadCount);
            Intent conversationIntent = new Intent(context, ConversationActivity.class);
            conversationIntent.putExtra("conversation", message.conversation);
            PendingIntent pendingIntent = PendingIntent.getActivities(context, notificationId(message.conversation), mainIntent != null ? new Intent[]{mainIntent, conversationIntent} : new Intent[]{conversationIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
            String tag = "sh notification tag";
            showNotification(context, tag, notificationId(message.conversation), title, pushContent, pendingIntent);
        }
    }

    private List<Conversation> notificationConversations = new ArrayList<>();

    private int notificationId(Conversation conversation) {
        if (!notificationConversations.contains(conversation)) {
            notificationConversations.add(conversation);
        }
        return notificationConversations.indexOf(conversation);
    }
}

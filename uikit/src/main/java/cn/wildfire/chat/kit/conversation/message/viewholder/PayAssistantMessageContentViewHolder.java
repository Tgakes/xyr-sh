/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.conversation.message.viewholder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.easy.utils.DateUtils;
import com.easy.utils.DimensUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.R2;
import cn.wildfire.chat.kit.annotation.MessageContentType;
import cn.wildfire.chat.kit.conversation.ConversationFragment;
import cn.wildfire.chat.kit.conversation.message.model.UiMessage;
import cn.wildfirechat.message.PayAssistantMessageContent;

@MessageContentType(value = {
    PayAssistantMessageContent.class,
})
public class PayAssistantMessageContentViewHolder extends NormalMessageContentViewHolder {


    @BindView(R2.id.tvPayStyle)
    TextView tvPayStyle;
    @BindView(R2.id.tvPayAmountType)
    TextView tvPayAmountType;
    @BindView(R2.id.tvPayAmount)
    TextView tvPayAmount;


    @BindView(R2.id.tvPayRemarks)
    TextView tvPayRemarks;
    @BindView(R2.id.tvPayTimeText)
    TextView tvPayTimeText;
    @BindView(R2.id.tvPayTime)
    TextView tvPayTime;

    @BindView(R2.id.ll_pay_item)
    LinearLayout llPayItem;
    PayAssistantMessageContent payAssistantMessageContent;


    public PayAssistantMessageContentViewHolder(ConversationFragment fragment, RecyclerView.Adapter adapter, View itemView) {
        super(fragment, adapter, itemView);
        portraitImageView.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = llPayItem.getLayoutParams();
        params.width = DimensUtils.getScreenWidth(fragment.getContext()) - DimensUtils.dp2px(fragment.getContext(), 30f);
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        llPayItem.setLayoutParams(params);
    }


    @Override
    protected void onBind(UiMessage message) {

        Context context = tvPayStyle.getContext();
        payAssistantMessageContent = (PayAssistantMessageContent) message.message.content;
        boolean isReceiptMoney = payAssistantMessageContent.getType() == 2;//到账
        boolean isSh = payAssistantMessageContent.getCurrency() == 1;
        boolean isScan = payAssistantMessageContent.getWay() == 1;//扫码
        String currency = (isSh ? "SH" : "USDT");
        String style = context.getString(isScan ? R.string.pay_style_2 : R.string.pay_style_1);
        tvPayStyle.setText(isReceiptMoney ? context.getString(R.string.receipt_amount_notice) : context.getString(R.string.pay_notice));
        tvPayAmountType.setText(isReceiptMoney ? context.getString(R.string.receipt_amount) : context.getString(R.string.pay_amount));


        tvPayAmount.setText(payAssistantMessageContent.getAmount() + " " + currency);

        tvPayRemarks.setText(isReceiptMoney ? context.getString(R.string.remarks_account, style) : context.getString(R.string.remarks_pay, style));
        tvPayTimeText.setText(context.getString(isReceiptMoney ? R.string.payment_date : R.string.trading_hours));
        tvPayTime.setText(DateUtils.getDateToString(message.message.serverTime, 4));
        //titleTextView.setText(payMessageContent.getTitle());
//        descTextView.setText(payMessageContent.getDesc());
//        UserInfo userInfo = ChatManager.Instance().getUserInfo(payMessageContent.getHost(), false);

    }

    @OnClick(R2.id.ll_pay_item)
    void onItemPay() {
        Log.i("logs","==========onItemPay=============");

    }

    //过滤转发item
    @Override
    public boolean contextMenuItemFilter(UiMessage uiMessage, String tag) {
        if (MessageContextMenuItemTags.TAG_FORWARD.equals(tag)) {
            return true;
        } else {
            return super.contextMenuItemFilter(uiMessage, tag);
        }
    }
}

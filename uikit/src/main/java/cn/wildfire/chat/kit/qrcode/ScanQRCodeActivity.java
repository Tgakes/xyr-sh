/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.qrcode;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.easy.utils.DateUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.LunarCalendar;
import com.easy.utils.StreamUtils;
import com.lqr.imagepicker.ImagePicker;
import com.lqr.imagepicker.bean.ImageItem;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;

import butterknife.BindView;
import cn.wildfire.chat.kit.WfcBaseActivity;
import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.R2;

public class ScanQRCodeActivity extends WfcBaseActivity implements ViewTreeObserver.OnGlobalLayoutListener{

   /* private CaptureHelper mCaptureHelper;
    @BindView(R2.id.surfaceView)
    SurfaceView surfaceView;
    @BindView(R2.id.viewfinderView)
    ViewfinderView viewfinderView;*/

    private static final int REQUEST_CODE_IMAGE = 100;

    @Override
    protected int contentLayout() {
        return R.layout.scan_qrcode_activity;
    }

    @Override
    protected void afterViews() {
        super.afterViews();
//        surfaceView.getViewTreeObserver().addOnGlobalLayoutListener(this);

    }


    @Override
    public void onGlobalLayout() {
  /*      mCaptureHelper = new CaptureHelper(this, surfaceView, viewfinderView);
        mCaptureHelper.onCreate();
        mCaptureHelper.vibrate(true)
                .fullScreenScan(true)//全屏扫码
                .supportVerticalCode(false)//支持扫垂直条码，建议有此需求时才使用。
                .continuousScan(false);
        onResume();
        surfaceView.getViewTreeObserver().removeOnGlobalLayoutListener(this);*/
    }

    @Override
    protected int menu() {
        return R.menu.qrcode;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.gallery) {
            scanGalleryQRCode();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void afterMenus(Menu menu) {
        super.afterMenus(menu);

        MenuItem menuItem = menu.findItem(R.id.gallery);
        int mainColor = StreamUtils.getInstance().resourceToColor(isDarkTheme() ? R.color.main_text_color : R.color.white, getApplicationContext());
        SpannableString spannableString = new SpannableString(menuItem.getTitle());
        spannableString.setSpan(new ForegroundColorSpan(mainColor), 0, spannableString.length(), 0);
        menuItem.setTitle(spannableString);
    }

    private void scanGalleryQRCode() {
        Intent intent = ImagePicker.picker().showCamera(false).buildPickIntent(this);
        startActivityForResult(intent, REQUEST_CODE_IMAGE);
    }

  /*  @Override
    protected void onResume() {
        super.onResume();
        if (mCaptureHelper != null) {
            mCaptureHelper.onResume();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCaptureHelper != null) {
            mCaptureHelper.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCaptureHelper != null) {
            mCaptureHelper.onDestroy();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mCaptureHelper != null) {
            mCaptureHelper.onTouchEvent(event);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_IMAGE && resultCode == Activity.RESULT_OK) {

            ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null && images.size() > 0) {
                String path = images.get(0).path;
//                InputStream is = null;

                new Thread(() -> ScanQRCodeActivity.this.runOnUiThread(() -> {
                    Result result = QRCodeHelper.decodeQR(path);
                    Intent intent = new Intent();
                    if (result == null) {
                        Toast.makeText(this, "识别二维码失败", Toast.LENGTH_SHORT).show();
                    } else {

                        intent.putExtra(Intents.Scan.RESULT, result.getText());
                        setResult(Activity.RESULT_OK, intent);
                    }
                    finish();
                })).start();


            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/
}

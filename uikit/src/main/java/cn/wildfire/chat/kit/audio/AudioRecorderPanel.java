/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.audio;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easy.utils.DimensUtils;
import com.haozhang.lib.AnimatedRecordingView;

import java.io.File;

import cn.wildfire.chat.kit.Config;
import cn.wildfire.chat.kit.R;

public class AudioRecorderPanel implements View.OnTouchListener {
    private int maxDuration = Config.DEFAULT_MAX_AUDIO_RECORD_TIME_SECOND * 1000;
    private int minDuration = 1 * 1000;
    private int countDown = 10 * 1000;
    private boolean isRecording;
    private long startTime;
    private boolean isToCancel;
    private String currentAudioFile;

    private Context context;
    private View rootView;
    private Button button;
    private AudioRecorder recorder;
    private OnRecordListener recordListener;
    private Handler handler;

    private TextView countDownTextView;
    private TextView stateTextView;
    //    private ImageView stateImageView;
    private AnimatedRecordingView animatedRecordingView;
    private PopupWindow recordingWindow;

    public AudioRecorderPanel(Context context) {
        this.context = context;
    }

    /**
     * @param maxDuration 最长录音时间，单位：秒
     */
    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration * 1000;
    }

    /**
     * @param minDuration 最短录音时间，单位：秒
     */
    public void setMinDuration(int minDuration) {
        this.minDuration = minDuration * 1000;
    }

    /**
     * @param countDown 录音剩余多少秒时开始倒计时，单位：秒
     */
    public void setCountDown(int countDown) {
        this.countDown = countDown * 1000;
    }

    /**
     * 将{@link AudioRecorderPanel}附加到button上面
     *
     * @param rootView 录音界面显示的rootView
     * @param button   长按触发录音的按钮
     */
    public void attach(View rootView, Button button) {
        this.rootView = rootView;
        this.button = button;
        this.button.setOnTouchListener(this);
    }

    public void deattch() {
        rootView = null;
        button = null;
    }

    public void setRecordListener(OnRecordListener recordListener) {
        this.recordListener = recordListener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (button == null) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                button.setBackgroundResource(R.drawable.shape_session_btn_voice_pressed);
                startRecord();
                break;
            case MotionEvent.ACTION_MOVE:
                isToCancel = isCancelled(v, event);
                if (isToCancel) {
                    if (recordListener != null) {
                        recordListener.onRecordStateChanged(RecordState.TO_CANCEL);
                    }
                    showCancelTip();
                } else {
                    hideCancelTip();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                button.setBackgroundResource(R.drawable.shape_session_btn_voice_normal);
                if (isToCancel) {
                    cancelRecord();
                } else if (isRecording) {
                    stopRecord();
                }
                break;
            default:
                break;
        }
        return true;
    }

    private void startRecord() {
        // FIXME: 2018/10/10 权限是否有权限，没权限提示错误，并返回
        isRecording = true;
        if (recorder == null) {
            recorder = new AudioRecorder(context);
            handler = new Handler();
        } else {
            handler.removeCallbacks(this::hideRecording);
        }
        currentAudioFile = genAudioFile();
        recorder.startRecord(currentAudioFile);
        if (recordListener != null) {
            recordListener.onRecordStateChanged(RecordState.START);
        }

        startTime = System.currentTimeMillis();
        showRecording();
        tick();
        updateVolumeTick();
    }

    public boolean isShowingRecorder() {
        return recordingWindow != null;
    }

    private void stopRecord() {
        if (!isRecording) {
            return;
        }
        if (recorder != null) {
            recorder.stopRecord();
        }
        if (recordListener != null) {
            long duration = System.currentTimeMillis() - startTime;
            if (duration > minDuration) {
                recordListener.onRecordSuccess(currentAudioFile, (int) duration / 1000);
                hideRecording();
            } else {
                recordListener.onRecordFail("语音时间太短");
                showTooShortTip();
                handler.postDelayed(this::hideRecording, 1000);
            }
        } else {
            hideRecording();
        }

        isToCancel = false;
        isRecording = false;

        recorder = null;
        handler = null;
    }

    private void cancelRecord() {
        if (recorder != null) {
            recorder.stopRecord();
        }
        if (recordListener != null) {
            recordListener.onRecordFail("用户取消发送语音");
        }
        hideRecording();

        isToCancel = false;
        isRecording = false;
    }

    private void showRecording() {
        if (recordingWindow == null) {
            View view = View.inflate(context, R.layout.audio_popup_wi_vo, null);
            animatedRecordingView = view.findViewById(R.id.voiceWaveView);
            stateTextView = view.findViewById(R.id.rc_audio_state_text);
            countDownTextView = view.findViewById(R.id.rc_audio_timer);
            recordingWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            recordingWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            recordingWindow.setFocusable(false);
            recordingWindow.setOutsideTouchable(false);
            recordingWindow.setTouchable(true);

           FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) animatedRecordingView.getLayoutParams();
           params.width = DimensUtils.dp2px(context,280f);
           params.height = DimensUtils.dp2px(context,60f);
           animatedRecordingView.setLayoutParams(params);

            recordingWindow.setHeight(DimensUtils.getScreenHeight(context) + getStatusBarHeight(context));
            recordingWindow.setClippingEnabled(false);// sdk > 21 解决 标题栏没有办法遮罩的问题
            recordingWindow.showAtLocation(rootView, Gravity.TOP, 0, 0);
            animatedRecordingView.start();
            showAnim(1.0f);
        }



        animatedRecordingView.setVisibility(View.VISIBLE);
//        stateImageView.setImageResource(R.mipmap.ic_volume_1);
        stateTextView.setVisibility(View.VISIBLE);
        stateTextView.setText(R.string.voice_rec);
        stateTextView.setBackgroundResource(R.drawable.corner_voice_style_n);
        countDownTextView.setVisibility(View.INVISIBLE);
    }

    private void showAnim(float grade) {
        Log.i("logs","grade==="+grade);
        animatedRecordingView.setVolume(grade);

    }

    //获取状态栏高度
    public int getStatusBarHeight(Context context) {
        if (context == null) {
            return 0;
        }
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void hideRecording() {
        if (recordingWindow == null) {
            return;
        }
        animatedRecordingView.setVisibility(View.GONE);
        animatedRecordingView.stop();
        recordingWindow.dismiss();
        recordingWindow = null;
        animatedRecordingView = null;
        stateTextView = null;
        countDownTextView = null;
    }

    private void showTooShortTip() {
        showAnim(1.0f);
        stateTextView.setText(R.string.voice_short);
    }

    private void showCancelTip() {
        countDownTextView.setVisibility(View.INVISIBLE);
        animatedRecordingView.setVisibility(View.VISIBLE);
        showAnim(1.0f);
        stateTextView.setVisibility(View.VISIBLE);
        stateTextView.setText(R.string.voice_cancel);
        stateTextView.setBackgroundResource(R.drawable.corner_voice_style);
    }

    private void hideCancelTip() {
        if (countDownTextView != null && countDownTextView.getVisibility() == View.INVISIBLE) {
            showRecording();
        }
    }

    /**
     * @param seconds
     */
    private void showCountDown(int seconds) {
        animatedRecordingView.setVisibility(View.GONE);
        stateTextView.setVisibility(View.VISIBLE);
        stateTextView.setText(R.string.voice_rec);
        stateTextView.setBackgroundResource(R.drawable.corner_voice_style_n);
        countDownTextView.setText(String.format("%s", seconds));
        countDownTextView.setVisibility(View.VISIBLE);
    }

    private void timeout() {
        stopRecord();
    }

    private void updateVolumeTick() {
        if (isRecording) {
            updateVolume();
            handler.postDelayed(this::updateVolumeTick, 15);
        }
    }

    private void tick() {
        if (isRecording) {
            long now = System.currentTimeMillis();
            if (now - startTime > maxDuration) {
                timeout();
                return;
            } else if (now - startTime > (maxDuration - countDown)) {
                int tmp = (int) ((maxDuration - (now - startTime)) / 1000);
                tmp = tmp > 1 ? tmp : 1;
                showCountDown(tmp);
                if (recordListener != null) {
                    recordListener.onRecordStateChanged(RecordState.TO_TIMEOUT);
                }
            }
            handler.postDelayed(this::tick, 100);
        }
    }

    private void updateVolume() {
        if (isToCancel || recorder == null || animatedRecordingView == null) {
            return;
        }
        // refer to https://www.cnblogs.com/lqminn/archive/2012/10/10/2717904.html
//        Log.i("logs","recorder.getMaxAmplitude()===="+recorder.getMaxAmplitude());
//        int db = 8 * recorder.getMaxAmplitude() / 32768;

        double ratio = (double) recorder.getMaxAmplitude() / 1;
        double db = 0;// 分贝
        if (ratio > 1) {
            db = 20 * Math.log10(ratio);
        }
        showAnim((float) db);

    }

    private String genAudioFile() {
        File dir = new File(context.getFilesDir(), "audio");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(dir, System.currentTimeMillis() + "");
        return file.getAbsolutePath();
    }

    private boolean isCancelled(View view, MotionEvent event) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);

        if (event.getRawX() < location[0] || event.getRawX() > location[0] + view.getWidth()
                || event.getRawY() < location[1] - 40) {
            return true;
        }

        return false;
    }

    public interface OnRecordListener {
        void onRecordSuccess(String audioFile, int duration);

        void onRecordFail(String reason);

        void onRecordStateChanged(RecordState state);
    }

    public enum RecordState {
        // 开始录音
        START,
        // 录音中
        RECORDING,
        // 用户准备取消
        TO_CANCEL,
        // 最长录音时间开到
        TO_TIMEOUT,
    }
}

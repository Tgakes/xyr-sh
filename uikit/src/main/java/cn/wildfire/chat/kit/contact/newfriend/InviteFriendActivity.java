/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.contact.newfriend;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.easy.utils.StreamUtils;
import com.easy.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wildfire.chat.kit.WfcBaseActivity;
import cn.wildfire.chat.kit.contact.ContactViewModel;
import cn.wildfire.chat.kit.user.UserViewModel;
import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.R2;
import cn.wildfirechat.model.UserInfo;

public class InviteFriendActivity extends WfcBaseActivity {
    @BindView(R2.id.introTextView)
    TextView introTextView;

    private UserInfo userInfo;

    @Override
    protected void afterViews() {
        super.afterViews();
        userInfo = getIntent().getParcelableExtra("userInfo");
        if (userInfo == null) {
            finish();
        }
        UserViewModel userViewModel =ViewModelProviders.of(this).get(UserViewModel.class);
        UserInfo me = userViewModel.getUserInfo(userViewModel.getUserId(), false);
        introTextView.setText("我是 " + (me == null ? "" : me.displayName));
    }

    @Override
    protected int contentLayout() {
        return R.layout.contact_invite_activity;
    }

    @Override
    protected int menu() {
        return R.menu.contact_invite;
    }

    @Override
    protected void afterMenus(Menu menu) {
        super.afterMenus(menu);
        MenuItem menuItem = menu.findItem(R.id.confirm);
        int mainColor = StreamUtils.getInstance().resourceToColor(isDarkTheme() ? R.color.main_text_color : R.color.white, getApplicationContext());
        SpannableString spannableString = new SpannableString(menuItem.getTitle());
        spannableString.setSpan(new ForegroundColorSpan(mainColor), 0, spannableString.length(), 0);
        menuItem.setTitle(spannableString);
    }

    @OnClick(R2.id.clearImageButton)
    void clear() {
        introTextView.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.confirm) {
            invite();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void invite() {
        ContactViewModel contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        contactViewModel.invite(userInfo.uid, introTextView.getText().toString())
                .observe(this, resultCode -> {
                    if (resultCode == 0) {
                        ToastUtils.showShort(getString(R.string.add_friend_result_1));
                        finish();
                    } else if (resultCode == 16) {
                        ToastUtils.showShort(getString(R.string.add_friend_result_2));
                    } else if (resultCode == 18) {
                        ToastUtils.showShort(getString(R.string.add_friend_result_3));
                    } else if (resultCode == 23) {
                        ToastUtils.showShort(getString(R.string.add_friend_result_4));
                    } else {
                        ToastUtils.showShort(getString(R.string.add_friend_result_5));
                    }
                });
    }
}

/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.conversation.message.viewholder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.easy.utils.EmptyUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.R2;
import cn.wildfire.chat.kit.annotation.EnableContextMenu;
import cn.wildfire.chat.kit.annotation.MessageContentType;
import cn.wildfire.chat.kit.conversation.ConversationFragment;
import cn.wildfire.chat.kit.conversation.message.model.UiMessage;
import cn.wildfirechat.message.LocationMessageContent;
import cn.wildfirechat.message.TransferMoneyMessageContent;
import cn.wildfirechat.message.core.MessageDirection;


@MessageContentType(TransferMoneyMessageContent.class)
@EnableContextMenu
public class TransferMoneyMessageContentViewHolder extends NormalMessageContentViewHolder {


    @BindView(R2.id.iv_image)
    ImageView ivImage;
    @BindView(R2.id.chat_text_money)
    TextView tvChatTextMoney;
    @BindView(R2.id.chat_text_desc)
    TextView tvChatTextDesc;


    public TransferMoneyMessageContentViewHolder(ConversationFragment fragment, RecyclerView.Adapter adapter, View itemView) {
        super(fragment, adapter, itemView);
    }


    @Override
    protected void onBind(UiMessage message) {

        TransferMoneyMessageContent payAssistantMessageContent = (TransferMoneyMessageContent) message.message.content;

        Context context = ivImage.getContext();
        if (EmptyUtils.isNotEmpty(payAssistantMessageContent.getDes())) {
            tvChatTextDesc.setText(payAssistantMessageContent.getDes());
        } else {
            if (message.message.direction == MessageDirection.Receive) {//
                tvChatTextDesc.setText(context.getString(R.string.already_receipt_money));
            } else {
                tvChatTextDesc.setText(context.getString(R.string.already_get_money));
            }
        }
        boolean isSh = payAssistantMessageContent.getCurrency() == 1;
        String currency = (isSh ? "SH" : "USDT");
        tvChatTextMoney.setText(payAssistantMessageContent.getAmount() + " " + currency);

    }

    @OnClick(R2.id.chat_warp_view)
    void onItemPay() {
        Log.i("logs", "==========onItemTransferAccount=============");

    }

    //过滤转发item
    @Override
    public boolean contextMenuItemFilter(UiMessage uiMessage, String tag) {
        if (MessageContextMenuItemTags.TAG_FORWARD.equals(tag)) {
            return true;
        } else {
            return super.contextMenuItemFilter(uiMessage, tag);
        }
    }

}

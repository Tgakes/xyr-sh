/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.conversation.ext;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.jeremyliao.liveeventbus.LiveEventBus;

import org.json.JSONException;
import org.json.JSONObject;

import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.annotation.ExtContextMenuItem;
import cn.wildfire.chat.kit.conversation.ext.core.ConversationExt;
import cn.wildfire.chat.kit.conversation.ext.core.ConversationExtension;
import cn.wildfire.chat.kit.viewmodel.MessageViewModel;
import cn.wildfirechat.message.TransferMoneyMessageContent;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.model.UserInfo;
import cn.wildfirechat.remote.ChatManager;

/**
 * 转账
 */
public class TransferMoneyExt extends ConversationExt {


    @Override
    protected void onBind(Fragment fragment, MessageViewModel messageViewModel, Conversation conversation, ConversationExtension conversationExtension, int index) {
        super.onBind(fragment, messageViewModel, conversation, conversationExtension, index);
        LiveEventBus.get("transMoney", String.class).observe(fragment.getActivity(), this::transMoney);
    }

    private void transMoney(String data) {
        Log.i("logs", "data====" + data);

        sendTransferMoney(data);
    }

    /**
     * @param containerView 扩展view的container
     * @param conversation
     */
    @ExtContextMenuItem
    public void pickContact(View containerView, Conversation conversation) {

        if (conversation.type == Conversation.ConversationType.Single) {
            UserInfo targetUser = ChatManager.Instance().getUserInfo(conversation.target, false);
            LiveEventBus.get("transfer").post(targetUser);
        }

//        Intent intent = new Intent(fragment.getActivity(), ContactListActivity.class);
//        startActivityForResult(intent, 100);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {

           /* UserInfo userInfo = data.getParcelableExtra("userInfo");
            ChannelInfo channelInfo = data.getParcelableExtra("channelInfo");
            if (userInfo != null) {
                sendUserCard(0, userInfo.uid, userInfo.name, userInfo.displayName, userInfo.portrait);
            } else if (channelInfo != null) {
                sendUserCard(3, channelInfo.channelId, channelInfo.name, channelInfo.name, channelInfo.portrait);
            }*/
        }
    }

    /**
     * @param json
     */
    private void sendTransferMoney(String json) {

        try {
//            Log.i("logs","payload.content==="+payload.content);
            JSONObject data = new JSONObject(json);
            TransferMoneyMessageContent transferMoneyMessageContent = new TransferMoneyMessageContent(
                    data.has("money") ? data.getString("money") : "0.00",
                    data.has("orderId") ? data.getString("orderId") : "",
                    data.has("des") ? data.getString("des") : "",
                    data.has("currency") ? data.getInt("currency") : -1);
            messageViewModel.sendMessage(conversation, transferMoneyMessageContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int priority() {
        return 100;
    }

    @Override
    public int iconResId() {
        return R.drawable.im_tool_transfer_button_bg;
    }

    @Override
    public String title(Context context) {
        return context.getString(R.string.transfer_money);
    }

    @Override
    public boolean filter(Conversation conversation) {

        boolean isFilter = conversation.type != Conversation.ConversationType.Single || "admin".equals(conversation.target) || "FireRobot".equals(conversation.target);
        Log.i("logs", "isFilter====" + isFilter);
        return isFilter;
    }

    @Override
    public String contextMenuTitle(Context context, String tag) {
        return title(context);
    }
}

/*
 * Copyright (c) 2020 WildFireChat. All rights reserved.
 */

package cn.wildfire.chat.kit.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.easy.utils.FileUtils;
import com.easy.utils.PhotoUtils;
import com.easy.utils.ViewUtils;
import com.easy.utils.base.FileConstant;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.lqr.imagepicker.ImagePicker;
import com.lqr.imagepicker.bean.ImageItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wildfire.chat.kit.R;
import cn.wildfire.chat.kit.R2;
import cn.wildfire.chat.kit.WfcIntent;
import cn.wildfire.chat.kit.WfcScheme;
import cn.wildfire.chat.kit.WfcUIKit;
import cn.wildfire.chat.kit.common.OperateResult;
import cn.wildfire.chat.kit.contact.ContactViewModel;
import cn.wildfire.chat.kit.contact.newfriend.InviteFriendActivity;
import cn.wildfire.chat.kit.conversation.ConversationActivity;
import cn.wildfire.chat.kit.mm.MMPreviewActivity;
import cn.wildfire.chat.kit.mm.MediaEntry;
import cn.wildfire.chat.kit.qrcode.QRCodeActivity;
import cn.wildfire.chat.kit.third.utils.ImageUtils;
import cn.wildfire.chat.kit.third.utils.UIUtils;
import cn.wildfire.chat.kit.widget.OptionItemView;
import cn.wildfire.chat.moment.FeedListActivity;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.model.UserInfo;

public class UserInfoFragment extends Fragment {
    @BindView(R2.id.portraitImageView)
    ImageView portraitImageView;
    @BindView(R2.id.nameTextView)
    TextView nameTextView;
    @BindView(R2.id.accountTextView)
    TextView accountTextView;
    @BindView(R2.id.chatButton)
    View chatButton;
    @BindView(R2.id.voipChatButton)
    View voipChatButton;
    @BindView(R2.id.inviteButton)
    Button inviteButton;
    @BindView(R2.id.aliasOptionItemView)
    OptionItemView aliasOptionItemView;

    @BindView(R2.id.qrCodeOptionItemView)
    OptionItemView qrCodeOptionItemView;

    @BindView(R2.id.momentButton)
    View momentButton;

    @BindView(R2.id.favContactTextView)
    TextView favContactTextView;

    private UserInfo userInfo;
    private UserViewModel userViewModel;
    private ContactViewModel contactViewModel;

    /**
     * 裁剪头像
     */
    public static final int START_CUT_AVATAR = 1001;

    public static UserInfoFragment newInstance(UserInfo userInfo) {
        UserInfoFragment fragment = new UserInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable("userInfo", userInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        assert args != null;
        userInfo = args.getParcelable("userInfo");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info_fragment, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        String selfUid = userViewModel.getUserId();
        if (selfUid.equals(userInfo.uid)) {
            // self
            chatButton.setVisibility(View.GONE);
            voipChatButton.setVisibility(View.GONE);
            inviteButton.setVisibility(View.GONE);
            qrCodeOptionItemView.setVisibility(View.VISIBLE);
            aliasOptionItemView.setVisibility(View.VISIBLE);
        } else if (contactViewModel.isFriend(userInfo.uid)) {
            // friend
            chatButton.setVisibility(View.VISIBLE);
            voipChatButton.setVisibility(View.VISIBLE);
            inviteButton.setVisibility(View.GONE);
        } else {
            // stranger
            momentButton.setVisibility(View.GONE);
            chatButton.setVisibility(View.GONE);
            voipChatButton.setVisibility(View.GONE);
            inviteButton.setVisibility(View.VISIBLE);
            aliasOptionItemView.setVisibility(View.GONE);
        }

        setUserInfo(userInfo);
        userViewModel.userInfoLiveData().observe(getViewLifecycleOwner(), userInfos -> {
            for (UserInfo info : userInfos) {
                if (userInfo.uid.equals(info.uid)) {
                    userInfo = info;
                    setUserInfo(info);
                    break;
                }
            }
        });
        userViewModel.getUserInfo(userInfo.uid, true);
        favContactTextView.setVisibility(contactViewModel.isFav(userInfo.uid) ? View.VISIBLE : View.GONE);

        if (!WfcUIKit.getWfcUIKit().isSupportMoment()) {
            momentButton.setVisibility(View.GONE);
        }
    }

    private void setUserInfo(UserInfo userInfo) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.mipmap.avatar_def)
                .transforms(new CenterCrop(), new RoundedCorners(UIUtils.dip2Px(getContext(), 5)));
        Glide.with(this)
                .load(userInfo.portrait)
                .apply(requestOptions)
                .into(portraitImageView);
        portraitImageView.setTag(R.id.picture_big, userInfo.portrait);
        nameTextView.setText(userViewModel.getUserDisplayName(userInfo));
        accountTextView.setText("山海ID:" + userInfo.name);
    }

    @OnClick(R2.id.chatButton)
    void chat() {
        if (!ViewUtils.canClick())
            return;
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        Conversation conversation = new Conversation(Conversation.ConversationType.Single, userInfo.uid, 0);
        intent.putExtra("conversation", conversation);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R2.id.momentButton)
    void moment() {
        if (!ViewUtils.canClick())
            return;
        Intent intent = new Intent(getActivity(), FeedListActivity.class);
        intent.putExtra("userInfo", userInfo);
        startActivity(intent);
    }

    @OnClick(R2.id.voipChatButton)
    void voipChat() {
        if (!ViewUtils.canClick())
            return;
        WfcUIKit.singleCall(getActivity(), userInfo.uid, false);
    }

    @OnClick(R2.id.aliasOptionItemView)
    void alias() {
        if (!ViewUtils.canClick())
            return;
        String selfUid = userViewModel.getUserId();
        if (selfUid.equals(userInfo.uid)) {
            Intent intent = new Intent(getActivity(), ChangeMyNameActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), SetAliasActivity.class);
            intent.putExtra("userId", userInfo.uid);
            startActivity(intent);
        }
    }

    private static final int REQUEST_CODE_PICK_IMAGE = 100;

    @OnClick(R2.id.portraitImageView)
    void portrait() {
        if (!ViewUtils.canClick())
            return;
        if (userInfo.uid.equals(userViewModel.getUserId())) {
            updatePortrait();
        } else {
            List<MediaEntry> entries = new ArrayList<>();
            MediaEntry mediaEntry = new MediaEntry();
            mediaEntry.setMediaUrl((String) portraitImageView.getTag(R.id.picture_big));
            entries.add(mediaEntry);
            MMPreviewActivity.startActivity(getActivity(), entries, 0);
        }

    }

    private void updatePortrait() {
        ImagePicker.picker().pick(this, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_PICK_IMAGE:
                    ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    if (images == null || images.isEmpty()) {
                        Toast.makeText(getActivity(), "更新头像失败: 选取文件失败 ", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!FileUtils.checkFileSizeIsLimit(new File(images.get(0).path).length(), 300, "K")) {
                        cn.wildfire.chat.kit.utils.FileUtils.startCorpImage(this, images.get(0).path, START_CUT_AVATAR);
                    } else {
                        LiveEventBus.get("updateInfo").post(images.get(0).path + ",avatar");
                    }

                    break;
                case START_CUT_AVATAR:
                    final String cutPath = FileUtils.getFilePath(FileConstant.TYPE_PHOTO, getActivity()) + "cutAvatar.png";
                    LiveEventBus.get("updateInfo").post(cutPath + ",avatar");
                    break;
            }



           /* if (thumbImgFile == null) {
                Toast.makeText(getActivity(), "更新头像失败: 生成缩略图失败", Toast.LENGTH_SHORT).show();
                return;
            }
            String imagePath = thumbImgFile.getAbsolutePath();

            MutableLiveData<OperateResult<Boolean>> result = userViewModel.updateUserPortrait(imagePath);
            result.observe(this, booleanOperateResult -> {
                if (booleanOperateResult.isSuccess()) {
                    Toast.makeText(getActivity(), "更新头像成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "更新头像失败: " + booleanOperateResult.getErrorCode(), Toast.LENGTH_SHORT).show();
                }
            });*/

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R2.id.inviteButton)
    void invite() {
        if (!ViewUtils.canClick())
            return;
        Intent intent = new Intent(getActivity(), InviteFriendActivity.class);
        intent.putExtra("userInfo", userInfo);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R2.id.qrCodeOptionItemView)
    void showMyQRCode() {
        if (!ViewUtils.canClick())
            return;
        UserInfo userInfo = userViewModel.getUserInfo(userViewModel.getUserId(), false);
        String qrCodeValue = WfcScheme.QR_CODE_PREFIX_USER + userInfo.uid;
        startActivity(QRCodeActivity.buildQRCodeIntent(getActivity(), "二维码", userInfo.portrait, qrCodeValue));
    }
}
